// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// Canvas1.java メイン処理部
//
// (C)Tomarky   2009.01.01-
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

// ルール
//
// 持ち点25000点／30000点返し
// 順位ウマ +20,+10,-10,-20
// 
// 包(パオ)は一切なし
// 国士13面,九蓮宝燈9面,四暗刻単騎,大四喜 いずれも通常の役満
// 役満の複合あり（但し、国士と国士13面,九蓮宝燈と九蓮宝燈9面,四暗刻と四暗刻単騎,小四喜と大四喜は複合せず１役満）
// 流し満貫あり (次局の親は連荘の条件に従う。流し満貫の三家和あり）
// 連荘の条件 親の聴牌or親の和了 ただし、オーラス親のトップ和了は強制和了止め
// 同点は起家に近い順に順位付け (起家＞起家の下家＞起家の対家＞起家の上家）
// 終局時の供託はトップ総取り
// ダブロンあり (供託、積み符は放銃者に近い側が総取り）
// 途中流局あり 途中流局は連荘（四家立直、三家和、四開槓、四風連打、九種九牌）
// 国士無双の暗槓の槍槓は無し
// 喰い断、飛び(ハコ)、南入(西入)およびサドンデスはプレイヤーが設定することができる
// オーラスの終了時点で３万点を超えるものがなければ点数の高い順に順位をつける
// 東風戦での西入、半荘戦での北入は無い
// 流局時の形式聴牌有効
// フリテン立直あり
// 常に一飜縛り
// 裏ドラ・槓ドラ・槓裏あり
// 槓ドラは、暗槓が即めくり、明槓は後めくり
// 食い替えはなし、チーポンで同一牌、チーで筋にあたる牌を捨てられない
// 副露は先付け（後付け）あり

// 目標
//  *テンパイ判定
//  *四開槓子 コンピュータ 流れ満貫 ルールの設定
//  *槍槓 *リーチ後のフリテン *見逃しフリテン
//  *喰い替えの制限 *リーチ後の暗槓

// 修正箇所マーク ...  @チェック

// 放銃率でダブロン分の放銃回数を引いて計算する必要がある

// 暗槓で変なことが起きてる

// 形式テンパイでロンできる（河底撈魚・槍槓以外で）バグがある
// ケータイで２度ほど目撃しただけ、詳細不明
// １度目は役表示されずフリーズ
// ２度目は役表示画面にはなったが役が何一つ表示されてなかた

// 海底のみ河底のみで和了できないときがあるらしい  

// *カラテン（和了牌が全部自分の手牌にある場合）テンパイとしない
// *checkTenpai内に設定した

// *連対率の表示

// *連荘数の表示

// ルール表示

// *順位ウマつける（ワンツー）

// *打牌種選択、副露選択をLEFT,RIGHTのキーの長押しで変更できるようにする
// *(俺のケータイを閉じたとき、LEFT,RIGHT,CLEAR,FIREしかキーが無い！)
// *CLEARキーの長押しはケータイの仕様でアプリ終了の確認になってしまう！

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

//インポート
import javax.microedition.lcdui.game.*;
import javax.microedition.lcdui.*;
import javax.microedition.midlet.*;
import java.util.*;                  //乱数や時間
import java.io.*;                  //InputStream OutputStream
import javax.microedition.rms.*;   //データセーブ用
//import javax.microedition.media.*; //音楽再生
//import javax.microedition.media.control.*; //トーン・ボリューム
//import javax.microedition.io;      //通信


// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

//メイン処理(キャンバス)
public class Canvas1 extends GameCanvas implements Runnable,CommandListener {
	private final int       DISPLAY_WIDTH  = 240,
	                        DISPLAY_HEIGHT = 268;

	private final int       CLEAR=-8, SEND=-10, SOFT3=-20;//,SOFT1=-6, SOFT2=-7,  SOFT4=-21;

	private final int       PAI_WIDTH  = 12,
	                        PAI_HEIGHT = 15;

	private final int       GPC_RT=Graphics.RIGHT|Graphics.TOP,
	                        GPC_LT=Graphics.LEFT|Graphics.TOP,
	                        GPC_HT=Graphics.HCENTER|Graphics.TOP;

	private final int       CALL_TUMO=0,CALL_CHI=1,CALL_PON=2,CALL_KAN=3,CALL_RON=4,CALL_REACH=5;

	private final int       FURO_CHI=0,FURO_PON=1,FURO_MINKAN=2,FURO_ANKAN=3,
	                        MT_JUNTSU=4, MT_ANKO=5, MT_TANKI=6, MT_TOITSU=7, 
	                        MT_RTATSU=8, MT_KTATSU=9, MT_PTATSU=10;

/*
 	private String[]  sss={"チー","ポン","明槓","暗槓","順子","暗刻","単騎","対子",
 	                       "両面塔子","嵌張塔子","辺張塔子","?"};
	private String painame(int pai) {
		if (pai<0)
			return "??";
		if (pai<9) 
			return Integer.toString(pai+1)+"萬";
		if (pai<18) 
			return Integer.toString(pai-8)+"筒";
		if (pai<27)
			return Integer.toString(pai-17)+"索";
		if (pai<31)
			return KazeName(pai-27);
		if (pai<34)
			return (new String("白発中？")).substring(pai-31,pai-30);
		return "??";
	}
//*/
	
	private final int       FCMD_PASS=0, FCMD_CHI=1, FCMD_PON=2, FCMD_KAN=3, FCMD_RON=4,
	                        TCMD_DAHAI=0, TCMD_REACH=1, TCMD_ANKAN=2, TCMD_KAKAN=3,
	                        TCMD_RYUKYOKU=4, TCMD_TUMOHO=5;
	
	private final int       RKK_KOHAI   =0,
	                        RKK_NAGASHI =1,
	                        RKK_9SHU    =2, 
	                        RKK_3RON    =3,
	                        RKK_4FON    =4,
	                        RKK_4REACH  =5, 
	                        RKK_4KAIKAN =6, 
	                        RKK_FINISH  =7;

	private final int       AG_TUMO=0,AG_RON=1;

	private final int       
	                        YK_MENZENTUMO  =1, 
	                        YK_RINSHANTUMO =2, 
	                        YK_HAITEI      =3,
	                        YK_HOTEI       =4, 
	                        YK_CHANKAN     =5, 
	                        YK_REACH       =6, 
	                        YK_DBLREACH    =7, 
	                        YK_IPPATSU     =8, 
	                        YK_CHITOI      =9,
	                        YK_TANYAO      =10,
	                        YK_PINFU       =11,
	                        YK_IPEKO       =12, 
	                        YK_ITTSU       =13, 
	                        YK_YAKUHAI     =14, 
	                        YK_CHANTA      =15, 
	                        YK_DOJUN       =16, 
	                        YK_DOKO        =17, 
	                        YK_TOITOI      =18, 
	                        YK_3ANKO       =19,
	                        YK_3KANTSU     =20, 
	                        YK_HONRO       =21, 
	                        YK_SHOSANGEN   =22,
	                        YK_HONITSU     =23,
	                        YK_JUNCHAN     =24,
	                        YK_RYANPEKO    =25,
	                        YK_CHINITSU    =26, 
	                        YK_DORA        =27,
	                        YK_URADORA     =28, 
	                        YK_TENHO       =101, 
	                        YK_CHIHO       =102,
	                        YK_KOKUSHI     =103, 
	                        YK_KOKUSHI13   =104, 
	                        YK_4ANKO       =105, 
	                        YK_4ANKOTANKI  =106, 
	                        YK_4KANTSU     =107,
	                        YK_DAISANGEN   =108, 
	                        YK_SHOSUSHI    =109,
	                        YK_DAISUSHI    =110, 
	                        YK_TUISO       =111, 
	                        YK_CHINRO      =112,
	                        YK_RYUISO      =113, 
	                        YK_CHUREN      =114, 
	                        YK_CHUREN9     =115;

	private final int       GM_START    = 0, GM_HAIPAI     = 1, GM_RIHAI     = 2, GM_TUMO      = 3,
	                        GM_NEXT     = 4, GM_RYUKYOKU1  = 5, GM_FUROCHECK = 6, GM_CALLCHECK = 7,
	                        GM_PON1     = 8, GM_PON2       = 9, GM_FURODAHAI =10, GM_CHI1      =11,
	                        GM_CHI2     =12, GM_MINKAN1    =13, GM_MINKAN2   =14, GM_RINSHAN   =15,
	                        GM_NEWGAME  =16, GM_ANKAN1     =17, GM_ANKAN2    =18, GM_KAKAN1    =19,
	                        GM_KAKAN2   =20, GM_RYUKYOKUSP =21, GM_RYUKYOKU2 =22, GM_CALLTUMO1 =23,
	                        GM_CALLRON1 =24, GM_REACH1     =25, GM_AGARI1    =26, GM_AGARI2    =27,
	                        GM_AGARI3   =28, GM_FINISH1    =29, GM_FINISH2   =30, GM_TITLE     =31,
	                        GM_MENU     =32, GM_CHANKAN1   =33, GM_NAGASHI   =34, GM_SEISEKI   =35,
	                        GM_RC_YAKU  =36, GM_RC_DATA    =37, GM_SETTING   =38, GM_RC_REKI   =39;

/*
	private int[][]         stc_yaku=new int[3][28],
	                        stc_yakuman=new int[3][15],
	                        stc_ryukyoku=new int[3][6],
	                        stc_HanCount=new int[3][13],
	                        stc_FuCount=new int[3][8];
	private int[]           stc_TotalHan=new int[3];
*/

	private Graphics        g;
	private Image[]         imgs=new Image[1];
	private int             keyEvent=-999;         //キープレスイベント
	private int             keyRepeat;              //キーリピートイベント
	private boolean         bl=true;
	private boolean         cf=false;
	private Command[]       cmds=new Command[4];//コマンド
	private int             cmd=-1;
	private Random          rndm=new Random();      //乱数

	private MIDlet          midlet;
	private String          appver;
	
	private Sprite          sprite;
	
	private int             GameMode=GM_TITLE;  //ゲームモード
	
	private boolean         sideFlag;
	private int             sideFlagCount;

	private int[]           seepais=new int[34];

	private int[]           yama=new int[136];
	private int[][]         tehai=new int[4][14];
	private int[][][]       sutehai=new int[4][30][2];
	private int[][][]       furopai=new int[4][5][2];
	private int             kicha, frCheck, oya, callPlayer, nowPlayer;
	private int             tumoIndex, ryukyokuIndex, rinshanIndex, dahai, kakanpai;
	private int             selectPai,  selectTCmd, selectFCmd;
	private int             cpuplayer, paiopen, PonCall,KanCall, ChiCall,RonCall;
	private int             baKaze, kyoku, honba, kyotaku, firstRotate;
	private boolean         furoselect, furodahai, rinshanhai ,callReach, haitei, kakan,
	                        minkan, alllast;
	
	private int             selectChi1, selectChi2, reachCount, agari_type, agaripai;
	private int             rkk_type, rkk_4fon, yk_fu, yk_han, yk_yakuman, yk_score, 
	                        rkk_4kaikan;
	
	private int             callCount, haipaiCount, rkkCount, agCount, fnCount, nagashi,
	                        selectMenu=0, titleCount=0, GameState=0, selectSeiseki=0,
	                        rc_yk_player, rc_yk_type, rc_yk_scroll,
	                        rc_dt_player, rc_dt_type, rc_dt_scroll,
	                        rc_rk_scroll;
	
	private int[]           score=new int[4],
	                        addscore= new int[4],
	                        furiten=new int[4],
	                        agariPlayer=new int[3],
	                        kuikae=new int[2],
	                        renchan=new int[4];

	private int[][]         yaku=new int[20][2],
	                        rank=new int[4][2], 
	                        think=new int[4][3];
	
	private boolean[]       menzen=new boolean[4],
	                        reach=new boolean[4], dblReach=new boolean[4],
	                        tenpai=new boolean[4],
	                        ippatsu=new boolean[4],
	                        c_r_agari=new boolean[34],
	                        c_t_agari=new boolean[34],
	                        c_k_agari=new boolean[34],
	                        c_ankan=new boolean[34];

	private boolean[][]     r_agari=new boolean[4][34],
	                        t_agari=new boolean[4][34],
	                        k_agari=new boolean[4][34];

	private boolean         rule_tobi, rule_kuitan, recording, cpuopen, nowPlaying,
	                        rule_SuddenDeath, rule_Continue;
	private int             rule_PlayLong, selSetting, showcomment, commentCount;
	
	private StcData[]       stcdata=new StcData[5];
	private SensekiData[]   senseki=new SensekiData[5];
	
	private int             senrekiCount;
	private int[][]         senreki=new int[10][12];
	private int[]           senrekiRule=new int[10];
	private long[]          senrekiDate=new long[10];

	//Test
	private int             rs_size;

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//コンストラクタ
	Canvas1(MIDlet m) {
		//キーイベントの抑制
		super(false);
		
		//MIDlet操作用
		midlet=m;
		appver="ver "+midlet.getAppProperty("MIDlet-Version");

	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void appinit() {
	
		try {
			imgs[0]=Image.createImage("/tenbo1.png");
		
			sprite=new Sprite(Image.createImage("/pai.png"),PAI_WIDTH,PAI_HEIGHT);
			sprite.setFrame(0);
		} catch (Exception e) {
			//System.out.println("Canvas1.run(createImage)::"+e.toString());
			bl=false;
		}
		

		//グラフィックスの取得
		g=getGraphics();

		//コマンドの生成
		cmds[0]=new Command("はい",  Command.OK,    0);
		cmds[1]=new Command("いいえ",Command.CANCEL,0);
		//cmds[2]=new Command("変更",  Command.SCREEN,0);
		//cmds[3]=new Command("戻る",  Command.BACK,  0);
		cmds[2]=new Command("終了",  Command.EXIT,  0);
		cmds[3]=new Command("中止",  Command.STOP,  0);

		//コマンドの追加
		addCommand(cmds[2]);


		//コマンドリスナーの指定
		setCommandListener(this);

		//ケータイ閉じた時の切り替えスイッチ(KEY_STAR [*])
		sideFlag=false;
		sideFlagCount=0;

		rs_size=RecordsSize();
		//System.out.println("RecordSize is "+rs_size);
	}

	private void gameinit() {
		int i;
		
		//プレイの状態
		nowPlaying=false;

		//牌山の初期化(34種の牌を４個ずつ設定）
		for (i=0;i<136;i++)
			yama[i]=i%34;
		

		//0がプレーヤー -2以下はツモ切り 1頭脳Ａ 2は頭脳Ｂ 3は頭脳Ｃ 4は頭脳Ｄ
		think[0][0]=1;
		think[1][0]=2;
		think[2][0]=3;
		think[3][0]=0;
		
		//データの記録をするか
		recording=true;
		
		//コンピュータの手牌の表示・非表示
		cpuopen=false;

		//ルールの設定（将来的にはレコードストアからのロードするように）
		rule_tobi=true;        //飛び trueあり falseなし
		rule_kuitan=true;      //喰い断 trueあり falseなし
		rule_Continue=true;    //南入・西入。一荘では無い
		rule_SuddenDeath=true; //南入・西入をサドンデスにするか
		rule_PlayLong=1;       //0東風 1半荘 2一荘
		
		//設定のロード
		loadSetting();
		
		//データ記録の準備
		//for (i=0;i<stcdata.length;i++)
		//	stcdata[i]=new StcData();
		loadStcData();

		//byte[] bb=stcdata[0].toBytes(0);
		//System.out.println("StcData Size:"+bb.length);
		//System.out.println("StcData Size:"+(bb.length*stcdata.length));
		
		//for (i=0;i<senseki.length;i++) 
		//	senseki[i]=new SensekiData();
		loadSensekiData();

		//byte[] bb2=senseki[0].toBytes(0,0);
		//System.out.println("Senseki Size:"+bb2.length);
		//System.out.println("Senseki Size:"+(bb2.length*senseki.length));
		
		senrekiCount=0;
		loadSenreki();

		showcomment=0;
	}

	//実行
	public void run() {
		int   i;
		long  sleepTime=0L;

		appinit();
		gameinit();
		
		//*************************************//
		
		while (bl) { //メインループ
		
			//コマンドイベントの処理
			switch (cmd) {
			case 0: //はい
				removeCommand(cmds[0]);
				removeCommand(cmds[1]);
				switch (showcomment) {
				case 1: //終了
					bl=false;
					showcomment=4;
					break;
				case 2: //中止
					drawComment("しばらくお待ち下さい");
					flushGraphics();
					if (recording) {
						int cp;
						for (i=0;i<4;i++) {
							cp=think[i][0];
							if (cp>=0) {
								stcdata[cp].StopCount++;
								senseki[cp].StopCount++;
								stcdata[cp].RenchanCount+=renchan[i]; //連荘の記録
								if (renchan[i]>stcdata[cp].RenchanMax)
									stcdata[cp].RenchanMax=renchan[i];
							}
						}
					}
					for (i=0;i<4;i++) {
						reach[i]=false;
						tehai[i][13]=0;
						sutehai[i][0][0]=0;
						furopai[i][0][0]=0;
						furiten[i]=0;
					}
					GameState=0; nowPlaying=false;
					honba=0; kyotaku=0; baKaze=0;
					if (recording) {
						saveSensekiData();
						saveStcData();
						saveSenreki();
					}
					GameMode=GM_MENU;
					addCommand(cmds[2]);
					showcomment=0;
					break;
				case 3: //全消去
					for (i=0;i<stcdata.length;i++)
						stcdata[i]=new StcData();
					deleteRecords();
					senrekiCount=0;
					//addCommand(cmds[2]);
					commentCount=20;
					showcomment=5;
					break;
				}
				break;
			case 1: //いいえ
				removeCommand(cmds[0]);
				removeCommand(cmds[1]);
				addCommand(cmds[2]);
				if ((nowPlaying)&&(GameMode!=GM_FINISH2))
					addCommand(cmds[3]);
				showcomment=0;
				break;
			/*
			case 2: //変更 (テスト用コマンド）
				if (cpuplayer==0) {
					cpuplayer=7;
					paiopen=8;
					cpuopen=false;
				} else {
					if (cpuopen) {
						cpuplayer=0;
						paiopen=15;
					} else {
						paiopen=15;
						cpuopen=true;
					}
				}
				break; //*/
			case 2: //終了
				removeCommand(cmds[2]);
				if (nowPlaying)
					removeCommand(cmds[3]);
				addCommand(cmds[0]);
				addCommand(cmds[1]);
				showcomment=1;
				break;
			case 3: //中止
				removeCommand(cmds[2]);
				removeCommand(cmds[3]);
				addCommand(cmds[0]);
				addCommand(cmds[1]);
				showcomment=2;
				break;
			}
			cmd=-999;
			
			if (showcomment==0) {
				
				if ((keyRepeat==KEY_STAR)&&(keyEvent==-999)) {
					if (sideFlagCount==0) {
						sideFlag=!sideFlag;
						sideFlagCount=20;
					}
					keyRepeat=-999;
				}
				
				//ゲームモードごとに各処理ルーチンへ
				switch (GameMode) {
				case GM_NEWGAME:    gm_newgame();    break;
				case GM_START:      gm_start();      break;
				case GM_HAIPAI:     gm_haipai();     break;
				case GM_RIHAI:      gm_rihai();      break;
				case GM_TUMO:       gm_tumo();       break;
				case GM_NEXT:       gm_next();       break;
				case GM_FUROCHECK:  gm_furocheck();  break;
				case GM_CALLCHECK:  gm_callcheck();  break;
				case GM_CHI1:       gm_chi1();       break;
				case GM_CHI2:       gm_chi2();       break;
				case GM_PON1:       gm_pon1();       break;
				case GM_PON2:       gm_pon2();       break;
				case GM_FURODAHAI:  gm_furodahai();  break;
				case GM_MINKAN1:    gm_minkan1();    break;
				case GM_MINKAN2:    gm_minkan2();    break;
				case GM_ANKAN1:     gm_ankan1();     break;
				case GM_ANKAN2:     gm_ankan2();     break;
				case GM_KAKAN1:     gm_kakan1();     break;
				case GM_KAKAN2:     gm_kakan2();     break;
				case GM_RYUKYOKUSP: gm_ryukyokusp(); break;
				case GM_RYUKYOKU1:  gm_ryukyoku1();  break;
				case GM_RYUKYOKU2:  gm_ryukyoku2();  break;
				case GM_REACH1:     gm_reach1();     break;
				case GM_CALLTUMO1:  gm_calltumo1();  break;
				case GM_CALLRON1:   gm_callron1();   break;
				case GM_AGARI1:     gm_agari1();     break;
				case GM_AGARI2:     gm_agari2();     break;
				case GM_AGARI3:     gm_agari3();     break;
				case GM_FINISH1:    gm_finish1();    break;
				case GM_FINISH2:    gm_finish2();    break;
				case GM_TITLE:      gm_title();      break;
				case GM_MENU:       gm_menu();       break;
				case GM_CHANKAN1:   gm_chankan1();   break;
				case GM_NAGASHI:    gm_ryukyoku2();  break;
				case GM_SEISEKI:    gm_seiseki();    break;
				case GM_RC_YAKU:    gm_rc_yaku();    break;
				case GM_RC_DATA:    gm_rc_data();    break;
				case GM_SETTING:    gm_setting();    break;
				case GM_RC_REKI:    gm_rc_reki();    break;
				}
				keyEvent=-999;
			
				//描画処理 ***************
	
				//雀卓（地）
				g.setColor(0,128,0);
				g.fillRect(0,0,DISPLAY_WIDTH,DISPLAY_HEIGHT);
				
				switch (GameMode) {
				case GM_TITLE:
					drawTitle();
					break;
				case GM_MENU:
					drawMenu();
					break;
				case GM_SEISEKI:
					drawSeiseki();
					break;
				case GM_RC_YAKU:
					drawRc_yaku();
					break;
				case GM_RC_DATA:
					drawRc_data();
					break;
				case GM_RC_REKI:
					drawRc_reki();
					break;
				case GM_SETTING:
					drawSetting();
					break;
				default:
					drawAll();
					break;
				}
				
				if (sideFlagCount>0) {
					g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_SMALL));
					g.setColor(255,255,255);
					g.drawString(sideFlag?"ON ":"OFF",240,0,GPC_RT);
					sideFlagCount--;
				}

			} else {
				switch (showcomment) {
				case 1:
				case 2:
				case 3:
					switch (keyEvent) {
					case FIRE:
						if (sideFlag) {
							cmd=0; 
						}
						break;
					case CLEAR:  
						cmd=1;
						break;
					}
					break;
				}
				keyEvent=-999;
				switch (showcomment) {
				case 1: drawComment("終了しますか？");   break;
				case 2: drawComment("中止しますか？");   break;
				case 3: drawComment("全消去しますか？"); break;
				case 4: drawComment("終了しています");   break;
				case 5: drawComment("消去しました");
				        if (commentCount==0) {
				        	addCommand(cmds[2]);
				        	showcomment=0;
				        } commentCount--;
				        break;
				}
			}
			flushGraphics(); //フラッシュ
            
			//スリープ
			while ((System.currentTimeMillis()<sleepTime+100L)&&(bl));
			sleepTime=System.currentTimeMillis();
		}
		
		if (cf==false){ 
			//データのセーブ
			saveSetting();
			//saveStcData();
			//saveSensekiData();
			//saveSenreki();
	
			//終了
			midlet.notifyDestroyed();
		}

	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//キープレスイベント
	public void keyPressed(int keyCode) {
		if (keyCode==0) return;
		switch (keyCode) {
		case KEY_NUM0:
		case CLEAR:
		case SEND:
		case SOFT3:
		case KEY_STAR:
			keyEvent=keyCode;
			break;
		default:
			keyEvent=getGameAction(keyCode);
			break;
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//キーリピートイベント
	public void keyRepeated(int keyCode) {
		if (keyCode==0) return;
		if ((keyRepeat<=-990)&&(keyRepeat>-999)) {
			keyRepeat--;
			return;
		}
		
		switch (keyCode) {
		case KEY_NUM0:
		case CLEAR:
		case SEND:
		case SOFT3:
		case KEY_STAR:
			keyRepeat=keyCode;
			break;
		default:
			keyRepeat=getGameAction(keyCode);
			break;
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//キーリリースイベント
	public void keyReleased(int keyCode) {
		if (keyCode==0) return;
		
		switch (keyCode) {
		case KEY_NUM0:
		case CLEAR:
		case SEND:
		case SOFT3:
		case KEY_STAR:
			if (keyRepeat==keyCode);
				keyRepeat=-999;
			break;
		default:
			if (keyRepeat==getGameAction(keyCode));
				keyRepeat=-999;
			break;
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//コマンドイベント
	public void commandAction(Command c,Displayable disp) {
		int i;
		for (i=0;i<cmds.length;i++)
			if (c==cmds[i]) {
				cmd=i;
				break;
			}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//コメント描写
	public void drawComment(String s) {
		g.setColor(255,255,255);
		g.fillRect(0,DISPLAY_HEIGHT-20,DISPLAY_WIDTH,20);
		g.setColor(0,128,255);
		g.drawRect(0,DISPLAY_HEIGHT-20,DISPLAY_WIDTH,20);
		g.setColor(0,0,0);
		g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_MEDIUM));
		g.drawString(s,DISPLAY_WIDTH/2,DISPLAY_HEIGHT-18,GPC_HT);
	} //*/

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

//中抜き文字の描写
	private void drawNukiMoji(String s, int x, int y, int rIn, int gIn, int bIn, int anchor) {
		int i,j;
		
		for (i=-1;i<2;i++)
			for (j=-1;j<2;j++)
				g.drawString(s,x+j,y+i,anchor);
		g.setColor(rIn,gIn,bIn);
		g.drawString(s,x,y,anchor);
	}
	private void drawNukiMoji(String s, int x, int y, int rIn,   int gIn,  int bIn,
	                                                  int rOut,  int gOut, int bOut, int anchor) {
		g.setColor(rOut,gOut,bOut);
		drawNukiMoji(s,x,y,rIn,gIn,bIn,anchor);
	} 

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//乱数の計算
	private int rand(int num) {
		return (rndm.nextInt()>>>1)%num;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//終了命令
	public void callFinish() {
		if (nowPlaying) { //ゲーム中なら
			if (recording) { //中止
				int cp;
				for (int i=0;i<4;i++) {
					cp=think[i][0];
					if (cp>=0) {
						stcdata[cp].StopCount++;
						senseki[cp].StopCount++;
						stcdata[cp].RenchanCount+=renchan[i]; //連荘の記録
						if (renchan[i]>stcdata[cp].RenchanMax)
							stcdata[cp].RenchanMax=renchan[i];
					}
				}
				saveSensekiData();
				saveStcData();
				saveSenreki();
			}
		}
		saveSetting();
		showcomment=4;
		bl=false;
		cf=true;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private String PlayerName(int n) {
		switch (n) {
		case 0:     return "あなた";
		case 1:     return "太郎(CPU)";
		case 2:     return "一郎(CPU)";
		case 3:     return "次郎(CPU)";
		case 4:     return "花子(CPU)";
		case -1:    return "全体";
		default:    return "ツモ切り";
		}
	}

	private String KazeName(int n) {
		switch (n) {
		case 0:  return "東";
		case 1:  return "南";
		case 2:  return "西";
		case 3:  return "北";
		default: return "？";
		}
	}
	
	private String YakuName(int n) {
		switch (n) {
		case YK_CHITOI:      return "七対子";         
		case YK_TANYAO:      return "断ヤオ九";       
		case YK_REACH:       return "立直";           
		case YK_IPPATSU:     return "一発";           
		case YK_DORA:        return "ドラ";           
		case YK_URADORA:     return "裏ドラ";         
		case YK_MENZENTUMO:  return "門前清自摸和";   
		case YK_DBLREACH:    return "ダブル立直";     
		case YK_HAITEI:      return "海底摸月";       
		case YK_HOTEI:       return "河底撈魚";       
		case YK_RINSHANTUMO: return "嶺上開花";       
		case YK_CHINITSU:    return "清一色";         
		case YK_HONITSU:     return "混一色";         
		case YK_CHANKAN:     return "槍槓";           
		case YK_PINFU:       return "平和";           
		case YK_IPEKO:       return "一盃口";         
		case YK_RYANPEKO:    return "二盃口";         
		case YK_ITTSU:       return "一気通貫";       
		case YK_DOJUN:       return "三色同順";       
		case YK_DOKO:        return "三色同刻";       
		case YK_3ANKO:       return "三暗刻";         
		case YK_3KANTSU:     return "三槓子";         
		case YK_TOITOI:      return "対々和";         
		case YK_CHANTA:      return "混全帯ヤオ九";   
		case YK_JUNCHAN:     return "純全帯ヤオ九";   
		case YK_HONRO:       return "混老頭";         
		case YK_SHOSANGEN:   return "小三元";         
		case YK_YAKUHAI:     return "役牌";           
		case YK_KOKUSHI:     return "国士無双";       
		case YK_KOKUSHI13:   return "国士無双１３面"; 
		case YK_TENHO:       return "天和";           
		case YK_CHIHO:       return "地和";           
		case YK_RYUISO:      return "緑一色";         
		case YK_TUISO:       return "字一色";         
		case YK_DAISUSHI:    return "大四喜";         
		case YK_SHOSUSHI:    return "小四喜";         
		case YK_DAISANGEN:   return "大三元";         
		case YK_CHUREN:      return "九蓮宝燈";       
		case YK_CHUREN9:     return "九蓮宝燈９面";   
		case YK_CHINRO:      return "清老頭";         
		case YK_4ANKOTANKI:  return "四暗刻単騎";     
		case YK_4ANKO:       return "四暗刻";         
		case YK_4KANTSU:     return "四槓子";         
		default:             return "";               
		}
	}
	
	private String RyukyokuName(int n) {
		switch (n) {
		case RKK_4FON:     return "四風連打";
		case RKK_9SHU:     return "九種九牌";
		case RKK_NAGASHI:  return "流し満貫";
		case RKK_KOHAI:    return "荒牌流局";
		case RKK_4REACH:   return "四家立直";
		case RKK_3RON:     return "三家和";
		case RKK_4KAIKAN:  return "四開槓";
		case RKK_FINISH:   return "終局";
		default:           return "途中流局";
		}
	}
	
	private int getSelectPai(int p, int selpai) {
		if (selpai<0) {
			if (rinshanhai) {
				if (minkan) {
					return yama[135-rinshanIndex];
				} else {
					return yama[135-rinshanIndex+1];
				}
			} else {
				return yama[tumoIndex];
			}
		} else {
			return tehai[p][selpai];
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void gm_title() {
		if (sideFlag) {
			if (keyEvent==CLEAR) {
				cmd=2;
				keyEvent=-999;
			}
		}
		if (keyEvent!=-999)
			GameMode=GM_MENU;
		//keyEvent=-999;
		titleCount++;
		titleCount%=80;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	
	private int[] randomCpu() {
		int   i,j,t;
		int[] p=new int [4];
		for (i=0;i<4;i++)
			p[i]=i+1;
		for (i=0;i<15;i++) {
			j=rand(4);
			t=p[i%4];
			p[i%4]=p[j];
			p[j]=t;
		}
		return p;
	}
	
	private void gm_menu() {
		if (keyEvent==-999) {
			switch (keyRepeat) {
			case UP:
				if (sideFlag) {
					keyEvent=SOFT3;
					keyRepeat=-990;
				}
				break;
			case DOWN:
				if (sideFlag) {
					keyEvent=SEND;
					keyRepeat=-990;
				}
				break;
			case FIRE:
				if (sideFlagCount==0) {
					sideFlag=!sideFlag;
					sideFlagCount=20;
				}
				keyRepeat=-999;
				break;
			}
		}
		if (selectMenu==0) {
			if (keyEvent==SEND) {
				int[] p=randomCpu();
				for (int i=0;i<3;i++)
					think[i][0]=p[i];
				think[3][0]=0;
				cpuopen=false;
				keyEvent=FIRE;
			}
			if (keyEvent==SOFT3) {
				int[] p=randomCpu();
				for (int i=0;i<4;i++)
					think[i][0]=p[i];
				keyEvent=FIRE;
			}
		}
		switch (keyEvent) {
		case CLEAR:
			GameMode=GM_TITLE;
			break;
		case UP:
			selectMenu=(selectMenu+2)%3;
			break;
		case DOWN:
			selectMenu=(selectMenu+1)%3;
			break;
		case FIRE:
			switch (selectMenu) {
			case 0: //対局開始
				addCommand(cmds[3]);
				GameMode=GM_NEWGAME;
				break;
			case 1: //成績
				selectSeiseki=0;
				GameMode=GM_SEISEKI;
				break;
			case 2: //設定
				selSetting=0;
				//saveSensekiData();
				GameMode=GM_SETTING;
				break;
			}
		}
		//keyEvent=-999;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void gm_seiseki() {
		switch (keyEvent) {
		case CLEAR:
			GameMode=GM_MENU;
			break;
		case UP:
			selectSeiseki=(selectSeiseki+2)%3;
			break;
		case DOWN:
			selectSeiseki=(selectSeiseki+1)%3;
			break;
		case FIRE:
			switch (selectSeiseki) {
			case 0: //戦績
				rc_dt_player=0;
				rc_dt_type=0;
				rc_dt_scroll=0;
				GameMode=GM_RC_DATA;
				break;
			case 1: //統計
				rc_yk_player=0;
				rc_yk_type=0;
				rc_yk_scroll=0;
				GameMode=GM_RC_YAKU;
				break;
			case 2: //戦歴
				rc_rk_scroll=0;
				GameMode=GM_RC_REKI;
				break;
			}
		}
		//keyEvent=-999;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	
	private void gm_rc_yaku() {
		if (keyEvent==-999) {
			switch (keyRepeat) {
			case UP:
				if (sideFlag) {
					keyEvent=LEFT;
					keyRepeat=-990;
				} else {
					if (rc_yk_scroll>0) {
						rc_yk_scroll--;
					}
				}
				break;
			case DOWN:
				if (sideFlag) {
					keyEvent=RIGHT;
					keyRepeat=-990;
				} else {
					switch (rc_yk_type) {
					case 1:
					case 2:
					case 3:
						if (rc_yk_scroll<12) {
							rc_yk_scroll++;
						}
						break;
					case 4:
					case 5:
						if (rc_yk_scroll<8) {
							rc_yk_scroll++;
						}
						break;
					case 0:
						if (rc_yk_scroll<74) {
							rc_yk_scroll++;
						}
						break;
					}
				}
				break;
			}
		}
		switch (keyEvent) {
		case CLEAR:
			GameMode=GM_SEISEKI;
			break;
		case FIRE:
			switch (rc_yk_type) {
			case 1:
			case 2:
			case 4:break;
			default:
				rc_yk_scroll=0;
				break;
			}
			rc_yk_type=(rc_yk_type+1)%10;
			break;
		case SEND:
			switch (rc_yk_type) {
			case 2:
			case 3:
			case 5: break;
			default:
				rc_yk_scroll=0;
				break;
			}
			rc_yk_type=(rc_yk_type+9)%10;
			break;
		case KEY_NUM0:
			rc_yk_scroll+=10;
			switch (rc_yk_type) {
			case 1:
			case 2:
			case 3: if (rc_yk_scroll>12) rc_yk_scroll=12; break;
			case 4:
			case 5: if (rc_yk_scroll>8) rc_yk_scroll=8;   break;
			case 0: if (rc_yk_scroll>74) rc_yk_scroll=74; break;
			}
			break;
		case UP:
			if (rc_yk_scroll>0) {
				rc_yk_scroll--;
			} else {
				switch (rc_yk_type) {
				case 1:
				case 2:
				case 3: rc_yk_scroll=12; break;
				case 4:
				case 5: rc_yk_scroll=8;  break;
				case 0: rc_yk_scroll=74; break;
				}
			}
			break;
		case DOWN:
			switch (rc_yk_type) {
			case 1:
			case 2:
			case 3:
				if (rc_yk_scroll<12) {
					rc_yk_scroll++;
				} else {
					rc_yk_scroll=0;
				}
				break;
			case 4:
			case 5:
				if (rc_yk_scroll<8) {
					rc_yk_scroll++;
				} else {
					rc_yk_scroll=0;
				}
				break;
			case 0:
				if (rc_yk_scroll<74) {
					rc_yk_scroll++;
				} else {
					rc_yk_scroll=0;
				}
				break;
			}
			break;
		case LEFT:
			if (rc_yk_player>-1) rc_yk_player--;
			break;
		case RIGHT:
			if (rc_yk_player<4) rc_yk_player++;
			break;
		}
		//keyEvent=-999;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void gm_rc_data() {
		if (keyEvent==-999) {
			switch (keyRepeat) {
			case UP:
				if (sideFlag) {
					keyEvent=LEFT;
					keyRepeat=-990;
				} else {
					if (rc_dt_scroll>0) {
						rc_dt_scroll--;
					}
				}
				break;
			case DOWN:
				if (sideFlag) {
					keyEvent=RIGHT;
					keyRepeat=-990;
				} else {
					switch (rc_dt_type) {
					case 0:
						if (rc_dt_scroll<41) rc_dt_scroll++;
						break;
					default:
						if (rc_dt_scroll<20) rc_dt_scroll++;
						break;
					}
				}
				break;
			}
		}
		switch (keyEvent) {
		case CLEAR:
			GameMode=GM_SEISEKI;
			break;
		case FIRE:
			rc_dt_scroll=0;
			rc_dt_type=(rc_dt_type+1)%(3+rule_PlayLong);
			break;
		case SEND:
			rc_dt_scroll=0;
			rc_dt_type=(rc_dt_type+2+rule_PlayLong)%(3+rule_PlayLong);
			break;
		case KEY_NUM0:
			rc_dt_scroll+=10;
			switch (rc_dt_type) {
			case 0: if (rc_dt_scroll>41) rc_dt_scroll=41; break;
			default: if (rc_dt_scroll>20) rc_dt_scroll=20; break;
			}
			break;
		case UP:
			if (rc_dt_scroll>0) {
				rc_dt_scroll--;
			} else {
				switch (rc_dt_type) {
				case 0: rc_dt_scroll=41; break;
				default: rc_dt_scroll=20; break;
				}
			}
			break;
		case DOWN:
			switch (rc_dt_type) {
			case 0:
				if (rc_dt_scroll<41) rc_dt_scroll++;
				else rc_dt_scroll=0;
				break;
			default:
				if (rc_dt_scroll<20) rc_dt_scroll++;
				else rc_dt_scroll=0;
				break;
			}
			break;
		case LEFT:
			if (rc_dt_player>-1) rc_dt_player--;
			break;
		case RIGHT:
			if (rc_dt_player<4) rc_dt_player++;
			break;
		}
		//keyEvent=-999;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void gm_rc_reki() {
		if ((keyEvent==-999)&&(sideFlag)) {
			switch (keyRepeat) {
			case UP:
				keyEvent=LEFT;
				keyRepeat=-990;
				break;
			case DOWN:
				keyEvent=RIGHT;
				keyRepeat=-990;
				break;
			}
		}
		switch (keyEvent) {
		case CLEAR:
			GameMode=GM_SEISEKI;
			break;
		case LEFT:
			if (rc_rk_scroll>0) rc_rk_scroll--;
			break;
		case RIGHT:
			if (rc_rk_scroll<senrekiCount-1) rc_rk_scroll++;
			break;
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void gm_setting() {
		if (keyEvent==-999) {
			switch (keyRepeat) {
			case UP:
				if (sideFlag) {
					keyEvent=LEFT;
					keyRepeat=-990;
				} else {
					if (selSetting>0)
						selSetting--;
				}
				break;
			case DOWN:
				if (sideFlag) {
					keyEvent=RIGHT;
					keyRepeat=-990;
				} else {
					if (selSetting<11)
						selSetting++;
				}
				break;
			}
		}
		switch (keyEvent) {
		case SEND:
			cpuopen=false;
			int[] ppp=randomCpu();
			for (int i=0;i<3;i++) {
				think[i][0]=ppp[i];
			}
			think[3][0]=0;
			recording=true;
			break;
		case FIRE:
			if (selSetting==11) { //データの消去
				removeCommand(cmds[2]);
				addCommand(cmds[0]);
				addCommand(cmds[1]);
				showcomment=3;
				return;
			}
		case CLEAR:
			int fg=0;
		    for (int i=0;i<4;i++) {
		    	if (think[i][0]<0) { fg=-10; break; }
		    	if (think[i][0]>0) fg++;
		    }
		    if (fg<3) recording=false;
		    if ((fg!=4)&&(cpuopen)) recording=false;
		    loadSensekiData();
			GameMode=GM_MENU;
			break;
		case UP:
			if (selSetting>0) selSetting--;
			else selSetting=11;
			break;
		case DOWN:
			if (selSetting<11) selSetting++;
			else selSetting=0;
			break;
		case LEFT:
			switch (selSetting) {
			case 0:
			case 1:
			case 2:
			case 3: int p=selSetting;
				    int k=think[p][0];
				    if (k>0) think[p][0]--;
				    else if (k==0)  think[p][0]=-100;
				    break;
			case 4: cpuopen=false; break;
			case 5: if (rule_PlayLong>0) rule_PlayLong--; break;
			case 6: rule_Continue=false; break;
			case 7: rule_SuddenDeath=false; break;
			case 8: rule_tobi=false; break;
			case 9: rule_kuitan=false; break;
			case 10: recording=false;
			}
			break;
		case RIGHT:
			switch (selSetting) {
			case 0:
			case 1:
			case 2:
			case 3: int p=selSetting;
				    int k=think[p][0];
				    if (k<0) think[p][0]=0;
				    else if (k<4)  think[p][0]++;
				    break;
			case 4: cpuopen=true; break;
			case 5: if (rule_PlayLong<2) rule_PlayLong++; break;
			case 6: rule_Continue=true; break;
			case 7: rule_SuddenDeath=true; break;
			case 8: rule_tobi=true; break;
			case 9: rule_kuitan=true; break;
			case 10: recording=true;
			}
			break;
		}
		//keyEvent=-999;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void gm_newgame() {
		int i;
		
		nowPlaying=true;
		
		//cpuplayer=7; //* 通常 7
		//*
		cpuplayer=0;
		for (i=0;i<4;i++) {
			if (think[i][0]!=0) {
				cpuplayer|=1<<i;
			}
		}
		//*/
		
		if (recording) {
			int cp;
			for (i=0;i<4;i++) {
				cp=think[i][0];
				if (cp>=0) {
					stcdata[cp].PlayCount++;
					switch (rule_PlayLong) {
					case 0: stcdata[cp].TonpuCount++;   break;
					case 1: stcdata[cp].HanchanCount++; break;
					case 2: stcdata[cp].IchanCount++;   break;
					}
					senseki[cp].PlayCount++;
				}
			}
		}
		
		for (i=0;i<4;i++)
			renchan[i]=0;
		
		oya=nowPlayer=kicha=rand(4); //*
		for (i=0;i<4;i++)
			score[i]=25000;  //*
		baKaze=0;    //*
		kyoku=0;     //*
		honba=0;     //*
		kyotaku=0;   //*
		haipaiCount=-1;
		alllast=false;
		GameMode=GM_START;
		//keyEvent=-999;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//スタート
	private void gm_start() {
		int i,j,p,q;
		
		if (haipaiCount<0) { 
			switch (keyEvent) {
			case FIRE:
//				for (i=0;i<4;i++)
//					System.out.println("連荘"+Integer.toString(i)+"..."+Integer.toString(renchan[i]));

				shuffle();
				haipai();
				for (i=0;i<34;i++) {
					seepais[i]=0;
				}
				seepais[yama[131]]++;
				for (i=0;i<4;i++) {
					if (recording) {
						int cp=think[i][0];
						if (cp>=0) {
							stcdata[cp].KyokuCount++;
							senseki[cp].KyokuCount[baKaze]++;
						}
					}
					think[i][1]=0; //思考フラグリセット
					think[i][2]=0;
					addscore[i]=0;
					reach[i]=false;
					dblReach[i]=false;
					menzen[i]=true;
					tenpai[i]=false;
					ippatsu[i]=false;
					furiten[i]=0;
					sutehai[i][0][0]=0;
					furopai[i][0][0]=0;
					for (j=0;j<34;j++) {
						r_agari[i][j]=false;
						t_agari[i][j]=false;
						k_agari[i][j]=false;
					}
					tehai[i][13]=0;
					
					//テスト @チェック
					/*
					tehai[i][0]=2;
					tehai[i][1]=2;
					tehai[i][2]=2;
					tehai[i][3]=3;
					tehai[i][4]=4;
					tehai[i][5]=5;
					tehai[i][6]=6;
					tehai[i][7]=7;
					tehai[i][8]=8;
					tehai[i][9]=29;
					tehai[i][10]=28;
					tehai[i][11]=28;
					tehai[i][12]=28;
					// */
					
				}
				//テスト
				/*
				for (j=0;j<60;j++)
					yama[j]=32;
				for (j=120;j<136;j++)
					yama[j]=27;
				//*/
				//paiopen=8; //通常 8
				//*
				if (cpuopen) {
					paiopen=15;
				} else {
					paiopen=0;
					for (i=0;i<4;i++) {
						if (think[i][0]==0)
							paiopen|=1<<i; //牌オープン設定によっては全オープン
					}
				}
				//*/
				kakan=false;
				reachCount=0;
				ryukyokuIndex=136-14;
				rkk_4kaikan=0;
				rinshanIndex=0;
				rinshanhai=false;
				minkan=false;
				haitei=false;
				callReach=false;
				furoselect=false;
				firstRotate=4;
				rkk_4fon=0;
				haipaiCount=0;
				GameState=1;
				agariPlayer[1]=agariPlayer[2]=-1;
				//GameMode=GM_HAIPAI;
				break;
			}
			//keyEvent=-999;

		} else { //配牌アニメーション
			switch (keyEvent) {
			case FIRE:
				for (i=0;i<4;i++) {
					tehai[i][13]=13;
				}
				//keyEvent=-999;
				GameMode=GM_HAIPAI;
				return;
			}
			//keyEvent=-999;
			p=haipaiCount/4;
			q=(oya+haipaiCount)%4;
			switch (p) {
			case 0:
			case 1:
			case 2:
				tehai[q][13]+=4;
				break;
			case 3:
				tehai[q][13]++;
				break;
			case 4:
				GameMode=GM_HAIPAI;
				break;
			}
			haipaiCount++;
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//配牌後
	private void gm_haipai() {
		int i;
		
		switch (keyEvent) {
		case FIRE:
			for (i=0;i<4;i++) {
				rihai(i);
				tenpai[i]=checkTenpai(i,-1,-1);
				checkFuriten1(i);
			}
			GameState=2;
			GameMode=GM_RIHAI;
			break;
		}
		//keyEvent=-999;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//理牌
	private void gm_rihai() {
		int keyTemp=keyEvent;
		
		//if (isCompute(nowPlayer)) 
			keyTemp=FIRE;
		switch (keyTemp) {
		case FIRE:
			selectPai=-1;
			selectTCmd=TCMD_DAHAI;
			int pai=getSelectPai(nowPlayer,-1);
			//if ((t_agari[nowPlayer][pai])||((rinshanhai||haitei)&&(k_agari[nowPlayer][pai]))) {
			if (checkTumo(nowPlayer,pai)) {
				selectTCmd=TCMD_TUMOHO;
			}
			GameMode=GM_TUMO;
			break;
		}
		//keyEvent=-999;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//ツモ時
	private void gm_tumo() {
		boolean tmp;
		int i,c,n, keyTemp, pai;
		int k,pp;
		
		keyTemp=keyEvent;
		if (isCompute(nowPlayer)) {
			keyTemp=computeTCommand(nowPlayer);
		} else {
			if (keyTemp==-999) {
				switch (keyRepeat) {
				case LEFT:
					keyTemp=LEFT;
					break;
				case RIGHT:
					keyTemp=RIGHT;
					break;
				case UP:
					if (sideFlag) {
						keyTemp=LEFT;
						keyRepeat=-990;
					}
					break;
				case DOWN:
					if (sideFlag) {
						keyTemp=RIGHT;
						keyRepeat=-990;
					}
					break;
				}
			}
		}
		switch (keyTemp) {
		case LEFT:
			if (selectPai<0) selectPai=tehai[nowPlayer][13]-1;
			else if (selectPai>0) selectPai--;
			else selectPai=-1;
			break;
		case RIGHT:
			if (selectPai<0) selectPai=0;
			else if (selectPai<tehai[nowPlayer][13]-1) selectPai++;
			else selectPai=-1;
			break;
		case UP:
			selectTCmd=(selectTCmd+5)%6;
			break;
		case DOWN:
			selectTCmd=(selectTCmd+1)%6;
			break;
		case CLEAR:
			selectTCmd=0;
			break;
		case FIRE:
			switch (selectTCmd) {
			case 0: //打牌
				if (reach[nowPlayer]) {
					if (selectPai>=0) {
						break;
					}
					if (ippatsu[nowPlayer])
						ippatsu[nowPlayer]=false;
				}
				n=++sutehai[nowPlayer][0][0];
				sutehai[nowPlayer][n][0]=getSelectPai(nowPlayer,selectPai);
				sutehai[nowPlayer][n][1]=0;
				dahai=sutehai[nowPlayer][n][0];

				//テンパイチェック
				if (reach[nowPlayer]==false) {
					if (selectPai<0)
						tenpai[nowPlayer]=checkTenpai(nowPlayer,-1,-1);
					else
						tenpai[nowPlayer]=checkTenpai(nowPlayer,getSelectPai(nowPlayer,selectPai)
						                                       ,getSelectPai(nowPlayer,-1));
					checkFuriten1(nowPlayer);
				} else {
					if ((r_agari[nowPlayer][dahai])||(t_agari[nowPlayer][dahai])
					                               ||(k_agari[nowPlayer][dahai])) {
					    furiten[nowPlayer]=1;
					}
				}
				
				//次のモードへの準備等
				if (selectPai>=0) {
					tehai[nowPlayer][selectPai]=44;
				}
				frCheck=1;
				selectFCmd=0;
				furodahai=false;
				PonCall=0; ChiCall=0; RonCall=0; KanCall=0;
				
				//１巡目の処理
				if (firstRotate>0) {
					if ((27<=dahai)&&(dahai<=30)) {
						if (firstRotate==4) {
							rkk_4fon=dahai;
						} else {
							if ((rkk_4fon!=dahai)&&(rkk_4fon>0)) {
								rkk_4fon=-1;
							}
						}
					} else {
						rkk_4fon=-1;
					}
					firstRotate--;
					if ((firstRotate==0)&&(rkk_4fon>0)) { //４風連打
						if (recording) {
							int cp=think[nowPlayer][0];
							if (cp>=0) {
								stcdata[cp].Ryukyoku[RKK_4FON-1]++;
							}
						}
						rkkCount=0;
						rkk_type=RKK_4FON;
						GameMode=GM_RYUKYOKUSP;
						//keyEvent=-999;
						return;
					}
				}
				/*
				if (ryukyokuIndex-tumoIndex==1) {
					//haitei=true; //漏れがあったらここで拾う @チェック
					System.out.println("河底打牌？"+haitei);
				} //*/
				GameMode=GM_FUROCHECK;
				break;
			case 1: //立直
				if (selectPai<0)
					tmp=checkReach(nowPlayer,-1,-1);
				else
					tmp=checkReach(nowPlayer,getSelectPai(nowPlayer,selectPai)
						                         ,getSelectPai(nowPlayer,-1));
				if (tmp) {
					callCount=10;
					if (firstRotate>0) dblReach[nowPlayer]=true;
					GameMode=GM_REACH1;
				}
				/*
				if ((score[nowPlayer]>=1000)&&(menzen[nowPlayer])&&(reach[nowPlayer]==false)) {
					if (selectPai<0) 
						tmp=checkTenpai(nowPlayer,-1,-1);
					else
						tmp=checkTenpai(nowPlayer,getSelectPai(nowPlayer,selectPai)
						                         ,getSelectPai(nowPlayer,-1));
					if (tmp) {
						callCount=10;
						if (firstRotate>0) dblReach[nowPlayer]=true;
						GameMode=GM_REACH1;
					}
				} //*/
				break;
			case 2: //暗カン
				pai=getSelectPai(nowPlayer,selectPai);
				if (checkAnkan(nowPlayer,pai)) {
					if (firstRotate>0) firstRotate=-1;
					callCount=10;
					for (i=0;i<4;i++) ippatsu[i]=false;
					GameMode=GM_ANKAN1;
				}
				break;
			case 3: //加カン
				pai=getSelectPai(nowPlayer,selectPai);
				if (checkKakan(nowPlayer,pai)) {
					if (firstRotate>0) firstRotate=-1;
					callCount=10;
					for (i=0;i<4;i++) ippatsu[i]=false;
					GameMode=GM_KAKAN1;
				}
				break;
			case TCMD_RYUKYOKU: //流局
				if (firstRotate>0) {
					c=0; pai=-1;
					n=tehai[nowPlayer][13];
					for (i=0;i<n;i++) {
						pp=tehai[nowPlayer][i];
						if (pp<27) {
							k=pp%9;
							if ((k==0)||(k==8)) { //数牌 １、９
								if (pai<0) {
									pai=pp; c=1;
								} else {
									if (pp!=pai) {
										pai=pp; c++;
									}
								}
							}
						} else { //字牌
							if (pai<0) {
								pai=pp; c=1;
							} else {
								if (pp!=pai) {
									pai=pp; c++;
								}
							}
						}
					}
					pai=getSelectPai(nowPlayer,-1);
					if (pai<27) {
						pp=(pai%9)%8;
						if (pp==0) {
							k=0;
							for (i=0;i<n;i++) {
								if (tehai[nowPlayer][i]==pai) {
									k++; break;
								}
							}
							if (k==0)
								c++;
						}
					} else {
						k=0;
						for (i=0;i<n;i++) {
							if (tehai[nowPlayer][i]==pai) {
								k++; break;
							}
						}
						if (k==0)
							c++;
					}
					if (c>8) { //九種九牌成立
						if (recording) { //記録
							int cp=think[nowPlayer][0];
							if (cp>=0) {
								stcdata[cp].Ryukyoku[RKK_9SHU-1]++;
							}
						}
						paiopen|=1<<nowPlayer;
						rkkCount=0;
						rkk_type=RKK_9SHU;
						GameMode=GM_RYUKYOKUSP;
					}
				}
				break;
			case TCMD_TUMOHO: //ツモ和
				pai=getSelectPai(nowPlayer,-1);
				//通常役アリツモ和｜他役無し嶺上開花｜他役無し海底摸月
				//if ((t_agari[nowPlayer][pai])||((rinshanhai||haitei)&&(k_agari[nowPlayer][pai]))) {
				if (checkTumo(nowPlayer,pai)) {
					callCount=15;
					//paiopen|=1<<nowPlayer;
					GameMode=GM_CALLTUMO1;
				}
				break;
			}
			break;
		}
	}
	
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//次の順番へ
	private void gm_next() {
		int keyTemp=keyEvent;
		
		//if (isCompute(nowPlayer)) 
			keyTemp=FIRE;
		//keyEvent=-999;
		switch (keyTemp) {
		case FIRE:
			if ((selectPai>=0)&&(furodahai==false)) {
				tehai[nowPlayer][selectPai]=getSelectPai(nowPlayer,-1);
				//if (rinshanhai)
				//	tehai[nowPlayer][selectPai]=yama[135-rinshanIndex];
				//else
				//	tehai[nowPlayer][selectPai]=yama[tumoIndex];
				rihai(nowPlayer);
			}
			seepais[dahai]++;
			if (rinshanhai) {
				if (minkan) {
					seepais[yama[129-rinshanIndex*2]]++;
					rinshanIndex++;
					minkan=false;
				}
				rinshanhai=false;
				if ((rinshanIndex==4)&&(rkk_4kaikan<0)) { //四開槓
					if (recording) {
						int cp=think[nowPlayer][0];
						if (cp>=0) {
							stcdata[cp].Ryukyoku[RKK_4KAIKAN-1]++;
						}
					}
					//keyEvent=-999;
					rkkCount=0;
					rkk_type=RKK_4KAIKAN;
					GameMode=GM_RYUKYOKUSP;
					return;
				}
			}
			if (callReach) {
				if (recording) { //記録
					int cp=think[nowPlayer][0];
					if (cp>=0) {
						stcdata[cp].ReachCount++;
						senseki[cp].ReachCount[baKaze]++;
					}
				}
				reach[nowPlayer]=true;
				kyotaku++;
				score[nowPlayer]-=1000;
				reachCount++;
				callReach=false;
				if (reachCount==4) { //四家立直
					if (recording) {
						int cp=think[nowPlayer][0];
						if (cp>=0) {
							stcdata[cp].Ryukyoku[RKK_4REACH-1]++;
						}
					}
					paiopen=15;
					//keyEvent=-999;
					rkkCount=0;
					rkk_type=RKK_4REACH;
					GameMode=GM_RYUKYOKUSP;
					return;
				}
			}
			nowPlayer=(nowPlayer+1)%4;
			tumoIndex++;
			furiten[nowPlayer]%=2;
			if (tumoIndex<ryukyokuIndex) {
				if (ryukyokuIndex-tumoIndex==1) haitei=true;
				GameMode=GM_RIHAI;
			} else {
				int i,n,c,pp,k,j,mm,m2,f;
				nagashi=0;
				for (i=0;i<4;i++) { //流し満貫判定 捨て牌の判定
					n=sutehai[i][0][0];
					c=0;
					for (j=1;j<=n;j++) {
						if ((sutehai[i][j][1]&1)>0) {
							break;
						}
						pp=sutehai[i][j][0];
						if (pp<27) {
							pp=(pp%9)%8;
							if (pp==0) {
								c++;
							} else {
								break;
							}
						} else {
							c++;
						}
					}
					if (c==n) { //手牌がタンヤオだけかどうか
						n=tehai[i][13];
						k=0;
						for (j=0;j<n;j++) {
							pp=tehai[i][j];
							if (pp<27) {
								pp=(pp%9)%8;
								if (pp==0) {
									break;
								} else {
									k++;
								}
							} else {
								break;
							}
						}
						if (k==n) {
							f=furopai[i][0][0]; //副露牌もタンヤオのみか
							mm=0;
							for (j=1;(j<=f)&&(mm==0);j++) {
								switch (furopai[i][j][0]) {
								case FURO_CHI:
									m2=furopai[i][j][1]%9;
									if ((m2==0)||(m2==6))
										mm++; //１２３・７８９のチー
									break;
								default:
									m2=furopai[i][j][1];
									if (m2>26) {
										mm++; //字牌のポン･カン
									} else {
										if ((m2%9)%8==0)
											mm++; //１・９のポン・カン
									}
									break;
								}
							}
							if (mm==0)
								nagashi|=1<<i; //流し満貫成立
						}
					}
				}
				rkkCount=0;
				rkk_type=RKK_KOHAI;
				if (nagashi>0) rkk_type=RKK_NAGASHI;
				GameMode=GM_RYUKYOKU1;
			}
			break;
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//副露決定
	private void gm_callcheck() {
		int i;
		callCount=10;
		if (RonCall>0) {
			callCount=15;
			GameMode=GM_CALLRON1;
		} else if (PonCall>0) {
			if (firstRotate>0) firstRotate=-1;
			for (i=0;i<4;i++) ippatsu[i]=false;
			callPlayer=PonCall-1;
			GameMode=GM_PON1;
		} else if (KanCall>0) {
			if (firstRotate>0) firstRotate=-1;
			for (i=0;i<4;i++) ippatsu[i]=false;
			callPlayer=KanCall-1;
			GameMode=GM_MINKAN1;
		} else if (ChiCall>0) {
			if (firstRotate>0) firstRotate=-1;
			for (i=0;i<4;i++) ippatsu[i]=false;
			callPlayer=ChiCall-1;
			GameMode=GM_CHI1;
		} else {
			GameMode=GM_NEXT;
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//各家の副露確認
	private void gm_furocheck() {
		int k,n,keyTemp;
	
		n=(nowPlayer+frCheck)%4;
		k=0;
		keyTemp=keyEvent;
		if (checkFuro(n,dahai)) {
			if (isCompute(n)) {
				keyTemp=computeFCommand(n);
			} else {
				if (furoselect==false) {
					furoselect=true;
					//if (r_agari[n][dahai])
					if (checkRon(n,dahai))
						selectFCmd=FCMD_RON;
				}
			}
		} else {
			keyTemp=FIRE;
			selectFCmd=FCMD_PASS;
		}
		//keyEvent=-999;
		switch (keyTemp) {
		case UP:
			selectFCmd=(selectFCmd+4)%5;
			break;
		case DOWN:
			selectFCmd=(selectFCmd+1)%5;
			break;
		case CLEAR:
			selectFCmd=FCMD_PASS;
			break;
		case FIRE:
			switch (selectFCmd) {
			case FCMD_PASS: //パス
				k=1;
				if ((r_agari[n][dahai])||(t_agari[n][dahai])||(k_agari[n][dahai])) {
					if (reach[n])
						furiten[n]|=1;
					else
						furiten[n]|=2;
				}
				break;
			case FCMD_CHI: //チー
				if (checkChi(n,dahai)>0) {
					ChiCall=n+1;
					k=1;
				}
				break;
			case FCMD_PON: //ポン
				if (checkPon(n,dahai)>1) {
					PonCall=n+1;
					k=1;
				}
				break;
			case FCMD_KAN: //カン
				if (checkKan(n,dahai)) {
					KanCall=n+1;
					k=1;
				}
				break;
			case FCMD_RON: //ロン和
				if (checkRon(n,dahai)) { //(r_agari[n][dahai])&&(furiten[n]==0)) {
					RonCall |= 1<<n;
					k=1;
				}
				break;
			}
			if (k>0) {
				frCheck++;
				selectFCmd=FCMD_PASS;
				furoselect=false;
				if (frCheck==4) {
					GameMode=GM_CALLCHECK;
				}
			}
			break;
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//チーの処理
	private void gm_chi1() { //チーコール
		int n;
		if (callCount==0) {
			if ((selectPai>=0)&&(furodahai==false)) {
				tehai[nowPlayer][selectPai]=getSelectPai(nowPlayer,-1);
				//if (rinshanhai)
				//	tehai[nowPlayer][selectPai]=yama[135-rinshanIndex];
				//else
				//	tehai[nowPlayer][selectPai]=yama[tumoIndex];
				rihai(nowPlayer);
			}
			if (rinshanhai) {
				if (minkan) {
					seepais[yama[129-rinshanIndex*2]]++;
					rinshanIndex++;
					minkan=false;
				}
				rinshanhai=false;
			}
			if (callReach) {
				if (recording) { //記録
					int cp=think[nowPlayer][0];
					if (cp>=0) {
						stcdata[cp].ReachCount++;
						senseki[cp].ReachCount[baKaze]++;
					}
				}
				reach[nowPlayer]=true;
				kyotaku++;
				score[nowPlayer]-=1000;
				reachCount++;
				callReach=false;
			}
			n=sutehai[nowPlayer][0][0]; //捨て牌副露表示
			sutehai[nowPlayer][n][1]|=1;
			nowPlayer=callPlayer;
			if (recording) { //記録
				int cp=think[nowPlayer][0];
				if (cp>=0) {
					stcdata[cp].ChiCount++;
				}
			}
			if (menzen[nowPlayer]) { //門前じゃなくなる
				menzen[nowPlayer]=false;
				if (recording) { //記録
					int cp=think[nowPlayer][0];
					if (cp>=0) {
						stcdata[cp].FuroCount++;
						senseki[cp].FuroCount[baKaze]++;
					}
				}
			}
			frCheck=1;
			selectPai=0;
			selectChi1=0; selectChi2=0;
			set_selectChi(nowPlayer,0);
			GameMode=GM_CHI2;
		}
		callCount--;
	}
	private void set_selectChi(int p, int sel) {
		int k=checkChi(p, dahai);
		int m=k&3;
		int i,c=0;
		int n=tehai[p][13];
		int pai1=0,pai2=0;
		if (m==0) return;
		if ((k&(1<<3))>0) {
			if (c==sel) {
				pai1=dahai-2;
				pai2=dahai-1;
			}
			c++;
		}
		if ((k&(1<<4))>0) {
			if (c==sel) {
				pai1=dahai-1;
				pai2=dahai+1;
			}
			c++;
		}
		if ((k&(1<<5))>0) {
			if (c==sel) {
				pai1=dahai+1;
				pai2=dahai+2;
			}
		}
		for (i=0;i<n;i++) {
			if (pai1==tehai[p][i]) {
				selectChi1=i;
				break;
			}
		}
		for (i=0;i<n;i++) {
			if (pai2==tehai[p][i]) {
				selectChi2=i;
				break;
			}
		}
	}
	private void gm_chi2() { //チー牌選択
		int keyTemp;
		int k=checkChi(nowPlayer,dahai);
		int m=k&3;
		int c1,c2,c3,i,n,p1,p2;
		if (m>1) { //選択
			keyTemp=keyEvent;
			if (isCompute(nowPlayer)) { //コンピュータの選択
				if (think[nowPlayer][2]==0) {
					keyTemp=FIRE;
				} else if (think[nowPlayer][2]>0) {
					think[nowPlayer][2]--;
					keyTemp=RIGHT;
				} else {
					keyTemp=FIRE;
				}
			} else { //サイドキーでの選択
				if (sideFlag) {
					switch (keyEvent) {
					case UP:
						keyTemp=LEFT;
						break;
					case DOWN:
						keyTemp=RIGHT;
						break;
					}
				}
			}
			switch (keyTemp) {
			case LEFT:
				selectPai=(selectPai+m-1)%m;
				set_selectChi(nowPlayer,selectPai);
				break;
			case RIGHT:
				selectPai=(selectPai+1)%m;
				set_selectChi(nowPlayer,selectPai);
				break;
			case FIRE:
				kuikae[0]=dahai; kuikae[1]=-1; //現物喰い替えの制限
				c3=dahai;
				p1=tehai[nowPlayer][selectChi1];
				p2=tehai[nowPlayer][selectChi2];
				if (p1<c3)
					c3=p1;
				if ((p1<dahai)&&(p2<dahai)) {
					if ((dahai%9)>2) kuikae[1]=dahai-3; //スジ(両面)喰い替えの制限
				} else if ((p1>dahai)&&(p2>dahai)) {
					if ((dahai%9)<6) kuikae[1]=dahai+3;
				}
				n=tehai[nowPlayer][13];
				tehai[nowPlayer][selectChi2]=tehai[nowPlayer][n-1];
				tehai[nowPlayer][selectChi1]=tehai[nowPlayer][n-2];
				tehai[nowPlayer][13]-=2;
				k=++furopai[nowPlayer][0][0];
				furopai[nowPlayer][k][0]=FURO_CHI;
				furopai[nowPlayer][k][1]=c3;
				for (i=0;i<3;i++)
					seepais[c3+i]++;
				rihai(nowPlayer);
				selectPai=0;
				GameMode=GM_FURODAHAI;
				break;
			}
			//keyEvent=-999;
		
		} else { //選択の必要性が無いとき
			n=tehai[nowPlayer][13];
			kuikae[0]=dahai; kuikae[1]=-1; //現物喰い替え牌の制限
			if ((k&(1<<3))>0) {
				c1=dahai-2; c2=dahai-1; c3=c1;
				if ((dahai%9)>2) kuikae[1]=dahai-3; //スジ(両面)喰い替えの制限
			} else if ((k&(1<<4))>0) {
				c1=dahai-1; c2=dahai+1; c3=c1;
			} else {
				c1=dahai+1; c2=dahai+2; c3=dahai;
				if ((dahai%9)<6) kuikae[1]=dahai+3; //スジ(両面)喰い替えの制限
			}
			for (i=0;i<n;i++) {
				if (tehai[nowPlayer][i]==c2) {
					tehai[nowPlayer][i]=tehai[nowPlayer][n-1];
					break;
				}
			}
			for (i=0;i<n;i++) {
				if (tehai[nowPlayer][i]==c1) {
					tehai[nowPlayer][i]=tehai[nowPlayer][n-2];
					break;
				}
			}
			for (i=0;i<3;i++)
				seepais[c3+i]++;
			tehai[nowPlayer][13]-=2;
			k=++furopai[nowPlayer][0][0];
			furopai[nowPlayer][k][0]=FURO_CHI;
			furopai[nowPlayer][k][1]=c3;
			rihai(nowPlayer);
			selectPai=0;
			GameMode=GM_FURODAHAI;
			//keyEvent=-999;
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	
	//ポンの処理
	private void gm_pon1() { //ポンコール
		int n;
		if (callCount==0) {
			if ((selectPai>=0)&&(furodahai==false)) {
				tehai[nowPlayer][selectPai]=getSelectPai(nowPlayer,-1);
				//if (rinshanhai)
				//	tehai[nowPlayer][selectPai]=yama[135-rinshanIndex];
				//else
				//	tehai[nowPlayer][selectPai]=yama[tumoIndex];
				rihai(nowPlayer);
			}
			if (rinshanhai) {
				if (minkan) {
					seepais[yama[129-rinshanIndex*2]]++;
					rinshanIndex++;
					minkan=false;
				}
				rinshanhai=false;
			}
			if (callReach) {
				if (recording) { //記録
					int cp=think[nowPlayer][0];
					if (cp>=0) {
						stcdata[cp].ReachCount++;
						senseki[cp].ReachCount[baKaze]++;
					}
				}
				reach[nowPlayer]=true;
				kyotaku++;
				score[nowPlayer]-=1000;
				reachCount++;
				callReach=false;
			}
			n=sutehai[nowPlayer][0][0]; //捨て牌副露表示
			sutehai[nowPlayer][n][1]|=1;
			nowPlayer=callPlayer;
			if (recording) { //記録
				int cp=think[nowPlayer][0];
				if (cp>=0) {
					stcdata[cp].PonCount++;
				}
			}
			if (menzen[nowPlayer]) { //門前じゃなくなる
				menzen[nowPlayer]=false;
				if (recording) { //記録
					int cp=think[nowPlayer][0];
					if (cp>=0) {
						stcdata[cp].FuroCount++;
						senseki[cp].FuroCount[baKaze]++;
					}
				}
			}
			GameMode=GM_PON2;
		}
		callCount--;
	}
	private void gm_pon2() { //赤を含むときは選択できるように変更する
		int n=tehai[nowPlayer][13];
		int i,k, c=0;
		for (i=0;i<n;i++) {
			if (tehai[nowPlayer][i]==dahai) {
				c++;
				tehai[nowPlayer][i]=tehai[nowPlayer][n-c];
			}
			if (c==2) break;
		}
		seepais[dahai]+=3;
		tehai[nowPlayer][13]-=2;
		k=++furopai[nowPlayer][0][0];
		furopai[nowPlayer][k][0]=FURO_PON;
		furopai[nowPlayer][k][1]=dahai;
		rihai(nowPlayer);
		kuikae[0]=dahai; kuikae[1]=-1; //現物喰い替えの制限
		selectPai=0;
		GameMode=GM_FURODAHAI;
		//keyEvent=-999;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//ポン･チー共通打牌処理
	private void gm_furodahai() {
		int n,k, keyTemp,pai;
		
		keyTemp=keyEvent;
		if (isCompute(nowPlayer)) {
			keyTemp=computeFDahai(nowPlayer);
		} else { 
			if (sideFlag) { //サイドキーで選択
				switch (keyEvent) {
				case UP:
					keyTemp=LEFT;
					break;
				case DOWN:
					keyTemp=RIGHT;
					break;
				}
			}
		}
		switch (keyTemp) {
		case LEFT:
			if (selectPai>0) selectPai--;
			else selectPai=tehai[nowPlayer][13]-1;
			break;
		case RIGHT:
			if (selectPai<tehai[nowPlayer][13]-1) selectPai++;
			else selectPai=0;
			break;
		case FIRE:
			pai=getSelectPai(nowPlayer,selectPai);
			if ((pai!=kuikae[0])&&(pai!=kuikae[1])) {
				k=tehai[nowPlayer][13];
				n=++sutehai[nowPlayer][0][0];
				sutehai[nowPlayer][n][0]=tehai[nowPlayer][selectPai];
				sutehai[nowPlayer][n][1]=0;
				tehai[nowPlayer][selectPai]=tehai[nowPlayer][k-1];
				tehai[nowPlayer][13]--;
				rihai(nowPlayer);
				tenpai[nowPlayer]=checkTenpai(nowPlayer,-1,-1);
				checkFuriten1(nowPlayer);
				dahai=sutehai[nowPlayer][n][0];
				frCheck=1; selectFCmd=0; furodahai=true;
				PonCall=0; ChiCall=0; RonCall=0; KanCall=0;
				GameMode=GM_FUROCHECK;
			}
			break;
		}
		//keyEvent=-999;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//明カンの処理
	private void gm_minkan1() { //明カンコール
		int n;
		if (callCount==0) {
			if ((selectPai>=0)&&(furodahai==false)) {
				tehai[nowPlayer][selectPai]=getSelectPai(nowPlayer,-1);
				//if (rinshanhai)
				//	tehai[nowPlayer][selectPai]=yama[135-rinshanIndex];
				//else
				//	tehai[nowPlayer][selectPai]=yama[tumoIndex];
				rihai(nowPlayer);
			}
			if (rinshanhai) {
				if (minkan) {
					seepais[yama[129-rinshanIndex*2]]++;
					rinshanIndex++;
					minkan=false;
				}
				rinshanhai=false;
			}
			if (callReach) {
				if (recording) { //記録
					int cp=think[nowPlayer][0];
					if (cp>=0) {
						stcdata[cp].ReachCount++;
						senseki[cp].ReachCount[baKaze]++;
					}
				}
				reach[nowPlayer]=true;
				kyotaku++;
				score[nowPlayer]-=1000;
				reachCount++;
				callReach=false;
			}
			n=sutehai[nowPlayer][0][0]; //捨て牌の副露表示
			sutehai[nowPlayer][n][1]|=1;
			nowPlayer=callPlayer;
			if (recording) { //記録
				int cp=think[nowPlayer][0];
				if (cp>=0) {
					stcdata[cp].MinkanCount++;
				}
			}
			if (menzen[nowPlayer]) { //門前じゃなくなる
				menzen[nowPlayer]=false;
				if (recording) { //記録
					int cp=think[nowPlayer][0];
					if (cp>=0) {
						stcdata[cp].FuroCount++;
						senseki[cp].FuroCount[baKaze]++;
					}
				}
			}
			GameMode=GM_MINKAN2;
		}
		callCount--;
	}
	private void gm_minkan2() {
		int n=tehai[nowPlayer][13];
		int i,k, c=0;
		for (i=0;i<n;i++) {
			if (tehai[nowPlayer][i]==dahai) {
				c++;
				tehai[nowPlayer][i]=tehai[nowPlayer][n-c];
			}
			if (c==3) break;
		}
		seepais[dahai]+=4;
		tehai[nowPlayer][13]-=3;
		k=++furopai[nowPlayer][0][0];
		furopai[nowPlayer][k][0]=FURO_MINKAN;
		furopai[nowPlayer][k][1]=dahai;
		rihai(nowPlayer);
		if (rkk_4kaikan>=0) {
			rkk_4kaikan|=1<<nowPlayer;
			if (rkk_4kaikan!=(1<<nowPlayer))
				rkk_4kaikan=-1;
		}
		//selectPai=0;
		//GameMode=GM_RINSHAN;
		ryukyokuIndex--;
		rinshanhai=true;
		minkan=true; //明槓は後乗り
		GameMode=GM_RIHAI;
		//keyEvent=-999;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//暗カンの処理
	private void gm_ankan1() {
		if (callCount==0) {
			GameMode=GM_ANKAN2;
		}
		callCount--;
	}
	private void gm_ankan2() {
		int pai=getSelectPai(nowPlayer,selectPai);
		int n=tehai[nowPlayer][13];
		int i,k,j,pp;
		k=0;
		i=0;
		while (i<n) {
			if (tehai[nowPlayer][i]==pai) {
				for (j=i;j<n-1;j++) {
					tehai[nowPlayer][j]=tehai[nowPlayer][j+1];
				}
				tehai[nowPlayer][n-1]=-1;
				k++;
			} else {
				i++;
			}
			//System.out.println("i="+i+",n="+n+",k="+k);
		}
		
		/*
		for (i=0;i<n;i++) {
			if (tehai[nowPlayer][n-1-i]==pai) { //このあたりがおかしいと思われ＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
				
				
				for (j=n-1;j>n-1-i;j--) {
					pp=tehai[nowPlayer][j];
					if ((pp!=pai)&&(pp>=0)) {
						tehai[nowPlayer][n-1-i]=pp;
						tehai[nowPlayer][j]=pai;
						k++;
						break;
					}
				}
				//j++;
				//tehai[nowPlayer][n-1-i]=tehai[nowPlayer][n-j];
			}
		}
		//*/
		if (k==4) {
			for (i=0;i<n;i++) {
				if (tehai[nowPlayer][i]==-1) {
					tehai[nowPlayer][i]=getSelectPai(nowPlayer,-1);
					break;
				}
			}
			//tehai[nowPlayer][n-3]=getSelectPai(nowPlayer,-1);
		}
		if (rinshanhai) { //カン後の嶺上ツモ直後のカンの場合
			if (minkan) {
				seepais[yama[129-rinshanIndex*2]]++;
				rinshanIndex++;
				minkan=false;
			}
		}
		seepais[pai]+=4;
		tehai[nowPlayer][13]-=3;
		k=++furopai[nowPlayer][0][0];
		furopai[nowPlayer][k][0]=FURO_ANKAN;
		furopai[nowPlayer][k][1]=pai;
		rihai(nowPlayer);
		if (rkk_4kaikan>=0) {
			rkk_4kaikan|=1<<nowPlayer;
			if (rkk_4kaikan!=(1<<nowPlayer))
				rkk_4kaikan=-1;
		}
		ryukyokuIndex--;
		rinshanhai=true;
		seepais[yama[129-rinshanIndex*2]]++; //暗槓は即乗り
		rinshanIndex++;
		if (recording) { //記録
			int cp=think[nowPlayer][0];
			if (cp>=0) {
				stcdata[cp].AnkanCount++;
			}
		}
		GameMode=GM_RIHAI;
		//keyEvent=-999;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//加カンの処理
	private void gm_kakan1() {
		if (callCount==0) {
			int i;
			int pai=getSelectPai(nowPlayer,selectPai);
			int f=furopai[nowPlayer][0][0];
			if (selectPai>=0) {
				tehai[nowPlayer][selectPai]=44;
			}
			for (i=1;i<=f;i++) {
				if (furopai[nowPlayer][i][1]==pai) {
					furopai[nowPlayer][i][0]=FURO_MINKAN;
					break;
				}
			}
			if (rinshanhai) {
				if (minkan) {
					seepais[yama[129-rinshanIndex*2]]++;
					rinshanIndex++;
					minkan=false;
				}
			}
			seepais[pai]++;
			if (recording) { //記録
				int cp=think[nowPlayer][0];
				if (cp>=0) {
					stcdata[cp].KakanCount++;
				}
			}
			kakan=true; frCheck=1; kakanpai=pai;
			selectFCmd=FCMD_RON; furodahai=true; RonCall=0;
			GameMode=GM_CHANKAN1;
			//keyEvent=-999;
		}
		callCount--;
	}
	private void gm_kakan2() {
		//int pai=getSelectPai(nowPlayer,selectPai);
		//int n=tehai[nowPlayer][13];
		//int f=furopai[nowPlayer][0][0];
		//int i;
		if (RonCall==0) {
			if (selectPai>=0) {
				tehai[nowPlayer][selectPai]=getSelectPai(nowPlayer,-1);
				rihai(nowPlayer);
			}
			if (rkk_4kaikan>=0) {
				rkk_4kaikan|=1<<nowPlayer;
				if (rkk_4kaikan!=(1<<nowPlayer))
					rkk_4kaikan=-1;
			}
			ryukyokuIndex--; kakan=false;
			rinshanhai=true;
			minkan=true; //明槓は後乗り
			GameMode=GM_RIHAI;
			//keyEvent=-999;
		} else {
			callCount=15;
			dahai=kakanpai;
			GameMode=GM_CALLRON1;
		}
	}
	private void gm_chankan1() {
		int n, keyTemp;
		keyTemp=keyEvent;
		n=(nowPlayer+frCheck)%4;
		if (checkRon(n,kakanpai)) {
			if (isCompute(n)) {
				if (think[n][0]<0)
					selectFCmd=FCMD_PASS;
				else
					selectFCmd=FCMD_RON;
				keyTemp=FIRE;
			} else {
				if (furoselect==false) {
					furoselect=true;
					//selectFCmd=FCMD_RON;
				}
			}
		} else {
			selectFCmd=FCMD_PASS;
			keyTemp=FIRE;
		}
		switch (keyTemp) {
		case UP:
		case DOWN:
			selectFCmd=4-selectFCmd;
			break;
		case FIRE:
			switch (selectFCmd) {
			case FCMD_PASS:
				if ((r_agari[n][kakanpai])||(t_agari[n][kakanpai])||(k_agari[n][kakanpai])) {
					if (reach[n])
						furiten[n]|=1;
					else
						furiten[n]|=2;
				}
				break;
			case FCMD_RON:
				RonCall|=1<<n;
				break;
			}
			frCheck++;
			selectFCmd=FCMD_RON;
			furoselect=false;
			if (frCheck==4) {
				GameMode=GM_KAKAN2;
			}
			break;
		}
		//keyEvent=-999;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//特別流局の処理
	private void gm_ryukyokusp() {
		int i;
		if (keyEvent==FIRE)
			rkkCount=30;
		if (rkkCount==30) { //現在連荘のみ
			for (i=0;i<4;i++) {
				tehai[i][13]=0;
				furopai[i][0][0]=0;
				sutehai[i][0][0]=0;
				reach[i]=false;
				furiten[i]=0;
			}
			renchan[oya]++;
			GameState=0;
			nowPlayer=oya;
			honba++;     //*
			haipaiCount=-1;
			GameMode=GM_START;
			//keyEvent=-999;
		}
		//rkkCount++;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//通常の流局
	private void gm_ryukyoku1() {
		int i;
		if (keyEvent==FIRE) {
			rkkCount=30;
			for (i=0;i<4;i++) {
				if (tenpai[i]) {
					paiopen|=1<<i;
				}
			}
		}
		//keyEvent=-999;
		if (rkkCount==10) {
			for (i=0;i<4;i++) {
				if (tenpai[i]) {
					paiopen|=1<<i;
				}
			}
		}
		if (rkkCount==30) {
			int c=0,pp,j,k,n,p;
			

			if (nagashi>0) { //流し満貫の処理
				c=0;
				for (i=0;i<4;i++) {
					addscore[i]=0;
					if ((nagashi&(1<<i))>0)
						c++;
				}
				//三家和の判定
				if (c>2) {
					if (recording) {
						int cp;
						for (i=0;i<4;i++) {
							if ((nagashi&(1<<i))>0) {
								cp=think[i][0];
								if (cp>=0) {
									stcdata[cp].Ryukyoku[RKK_3RON-1]++;
								}
							}
						}
					}
					rkkCount=0;
					rkk_type=RKK_3RON;
					GameMode=GM_RYUKYOKUSP;
					//keyEvent=-999;
					return;
				}

				c=1;
				for (i=0;i<4;i++) { //流し満貫の点数計算
					p=(oya+i)%4;
					if ((nagashi&(1<<p))>0) {
						if (recording) {
							int cp=think[p][0];
							if (cp>=0) {
								stcdata[cp].Ryukyoku[RKK_NAGASHI-1]++;
							}
						}
						if (p==oya) {
							addscore[p]+=12000;
							if (c>0) {
								addscore[p]+=kyotaku*1000+honba*300;
								c=0;
							}
							for (j=1;j<4;j++) {
								k=(p+j)%4;
								addscore[k]-=4000+honba*100;
							}
						} else {
							addscore[p]+=8000;
							if (c>0) {
								addscore[p]+=kyotaku*1000+honba*300;
								c=0;
							}
							for (j=1;j<4;j++) {
								k=(p+j)%4;
								if (k==oya)
									addscore[k]-=4000+honba*100;
								else
									addscore[k]-=2000+honba*100;
							}
						}
					}
				}
				kyotaku=0;
				rkkCount=0;
				GameMode=GM_NAGASHI;
				return;
			}			
			
			c=0;
			for (i=0;i<4;i++) { //荒牌流局の点数計算等
				if (tenpai[i]) c++;
				//addscore[i]=0;
			}
			if (recording) {
				for (i=0;i<4;i++) {
					int cp=think[i][0];
					if (cp>=0) {
						if (tenpai[i])
							stcdata[cp].KohaiRyukyoku[1]++;
						else
							stcdata[cp].KohaiRyukyoku[0]++;
					}
				}
			}
			if ((0<c)&&(c<4))
				for (i=0;i<4;i++) {
					if (tenpai[i])
						addscore[i]=3000/c;
					else
						addscore[i]=-3000/(4-c);
				}
			rkkCount=0;
			GameMode=GM_RYUKYOKU2;
		} else {
			rkkCount++;
		}
	}
	private void gm_ryukyoku2() {
		int i;
		if (keyEvent==FIRE) {
			rkkCount=30;
		}
		//keyEvent=-999;
		if (rkkCount==30) {
			for (i=0;i<4;i++) {
				score[i]+=addscore[i];
				tehai[i][13]=0;
				furopai[i][0][0]=0;
				sutehai[i][0][0]=0;
				reach[i]=false;
				furiten[i]=0;
			}
			GameState=0;
			if (rule_tobi) {
				int ffg=0;
				for (i=0;i<4;i++) {
					if (score[i]<0) { 
						ffg++;
						if (recording) {
							int cp=think[i][0];
							if (cp>=0) {
								stcdata[cp].TobiCount++;
								senseki[cp].TobiCount++;
							}
						}
					}
				}
				if (ffg>0) { //トビ終了
					fnCount=0;
					GameMode=GM_FINISH1;
					return;
				}
			}

			//点数が3万点を超えるプレーヤーを探す
			int fg=0;
			for (i=0;i<4;i++)
				if (score[i]>=30000) fg=1;
			
			if (rule_SuddenDeath) { //サドンデスルール適用か
				switch (rule_PlayLong) {
				case 0: //東風戦
					if ((baKaze==1)&&(fg>0)) { //南場で３万超えがいるなら
						fnCount=0; GameMode=GM_FINISH1; return;
					}
					break;
				case 1: //半荘戦
					if ((baKaze==2)&&(fg>0)) { //西場で３万超えがいるなら
						fnCount=0; GameMode=GM_FINISH1; return;
					}
					break;
				}
			}

			if (tenpai[oya]==false) { //輪荘
				oya=(oya+1)%4;
				kyoku=(kyoku+1)%4;


				if (kyoku==3) { //４局目
					switch (rule_PlayLong) {
					case 0: //東風戦
						alllast=true; //オーラス
						break;
					case 1: //半荘戦
						if (baKaze>0) 
							alllast=true; //南場（西場）ならオーラス
						break;
					case 2: //一荘戦
						if (baKaze==3)
							alllast=true; //北場ならオーラス
						break;
					}
				}

				if (kyoku==0) { //４局目が終了

					baKaze=(baKaze+1)%4;   //次局へ、終局の判定しとけや


					switch (rule_PlayLong) {
					case 0: //東風戦
						if (rule_Continue==false) { //南入なし
							fnCount=0; GameMode=GM_FINISH1; return;
						} else { //南入あり
							if (fg>0) { //3万超えいる
								fnCount=0; GameMode=GM_FINISH1; return;
							} else { //3万超えいない
								if (baKaze==2) { //西入はしない
									fnCount=0; GameMode=GM_FINISH1; return;
								} else { //南入
									if (rule_SuddenDeath==false) { //サドンデスしない
										alllast=false;
									}
									if (recording) {
										for (i=0;i<4;i++) {
											int cp=think[i][0];
											if (cp>=0) {
												senseki[cp].ContinueCount++;
											}
										}
									}
								}
							}
						}
						break;
					case 1: //半荘戦
						if (baKaze>1) { //南場終了以降
							if (rule_Continue==false) { //南入なし
								fnCount=0; GameMode=GM_FINISH1; return;
							} else { //西入あり
								if (fg>0) { //3万超えいる
									fnCount=0; GameMode=GM_FINISH1; return;
								} else { //3万超えいない
									if (baKaze==3) { //北入はしない
										fnCount=0; GameMode=GM_FINISH1; return;
									} else { //西入
										if (rule_SuddenDeath==false) { //サドンデスしない
											alllast=false;
										}
										if (recording) {
											for (i=0;i<4;i++) {
												int cp=think[i][0];
												if (cp>=0) {
													senseki[cp].ContinueCount++;
												}
											}
										}
									}
								}
							}
						}
						break;
					case 2: //一荘戦
						if (baKaze==0) { //北場終了
							fnCount=0; GameMode=GM_FINISH1; return;
						}
						break;
					}
				}
			} else { //連荘
				renchan[oya]++;
			}
			nowPlayer=oya;
			honba++;     //*
			haipaiCount=-1;
			GameMode=GM_START;
			//keyEvent=-999;
		}
		//rkkCount++;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//リーチの処理
	private void gm_reach1() {
		int n;
		if (callCount==0) {
			n=++sutehai[nowPlayer][0][0];
			sutehai[nowPlayer][n][0]=getSelectPai(nowPlayer,selectPai);
			sutehai[nowPlayer][n][1]=2;
			dahai=sutehai[nowPlayer][n][0];
			
			callReach=true;
			ippatsu[nowPlayer]=true;

			tenpai[nowPlayer]=true;
			
			checkFuriten1(nowPlayer);

			int i;
			for (i=0;i<34;i++) {
				if (k_agari[nowPlayer][i]) {
					r_agari[nowPlayer][i]=true;
					t_agari[nowPlayer][i]=true;
				}
				if (t_agari[nowPlayer][i])
					r_agari[nowPlayer][i]=true;
			}
			
			//次のモードへの準備等
			if (selectPai>=0) {
				tehai[nowPlayer][selectPai]=44;
			}
			frCheck=1;
			selectFCmd=0;
			furodahai=false;
			PonCall=0; ChiCall=0; RonCall=0; KanCall=0;
			
			//１巡目の処理
			if (firstRotate>0) {
				if ((27<=dahai)&&(dahai<=30)) {
					if (firstRotate==4) {
						rkk_4fon=dahai;
					} else {
						if ((rkk_4fon!=dahai)&&(rkk_4fon>0)) {
							rkk_4fon=-1;
						}
					}
				} else {
					rkk_4fon=-1;
				}
				firstRotate--;
				if ((firstRotate==0)&&(rkk_4fon>0)) { //四風連打
					if (recording) {
						int cp=think[nowPlayer][0];
						if (cp>=0) {
							stcdata[cp].Ryukyoku[RKK_4FON-1]++;
						}
					}
					rkkCount=0;
					rkk_type=RKK_4FON;
					GameMode=GM_RYUKYOKUSP;
					//keyEvent=-999;
					return;
				}
			}
			GameMode=GM_FUROCHECK;
		}
		callCount--;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//ツモ和
	private void gm_calltumo1() {
		if (callCount==10) {
			paiopen|=1<<nowPlayer;
		}
		if (callCount==0) {
			if (reach[nowPlayer])
				GameState=3;
			agCount=10;
			agariPlayer[0]=1;
			agariPlayer[1]=nowPlayer;
			agaripai=getSelectPai(nowPlayer,-1);
			agari_type=AG_TUMO;
			GameMode=GM_AGARI1;
		}
		callCount--;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//ロン和
	private void gm_callron1() {
		if (callCount==10) {
			int k,i;
			for (i=0;i<4;i++) {
				k=(i+nowPlayer)%4;
				if ((RonCall&(1<<k))>0) {
					paiopen|=1<<k;
				} 
			}
		}
		if (callCount==0) {
			int c,i,k,n;
			c=0;
			for (i=0;i<4;i++)
				if ((RonCall&(1<<i))>0) c++;
			if (c==3) { //三家和
				if (recording) {
					int cp;
					for (i=0;i<4;i++) {
						if ((RonCall&(1<<i))>0) {
							cp=think[i][0];
							if (cp>=0) {
								stcdata[cp].Ryukyoku[RKK_3RON-1]++;
							}
						}
					}
				}
				rkkCount=0;
				rkk_type=RKK_3RON;
				GameMode=GM_RYUKYOKUSP;
				//keyEvent=-999;
				return;
			}
			n=0;
			for (i=0;i<4;i++) {
				k=(i+nowPlayer)%4;
				if ((RonCall&(1<<k))>0) {
					n++;
					agariPlayer[n]=k;
					if (reach[k])
						GameState=3;
				} 
			}
			agCount=15;
			agariPlayer[0]=n;
			agaripai=dahai;
			agari_type=AG_RON;
			GameMode=GM_AGARI1;
		}
		callCount--;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//あがりの処理
	private void gm_agari1() {
		//keyEvent=-999;
		if (agCount==0) {
			int n=agariPlayer[0];
			int p=agariPlayer[n];
			int bp,i,sc;
	
			checkYaku(p,agaripai,agari_type);
			if (yk_yakuman>0) { //基本点数の計算 (役満)
				bp=8000*yk_yakuman;
			} else { //基本点数の計算 (役満以外)
				switch (yk_han) {
				case 1:
				case 2:
				case 3:
				case 4:
					bp=yk_fu*(1<<(yk_han+2));
					if (bp>2000) bp=2000;
					break;
				case 5: //満貫
					bp=2000;
					break;
				case 6: //ハネ満
				case 7:
					bp=3000;
					break;
				case 8: //倍満
				case 9:
				case 10:
					bp=4000;
					break;
				case 11: //３倍満
				case 12:
					bp=6000;
					break;
				default: //数え役満
					bp=8000;
					break;
				}
			}
			switch (agari_type) {
			case AG_RON:
				if (p==oya)
					sc=((bp*6+90)/100)*100;
				else
					sc=((bp*4+90)/100)*100;
				yk_score=sc;
				addscore[p]+=sc;
				addscore[nowPlayer]-=sc;
				if (n==1) {
					addscore[p]+=kyotaku*1000+honba*300;
					addscore[nowPlayer]-=honba*300;
				}
				if (recording) { //点数の記録(ロン)
					int cp=think[p][0]; 
					if (cp>=0) {
						stcdata[cp].RonTotalScore+=yk_score; //addscore[p];
						//if (addscore[p]>stcdata[cp].RonMaxScore)
						if (yk_score>stcdata[cp].RonMaxScore)
							stcdata[cp].RonMaxScore=yk_score; //addscore[p];
						senseki[cp].HoraCount[baKaze]++;
						senseki[cp].HoraTotalScore[baKaze]+=yk_score;
					}
					cp=think[nowPlayer][0]; //点数の記録(放銃者)
					if (cp>=0) { 
						if (reach[p]) {
							if (reach[nowPlayer]) {
								stcdata[cp].HojuReachCount[1]++;
								senseki[cp].HojuReachCount[baKaze][1]++;
							} else {
								stcdata[cp].HojuReachCount[0]++;
								senseki[cp].HojuReachCount[baKaze][0]++;
							}
						}else if (menzen[p]) {
							if (reach[nowPlayer]) {
								stcdata[cp].HojuYamiCount[1]++;
								senseki[cp].HojuYamiCount[baKaze][1]++;
							} else {
								stcdata[cp].HojuYamiCount[0]++;
								senseki[cp].HojuYamiCount[baKaze][0]++;
							}
						} else {
							if (reach[nowPlayer]) {
								stcdata[cp].HojuFuroCount[1]++;
								senseki[cp].HojuFuroCount[baKaze][1]++;
							} else {
								stcdata[cp].HojuFuroCount[0]++;
								senseki[cp].HojuFuroCount[baKaze][0]++;
							}
						}
						if (yk_yakuman>0) {
							if (reach[nowPlayer])
								stcdata[cp].YmHojuCount[1]++;
							else
								stcdata[cp].YmHojuCount[0]++;
						}
						if (n==1) {
							if (RonCall!=(1<<p)) { //ダブロン
								int dblsc=-(addscore[nowPlayer]+honba*300);
								if (reach[nowPlayer]) {
									stcdata[cp].DblRonHojuCount[1]++;
									senseki[cp].DblRonHojuCount[baKaze][1]++;
								} else {
									stcdata[cp].DblRonHojuCount[0]++;
									senseki[cp].DblRonHojuCount[baKaze][0]++;
								}
								stcdata[cp].DblRonHojuTotalScore+=dblsc;
								if (dblsc>stcdata[cp].DblRonHojuMaxScore) {
									stcdata[cp].DblRonHojuMaxScore=dblsc;
								}
							}
						}
						//if (n==1) {
							stcdata[cp].HojuTotalScore+=yk_score; //addscore[nowPlayer];
							//if (addscore[nowPlayer]<stcdata[cp].HojuMaxScore)
							if (yk_score>stcdata[cp].HojuMaxScore)
								stcdata[cp].HojuMaxScore=yk_score; //addscore[nowPlayer];
						//}
						senseki[cp].HojuTotalScore[baKaze]+=yk_score;
					}
				}
				break;
			case AG_TUMO:
				if (p==oya) {
					sc=((bp*2+90)/100)*100;
					addscore[p]+=sc*3;
					yk_score=sc*3;
					for (i=0;i<4;i++) {
						if (i!=p) {
							addscore[i]-=sc+honba*100;
						}
					}
				} else {
					yk_score=sc=(2*((bp+90)/100)+(bp*2+90)/100)*100;
					addscore[p]+=sc;
					for (i=0;i<4;i++) {
						if (i!=p) {
							if (i==oya)
								addscore[i]-=((bp*2+90)/100+honba)*100;
							else
								addscore[i]-=((bp+90)/100+honba)*100;
						}
					}
				}
				if (n==1) {
					addscore[p]+=kyotaku*1000+honba*300;
					//addscore[nowPlayer]-=honba*300;
				}
				if (recording) { //点数の記録（ツモ）
					int cp=think[nowPlayer][0];
					if (cp>=0) {
						stcdata[cp].TumoTotalScore+=yk_score; //addscore[nowPlayer];
						//if (addscore[nowPlayer]>stcdata[cp].TumoMaxScore)
						if (yk_score>stcdata[cp].TumoMaxScore)
							stcdata[cp].TumoMaxScore=yk_score; //addscore[nowPlayer];
						senseki[cp].HoraCount[baKaze]++;
						senseki[cp].HoraTotalScore[baKaze]+=yk_score;
					}
				}
				break;
			}
			
			//役の統計
			int cp=think[p][0];
			if ((recording)&&(cp>=0)) {
				int nn=yaku[0][0];
				int yk,yp;
				if (yk_yakuman>0) { //役満
					for (i=1;i<=nn;i++) {
						//stc_yakuman[cp][yaku[i][0]-101]++;
						yk=yaku[i][0];
						stcdata[cp].Yakuman[yk-101]++;
					}
					if ((yk_yakuman>1)&&(yk_yakuman<6)) {
						stcdata[cp].Yakumans[yk_yakuman-2]++;
					} else if (yk_yakuman>=6) {
						stcdata[cp].Yakumans[4]++;
					}

				} else { //普通の役
					for (i=1;i<=nn;i++) {
						//stc_yaku[cp][yaku[i][0]-1]++;
						yk=yaku[i][0];
						yp=yaku[i][1];
						stcdata[cp].Yaku[yk-1][0]++;
						stcdata[cp].Yaku[yk-1][1]+=yp;
						if (yk==YK_DORA) {
							if (yp>stcdata[cp].DoraMax) {
								stcdata[cp].DoraMax=yp;
							}
						}
						if ((yk==YK_REACH)||(yk==YK_DBLREACH)) {
							senseki[cp].ReachHoraCount[baKaze]++;
						}
					}
					if (yk_han<13) { //翻
						stcdata[cp].HanCount[yk_han-1]++;
					} else {
						stcdata[cp].HanCount[12]++;
					}
					stcdata[cp].TotalHan+=yk_han;
					if (yk_han>stcdata[cp].MaxHan) {
						stcdata[cp].MaxHan=yk_han;
					}
					if (yk_fu>stcdata[cp].MaxFu) {
						stcdata[cp].MaxFu=yk_fu;
					}
					if (yk_fu<30) { //符
						if (yk_fu==20) {
							stcdata[cp].FuCount[0]++;
						}
						if (yk_fu==25) {
							stcdata[cp].FuCount[1]++;
						}
					} else if (yk_fu<120) {
						stcdata[cp].FuCount[(yk_fu/10)-1]++;
					} else {
						stcdata[cp].FuCount[11]++;
					}
					stcdata[cp].TotalFu+=yk_fu;
				}
				if (agari_type==AG_RON) {
					stcdata[cp].RonCount++;
					if (yk_yakuman>0)
						stcdata[cp].YmRonCount++;
				} else {
					stcdata[cp].TumoCount++;
					if (yk_yakuman>0)
						stcdata[cp].YmTumoCount++;
				}
			}
			
			agCount=100;
			GameMode=GM_AGARI2;
			return;
		}
		agCount--;
	}
	private void gm_agari2() { //役表示待ち
		if (keyEvent==FIRE)
			agCount=0;
		//keyEvent=-999;
		if (agCount==0) {
			agariPlayer[0]--;
			if (agariPlayer[0]>0) {
				agCount=10;
				GameMode=GM_AGARI1;
				return;
			}
			agCount=50;
			GameMode=GM_AGARI3;
			return;
		}
		//agCount--;
	}
	private void gm_agari3() { //点数表示待ち
		int i,r;
		if (keyEvent==FIRE)
			agCount=0;
		//keyEvent=-999;
		if (agCount==0) {
			for (i=0;i<4;i++) {
				score[i]+=addscore[i];
				tehai[i][13]=0;
				furopai[i][0][0]=0;
				sutehai[i][0][0]=0;
				reach[i]=false;
				furiten[i]=0;
			}
			GameState=0;
			kyotaku=0;
			
			if (rule_tobi) {
				int ffg=0;
				for (i=0;i<4;i++) {
					if (score[i]<0) {
						ffg++;
						if (recording) { //記録
							int cp=think[i][0];
							if (cp>=0) {
								stcdata[cp].TobiCount++;
								senseki[cp].TobiCount++;
							}
						}
					}
				}
				if (ffg>0) { //トビ終了
						fnCount=0;
						GameMode=GM_FINISH1;
						return;
				}
			}
			
			r=0;
			for (i=1;i<=2;i++)
				if (agariPlayer[i]==oya) r=1; //連荘

			//３万点超えがいるかどうか
			int fg=0;
			for (i=0;i<4;i++)
				if (score[i]>=30000) fg=1;
			
			if (rule_SuddenDeath) { //サドンデスルール適用か
				switch (rule_PlayLong) {
				case 0: //東風戦
					if ((baKaze==1)&&(fg>0)) { //南場で３万超えがいるなら
						fnCount=0; GameMode=GM_FINISH1; return;
					}
					break;
				case 1: //半荘戦
					if ((baKaze==2)&&(fg>0)) { //西場で３万超えがいるなら
						fnCount=0; GameMode=GM_FINISH1; return;
					}
					break;
				}
			}

			if (r==0) { //輪荘
				oya=(oya+1)%4;
				kyoku=(kyoku+1)%4;
				

				if (kyoku==3) { //４局目
					switch (rule_PlayLong) {
					case 0: //東風戦
						alllast=true; //オーラス
						break;
					case 1: //半荘戦
						if (baKaze>0) 
							alllast=true; //南場（西場）ならオーラス
						break;
					case 2: //一荘戦
						if (baKaze==3)
							alllast=true; //北場ならオーラス
						break;
					}
				}

				if (kyoku==0) { //４局目が終了

					baKaze=(baKaze+1)%4;   //次局へ、終局の判定しとけや

					switch (rule_PlayLong) {
					case 0: //東風戦
						if (rule_Continue==false) { //南入なし
							fnCount=0; GameMode=GM_FINISH1; return;
						} else { //南入あり
							if (fg>0) { //3万超えいる
								fnCount=0; GameMode=GM_FINISH1; return;
							} else { //3万超えいない
								if (baKaze==2) { //西入はしない
									fnCount=0; GameMode=GM_FINISH1; return;
								} else { //南入
									if (rule_SuddenDeath==false) { //サドンデスしない
										alllast=false;
									}
									if (recording) {
										for (i=0;i<4;i++) {
											int cp=think[i][0];
											if (cp>=0) {
												senseki[cp].ContinueCount++;
											}
										}
									}
								}
							}
						}
						break;
					case 1: //半荘戦
						if (baKaze>1) { //南場終了以降
							if (rule_Continue==false) { //南入なし
								fnCount=0; GameMode=GM_FINISH1; return;
							} else { //西入あり
								if (fg>0) { //3万超えいる
									fnCount=0; GameMode=GM_FINISH1; return;
								} else { //3万超えいない
									if (baKaze==3) { //北入はしない
										fnCount=0; GameMode=GM_FINISH1; return;
									} else { //西入
										if (rule_SuddenDeath==false) { //サドンデスしない
											alllast=false;
										}
										if (recording) {
											for (i=0;i<4;i++) {
												int cp=think[i][0];
												if (cp>=0) {
													senseki[cp].ContinueCount++;
												}
											}
										}
									}
								}
							}
						}
						break;
					case 2: //一荘戦
						if (baKaze==0) { //北場終了
							fnCount=0; GameMode=GM_FINISH1; return;
						}
						break;
					}
				}
				honba=0;
			} else { //連荘
				if ((alllast)&&(score[oya]>=30000)) { //４局目 ラス親のあがりでトップなら終局
					int fg1=0;
					for (i=0;i<4;i++) { //親がトップかチェック
						if ((score[i]>=score[oya])&&(i!=oya)) {
							fg1=1; break;
						}
					}
					if (fg1==0) {
						switch (rule_PlayLong) {
						case 0: //東風戦
							fnCount=0; GameMode=GM_FINISH1; return;
						case 1: //半荘戦
							if (baKaze>0) { //南場以降
								fnCount=0; GameMode=GM_FINISH1; return;
							}
							break;
						case 2: //一荘戦
							if (baKaze==3) { //北場なら
								fnCount=0; GameMode=GM_FINISH1; return;
							}
							break;
						}
					}
				}
				renchan[oya]++;
				honba++;
			}
			nowPlayer=oya;
			haipaiCount=-1;
			GameMode=GM_START;
			//keyEvent=-999;
			return;
		}
		//agCount--;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void gm_finish1() {
		if (fnCount==30) {
			int[] r=new int[4];
			int i,j,k;
			for (i=0;i<4;i++) r[i]=(i-kicha+4)%4; //同点は起家から順位付けるため
			
			for (j=0;j<4;j++)
				for (i=0;i<3;i++) {
					if (score[r[i]]<score[r[i+1]]) {
						k=r[i];
						r[i]=r[i+1];
						r[i+1]=k;
					}
				}
			
			for (i=0;i<4;i++) {
				k=r[i];
				rank[k][0]=i+1;
				if (i==0) {
					//score[k]+=20000; //オカ
					score[k]+=kyotaku*1000; //点棒
				}
				if (score[k]>=0)
					rank[k][1]=(score[k]+400)/1000-30; //五捨六入 と３万点返し
				else
					rank[k][1]=(score[k]-500)/1000-30; //五捨六入 と３万点返し
				switch (i) { //順位ウマ
				case 1: rank[k][1]+=10; break;
				case 2: rank[k][1]-=10; break;
				case 3: rank[k][1]-=20; break;
				}
			}
			k=r[0];
			rank[k][1]=0;
			for (i=1;i<4;i++)
				rank[k][1]-=rank[r[i]][1]; //一位の得点を計算
			fnCount=0;
			if (recording) { //記録
				int cp;
				for (i=0;i<4;i++) {
					cp=think[i][0];
					if (cp>=0) {
						stcdata[cp].RankCount[rank[i][0]-1]++;
						senseki[cp].RankCount[rank[i][0]-1]++;
						int pt=rank[i][1];
						senseki[cp].TotalPoint+=pt;
						if (pt<senseki[cp].MinPoint)
							senseki[cp].MinPoint=pt;
						if (pt>senseki[cp].MaxPoint)
							senseki[cp].MaxPoint=pt;
						stcdata[cp].RenchanCount+=renchan[i]; //連荘の記録
						if (renchan[i]>stcdata[cp].RenchanMax)
							stcdata[cp].RenchanMax=renchan[i];
					}
				}
				if (senrekiCount<10) {
					senrekiCount++;
				} else {
					for (i=0;i<9;i++) {
						for (j=0;j<12;j++) {
							senreki[i][j]=senreki[i+1][j];
						}
						senrekiRule[i]=senrekiRule[i+1];
						senrekiDate[i]=senrekiDate[i+1];
					}
				}
				k=senrekiCount-1;
				for (i=0;i<4;i++) {
					cp=think[i][0];
					if (cp>=0) {
						j=(i-kicha+4)%4;
						senreki[k][j*3]=cp;
						senreki[k][j*3+1]=rank[i][0];
						senreki[k][j*3+2]=rank[i][1];
					}
				}
				int tp=rule_PlayLong;
				
				if(rule_tobi) tp|=1<<3;
				if(rule_kuitan) tp|=1<<4;
				if (rule_Continue) tp|=1<<5;
				if (rule_SuddenDeath) tp|=1<<6;
				senrekiRule[k]=tp;
				senrekiDate[k]=System.currentTimeMillis();
			}
			removeCommand(cmds[3]);
			GameMode=GM_FINISH2;
			return;
		}
		fnCount++;
	}
	private void gm_finish2() {
		if (keyEvent==FIRE)
			fnCount=50;
		if (fnCount==50) {
			//keyEvent=-999;
			drawComment("しばらくお待ち下さい");
			flushGraphics();
			if (recording) {
				saveSensekiData();
				saveStcData();
				saveSenreki();
			}
			nowPlaying=false;
			GameMode=GM_MENU;
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //


// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@  コンピュータ  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	
	//コンピュータか
	private boolean isCompute(int p) {
		int pp=1<<p;
		if ((cpuplayer & pp)>0) {
			return true;
		}
		return false;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//特定の牌が手牌の何番目にあるかを返す
	private int paiIndex(int p, int pai) {
		int i, n=tehai[p][13];
		for (i=0;i<n;i++) {
			if (tehai[p][i]==pai) 
				return i;
		}
		return -1;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//コンピュータのツモ後選択
	private int computeTCommand(int p) {
		int pp=1<<p;
		if ((cpuplayer & pp)>0) {
			
			if (think[p][0]<0) { //ツモ切りモード
				selectPai=-1;
				selectTCmd=TCMD_DAHAI;
				return FIRE;
			}
			
			int i,j,k;
			int[] pais=new int[34];
			int pai=getSelectPai(p,-1);
			int n=tehai[p][13];
			
			//if (t_agari[p][pai]) { //和了牌ならあがる @チェック
			if (checkTumo(p,pai)) {
				selectTCmd=TCMD_TUMOHO;
				return FIRE;
			}
			//牌のカウント
			for (i=0;i<n;i++)
				pais[tehai[p][i]]++;
			pais[pai]++;
			
			int t=-1;
			switch (think[p][0]) {
			case 1:
				t=computeTCmdType1(p,pai,pais);
				break;
			case 2:
				t=computeTCmdType2(p,pai,pais);
				break;
			case 3:
				think[p][1]=0;
				t=computeTCmdType3(p,pai,pais);
				break;
			case 4:
				t=computeTCmdType4(p,pai,pais);
				break;
			}
			if (t>=0) {
				selectTCmd=t;
				return FIRE;
			}
			boolean bb;
			selectPai=rand(tehai[p][13]);
			if (selectPai<0)
				bb=checkReach(p,-1,-1);
			else
				bb=checkReach(p,tehai[p][selectPai],pai);
			if (bb)
				selectTCmd=TCMD_REACH;
			else
				selectTCmd=TCMD_DAHAI;
			return FIRE;
		}
		return keyEvent;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private int computeKokushi(int p, int pai, int[] pais) {
		int i,k;
		//九種九牌の判定
		if (firstRotate>0) {
			int c=0;
			for (i=0;i<3;i++) {
				if (pais[i*9]>0)
					c++;
				if (pais[i*9+8]>0)
					c++;
			}
			for (i=27;i<34;i++) {
				if (pais[i]>0)
					c++;
			}
			if (c>10) { //国士無双狙っちゃう？
				think[p][1]=1;
			} else if (c>8) {
				return TCMD_RYUKYOKU;
			}
		}
		
		if (think[p][1]==1) { //国士無双狙い
			for (i=0;i<27;i++) { //タンヤオ捨てる
				if ((i%9)%8>0) {
					if (pais[i]>0) {
						selectPai=paiIndex(p,i);
						return TCMD_DAHAI;
					}
				}
			}
			k=0;
			for (i=0;i<34;i++) {
				if (pais[i]>1) { 
					k++;
					if (pais[i]>2) { //３枚以上あるなら捨てる
						selectPai=paiIndex(p,i);
						return TCMD_DAHAI;
					}
					if (k>1) { //対子が２つあるので捨てる
						selectPai=paiIndex(p,i);
						return TCMD_DAHAI;
					}
				}
			}
		}
		return -1;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	
	//打牌をリーチするか決める
	private int computeDahai(int p, int pai, int dpai, int[] pais) {
		int i;
		boolean bb;
		selectPai=paiIndex(p,dpai);
		if (selectPai<0)
			bb=checkReach(p,-1,-1);
		else
			bb=checkReach(p,dpai,pai);
		if ((bb)&&(reachCount<3)) {
			int n=sutehai[p][0][0]; //リーチするまえにフリテンにならないかチェック
			int pp,f=0;
			for (i=1;i<=n;i++) {
				pp=sutehai[p][i][0];
				if ((c_r_agari[pp])||(c_t_agari[pp])||(c_k_agari[pp])) {
					f=1;
					break;
				}
			}
			if (f==0) {
				int kk=0; //あがり牌がすでに４枚切れてないかチェック
				for (i=0;i<34;i++) {
					if ((c_r_agari[i])||(c_t_agari[i])||(c_k_agari[i])) {
						if (seepais[i]+pais[i]<4) { //まだ牌は残ってる
							kk=-1;
							break;
						}
					}
				}
				if (kk<0) {
					int cc=0,dd=0;
					for (i=0;i<34;i++) {
						if (c_k_agari[i]) { //形式テンパイがある
							cc++; break;
						}
						if (c_r_agari[i]!=c_t_agari[i]) { //ロンできないあがり牌がある
							cc++; break;
						}
						if (c_t_agari[i]) //ロンできるあがり牌がある
							dd++;
					}
					if ((cc==0)&&(dd>0)) { //ダマ聴するか？
						if (think[p][1]==0) {
							if ((rand(100)>90)&&(score[p]>30000)) {
								think[p][1]=5;
								return TCMD_DAHAI;
							}
						}
					}
					return TCMD_REACH;
				}
			}
		}
		return TCMD_DAHAI;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//打牌をリーチするか決める(3)
	private int computeDahai3(int p, int pai, int dpai, int[] pais) {
		int i,k,c,d,pp,e;
		boolean bb;
		
		selectPai=paiIndex(p,dpai);
		if (selectPai<0)
			bb=checkReach(p,-1,-1);
		else
			bb=checkReach(p,dpai,pai);
		
		if ((bb)&&(furiten[p]==0)) {
			c=sutehai[p][0][0]; k=0;
			for (i=1;i<=c;i++) {
				pp=sutehai[p][i][0];
				if ((c_r_agari[pp])||(c_t_agari[pp])||(c_k_agari[pp])) {
					k=1; break;
				}
			}
			if (k==0) {
				c=0; d=0; 
				for (i=0;i<34;i++) {
					if (c_r_agari[i]) c++;
					if (c_r_agari[i]!=c_t_agari[i]) c++;
					if (c_k_agari[i]) d++;
				}
				if ((c==0)||(d>0)) { //立直しないと同巡フリテンの恐れがある
					e=0;
					for (i=0;i<34;i++) {
						if ((c_r_agari[i])||(c_t_agari[i])||(c_k_agari[i])) {
							if (seepais[i]+pais[i]<4) { //まだ牌は残ってる
								e++;
								break;
							}
						}
					}
					if (e>0)
						return TCMD_REACH;
				}
			}
		}
		
		return TCMD_DAHAI;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//打牌をリーチするか決める(4)
	private int computeDahai4(int p, int pai, int dpai, int[] pais) {
		int i,k,c,d,pp,e;
		boolean bb;
		
		selectPai=paiIndex(p,dpai);
		if (selectPai<0)
			bb=checkReach(p,-1,-1);
		else
			bb=checkReach(p,dpai,pai);
		
		if ((bb)&&(furiten[p]==0)) {
			c=sutehai[p][0][0]; k=0;
			for (i=1;i<=c;i++) { //フリテンチェック
				pp=sutehai[p][i][0];
				if ((c_r_agari[pp])||(c_t_agari[pp])||(c_k_agari[pp])) {
					k=1; break;
				}
			}
			if (k==0) {
				int d1=0,d2=0;
				for (i=0;i<34;i++) {
					if (c_r_agari[i]) d1++;
					if (c_k_agari[i]) d2++;
					if (c_r_agari[i]!=c_t_agari[i]) d2++;
				}
				if ((d1>0)&&(d2==0)&&(score[p]>40000))
					return TCMD_DAHAI; //おそらくトップなのでダマ
				e=0;
				for (i=0;i<34;i++) {
					if ((c_r_agari[i])||(c_t_agari[i])||(c_k_agari[i])) {
						if (seepais[i]+pais[i]<4) { //まだ牌は残ってる
							e++;
							break;
						}
					}
				}
				if (e>0)
					return TCMD_REACH;
			}
		}
		
		return TCMD_DAHAI;
	}


// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//思考パターン１ (paiはツモった牌、-1なら副露(チー・ポン)の打牌選択)
	private int computeTCmdType1(int p, int pai, int[] pais) {
		int i,j,k;
		boolean bb;
		
		int t=computeKokushi(p,pai,pais);
		if (t>=0) return t;

		if ((tenpai[p])&&(pai>=0)) { //テンパイ状態ならツモ切り、リーチ前ならリーチする
			selectPai=-1;
			if (reach[p]) { //リーチしてるならツモ捨て
				return TCMD_DAHAI;
			}
			if (furiten[p]==0) { //フリテンじゃないなら打牌
				k=0;
				for (i=0;i<34;i++) { //あがり牌が存在してることを確認
					if ((r_agari[p][i])||(t_agari[p][i])||(k_agari[p][i])) {
						if (seepais[i]<4) {
							k=-1;
							break;
						}
					}
				}
				if (k<0) {
					if ((checkReach(p,-1,-1))&&(reachCount<3)) {
						int d1=0,d2=0;
						for (i=0;i<34;i++) {
							if (c_r_agari[i]) d1++; //ロンできるか
							if (c_k_agari[i]) d2++; //形聴があるか
							if (c_r_agari[i]!=c_t_agari[i]) d2++; //ツモのみがあるか
						}
						if ((d1>0)&&(d2==0)) { //役つきテンパイ
							if (think[p][1]==5) { //すでにダマテンモード
								return TCMD_DAHAI;
							} else {
								if (rand(100)>90) { //10%の確率でダマテンモード
									think[p][1]=5;
									return TCMD_DAHAI;
								}
							}
						}
						return TCMD_REACH;
					} else {
						return TCMD_DAHAI;
					}
				}
			}
			if (think[p][1]==5) think[p][1]=0; //副露等の事情でダマテン解除
		}

		int n=tehai[p][13];
		int f=furopai[p][0][0];
		if ((rand(100)>90)&&(rinshanIndex<3)&&(pai>=0)) { //気まぐれでカンする
			if (checkAnkan(p,pai)) {
				selectPai=-1;
				return TCMD_ANKAN;
			}
			if (f>0)
				if (checkKakan(p,pai)) {
					selectPai=-1;
					return TCMD_KAKAN;
				}
			if (reach[p]==false) {
				for (i=0;i<34;i++) {
					if (pais[i]==4) {
						selectPai=paiIndex(p,i);
						return TCMD_ANKAN;
					}
				}
				if (f>0)
					for (i=0;i<n;i++) {
						if (checkKakan(p,tehai[p][i])) {
							selectPai=i;
							return TCMD_KAKAN;
						}
					}
			}
		}

		for (i=27;i<34;i++) { //カンしないから４枚ある字牌切り
			if (pais[i]==4) {
				return computeDahai(p,pai,i,pais);
			}
		}

		for (i=27;i<34;i++) { //ポンできない字牌は切る
			if ((seepais[i]>1)&&(pais[i]>0)) {
				return computeDahai(p,pai,i,pais);
			}
		}
		for (i=0;i<4;i++) { //オタ風牌が１枚なら切る
			if (i!=baKaze) {
				if (i!=((p-oya+4)%4)) {
					if (pais[i+27]==1) {
						return computeDahai(p,pai,i+27,pais);
					}
				}
			}
		}
		for (i=27;i<34;i++) { //字牌で使えなさそうなのを切る
			if ((pais[i]==1)&&(seepais[i]>0)) {
				return computeDahai(p,pai,i,pais);
			}
		}
		for (i=27;i<34;i++) { //１枚字牌切る
			if (pais[i]==1) {
				return computeDahai(p,pai,i,pais);
			}
		}
		int[] pflag=new int[34]; //順子を作れ無さそうな数牌を切る
		for (j=0;j<3;j++) {
			for (i=0;i<7;i++) {
				k=i+j*9;
				if ((pflag[k]==0)&&(pais[k]>0)) {
					if ((pais[k+1]>0)&&(pais[k+2])>0) {
						pflag[k]++;
						pflag[k+1]++;
						pflag[k+2]++;
					}
				}
			}
		}
		for (j=0;j<3;j++) {
			for (i=0;i<7;i++) {
				k=i+j*9;
				if ((t=pais[k]-pflag[k])>0) {
					if (t>1) {
						pflag[k]+=t;
					} else {
						if ((pais[k+1]-pflag[k+1]>0)&&(pais[k+2]-pflag[k+2]>0)) {
							pflag[k]++;
							pflag[k+1]++;
							pflag[k+2]++;
						}
					}
				}
			}
		}
		for (i=0;i<27;i++) {
			if ((pais[i]==1)&&(pflag[i]==0)) {
				return computeDahai(p,pai,i,pais);
			}
		}
		for (i=0;i<27;i++) {
			if (pais[i]-pflag[i]>0) {
				return computeDahai(p,pai,i,pais);
			}
		}
		pflag=new int[34];
		for (j=0;j<3;j++)
			for (i=0;i<7;i++) {
				k=i+j*9;
				if (pais[k]>0)   { pflag[k+1]++; pflag[k+2]++;}
				if (pais[k+1]>0) { pflag[k]++;   pflag[k+2]++;}
				if (pais[k+2]>0) { pflag[k]++;   pflag[k+1]++;}
			}
		for (i=0;i<27;i++) {
			if ((pais[i]==1)&&(pflag[i]==0)) {
				return computeDahai(p,pai,i,pais);
			}
		}
		return -1;
		
		/* 新・太郎 構想
		
		//国士無双
		int t=computeKokushi(p,pai,pais);
		if (t>=0) return t;
		
		if ((reach[p])&&(pai>=0)) { //立直中ならツモ捨て
			if ((checkAnkan(p,pai))&&(rinshanIndex<3)) {
				selectPai=-1;
				return TCMD_ANKAN;
			}
			selectPai=-1;
			return TCMD_DAHAI;
		}
		
		//ここにオリの処理
		
		//うまく面子を分ける
		//三色同順・一気通貫・一盃口・二盃口・三暗刻・三色同刻
		//などの役考慮（待ちとしてのカウントもする
		
		//七対子処理(二盃口の可能性が無いなら)
		
		//切る牌を選ぶ(打牌に待ちと捨て牌を考慮できるとよい
		
		//*/
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	
	//思考パターン２ (paiはツモった牌、-1なら副露(チー・ポン)の打牌選択)
	private int computeTCmdType2(int p, int pai, int[] pais) {
		int i,j,k,e,c;
		int[] pais2=new int[34];
		boolean bb;

		int t=computeKokushi(p,pai,pais);
		if (t>=0) return t;
		
		if ((tenpai[p])&&(pai>=0)) { //テンパイ状態ならツモ切り、リーチ前ならリーチする
			selectPai=-1;
			if (reach[p]) { //リーチしてるならツモ捨て
				if (checkAnkan(p,pai)) {
					return TCMD_ANKAN;
				}
				return TCMD_DAHAI;
			}
			if (furiten[p]==0) { //フリテンじゃないなら打牌
				k=0;
				for (i=0;i<34;i++) { //あがり牌が存在してることを確認
					if ((r_agari[p][i])||(t_agari[p][i])||(k_agari[p][i])) {
						if (seepais[i]<4) {
							k=-1;
							break;
						}
					}
				}
				if (k<0) {
					if ((checkReach(p,-1,-1))&&(reachCount<3)) { 
						int d1=0,d2=0;
						for (i=0;i<34;i++) {
							if (c_r_agari[i]) d1++; //ロンできるか
							if (c_k_agari[i]) d2++; //形聴があるか
							if (c_r_agari[i]!=c_t_agari[i]) d2++; //ツモのみがあるか
						}
						if ((d1>0)&&(d2==0)) { //役つきテンパイ
							if (think[p][1]==5) { //すでにダマテンモード
								return TCMD_DAHAI;
							} else {
								if (rand(100)>90) { //10%の確率でダマテンモード
									think[p][1]=5;
									return TCMD_DAHAI;
								}
							}
						}
						return TCMD_REACH;
					} else {
						return TCMD_DAHAI;
					}
				}
			}
			if (think[p][1]==5) think[p][1]=0; //副露等の事情でダマテン解除
		}
		
		int n=tehai[p][13];
		int f=furopai[p][0][0];
		if ((rand(100)>90)&&((rinshanIndex<3)||(tenpai[p]))&&(pai>=0)) { //気まぐれでカンする
			if (checkAnkan(p,pai)) {
				selectPai=-1;
				return TCMD_ANKAN;
			}
			if (f>0)
				if (checkKakan(p,pai)) {
					selectPai=-1;
					return TCMD_KAKAN;
				}
			if (reach[p]==false) {
				for (i=0;i<34;i++) {
					if (pais[i]==4) {
						selectPai=paiIndex(p,i);
						return TCMD_ANKAN;
					}
				}
				if (f>0)
					for (i=0;i<n;i++) {
						if (checkKakan(p,tehai[p][i])) {
							selectPai=i;
							return TCMD_KAKAN;
						}
					}
			}
		}
		
		for (i=27;i<34;i++) { //カンしないから４枚ある字牌切り
			if (pais[i]==4) {
				return computeDahai(p,pai,i,pais);
			}
		}
		
		for (i=0;i<34;i++)
			pais2[i]=pais[i];
		
		c=0;
		
		for (j=0;j<3;j++) { //順子抜き出し
			do {
				e=0;
				for (i=0;i<7;i++) {
					k=i+j*9;
					if ((pais2[k]>0)&&(pais2[k+1]>0)&&(pais2[k+2]>0)) {
						pais2[k]--;
						pais2[k+1]--;
						pais2[k+2]--;
						e=1;
						c+=3;
					}
				}
			} while (e>0);
		}
		do { //刻子抜き出し
			e=0;
			for (i=0;i<34;i++) {
				if (pais2[i]>2) {
					pais2[i]-=3;
					e=1;
					c+=3;
				}
			}
		} while (e>0);
		
		for (i=33;i>=0;i--) { //雀頭抜き出し
			if (pais2[i]==2) {
				pais2[i]=0;
				c+=2;
				break;
			}
		}
		
		for (j=0;j<3;j++) {
			do {
				e=0;
				for (i=0;i<8;i++) {
					k=i+j*9;
					if ((pais2[k]>0)&&(pais2[k+1]>0)) { //両面・辺張塔子抜き出し
						pais2[k]--; if (pais2[k]==0) pais2[k]--;
						pais2[k+1]--; if (pais2[k+1]==0) pais2[k+1]--;
						e=1;
						c+=2;
					}
					if ((pais2[k]>0)&&(pais2[k+2]>0)&&(i<7)) { //嵌張塔子抜き出し
						pais2[k]--; if (pais2[k]<1) pais2[k]=-2;
						pais2[k+2]--; if (pais2[k+2]<1) pais2[k+2]=-2;
						e=1;
						c+=2;
					}
				}
			} while (e>0);
		}
		
		if ((menzen[p])&&(furopai[p][0][0]==0)) {
			int cc=0, c3=0;
			for (i=0;i<34;i++) {
				if (pais[i]>1) {
					cc++;
					if (pais[i]>2) {
						c3++;
					}
				}
			}
			if (((c<8)&&(cc>3)&&(c3<3))||(cc>4)) { //七対子ねらう？
				for (i=0;i<34;i++) {
					if (pais[i]>2) {
						return computeDahai(p,pai,i,pais);
					}
				}
				for (i=33;i>=0;i--) {
					if (pais[i]==1) {
						return computeDahai(p,pai,i,pais);
					}
				}
			}
		}
		
		for (i=27;i<31;i++) { //単騎っぽいの捨てる（オタ風）
			if (pais2[i]==1) {
				if ((i!=baKaze+27)&&(i!=(p-oya+4)%4+27)) {
					return computeDahai(p,pai,i,pais);
				}
			}
		}
		
		for (i=27;i<34;i++) { //単騎っぽいの捨てる （一枚以上切られてる字牌）
			if (pais2[i]==1) {
				if (seepais[i]>0) {
					return computeDahai(p,pai,i,pais);
				}
			}
		}
		
		int[] ccc=new int[20];
		ccc[0]=0;
		for (i=0;i<34;i++) {
			if (pais2[i]==1) {
				ccc[0]++;
				ccc[ccc[0]]=i;
			}
		}
		if (ccc[0]>0) {
			return computeDahai(p,pai,ccc[rand(ccc[0])+1],pais);
		}
		/*
		for (i=0;i<34;i++) { //単騎っぽいの捨てる （字牌から）
			if (pais2[i]==1) {
				return computeDahai(p,pai,i,pais);
			}
		}
		*/

		for (i=27;i<31;i++) { //対子を捨てる（2枚以上切られてるオタ風）
			if ((pais2[i]==2)&&(seepais[i]>1)) {
				if ((i!=baKaze+27)&&(i!=(p-oya+4)%4+27)) {
					return computeDahai(p,pai,i,pais);
				}
			}
		}

		for (i=27;i<34;i++) { //対子を捨てる （2枚以上切られてる字牌）
			if (pais2[i]==2) {
				if (seepais[i]>1) {
					return computeDahai(p,pai,i,pais);
				}
			}
		}
		
		for (i=27;i<31;i++) { //対子を捨てる（一枚以上切られてるオタ風）
			if ((pais2[i]==2)&&(seepais[i]>0)) {
				if ((i!=baKaze+27)&&(i!=(p-oya+4)%4+27)) {
					return computeDahai(p,pai,i,pais);
				}
			}
		}

		for (i=27;i<34;i++) { //対子を捨てる （一枚以上切られてる字牌）
			if (pais2[i]==2) {
				if (seepais[i]>0) {
					return computeDahai(p,pai,i,pais);
				}
			}
		}

		for (i=0;i<34;i++) { //雀頭にならない対子を捨てる
			if (pais2[i]==2) {
				return computeDahai(p,pai,i,pais);
			}
		}

		for (i=26;i>=0;i--) {  //嵌張塔子崩す
			if (pais2[i]==-2) {
				return computeDahai(p,pai,i,pais);
			}
		}
		for (i=26;i>=0;i--) {  //両面・辺張塔子崩す
			if (pais2[i]<0) {
				return computeDahai(p,pai,i,pais);
			}
		}

		return -1;
	}
	
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	
	private boolean inSutehai(int p, int pai) {
		return inSutehai(p,pai,-1);
	}
	private boolean inSutehai(int p, int pai1, int pai2) {
		int i;
		int n=sutehai[p][0][0];
		for (i=1;i<=n;i++) {
			if (sutehai[p][i][0]==pai1)
				return true;
			if (sutehai[p][i][0]==pai2)
				return true;
		}
		return false;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private int computeTCmdType3(int p, int pai, int[] pais) {
		int i,j,k,c,d,pp,fg,u,cc2;
		
		if ((reach[p])&&(pai>=0)) { //立直してるならツモ捨て
			selectPai=-1;
			return TCMD_DAHAI;
		}
		
		c=0; cc2=0; //他家の副露and立直をチェック
		for (i=0;i<4;i++) 
			if ((i!=p)&&((furopai[i][0][0]>1)||(reach[i]))) {
				c|=1<<i;
				cc2++;
			}
		if (c>0) { //２副露以上or立直者がいるならベタおりする
			if (think[p][1]<100) think[p][1]=100;
			for (i=27;i<34;i++) {
				if ((seepais[i]+pais[i]>3)&&(pais[i]>0)) { //字牌のうち安全牌を切る
					selectPai=paiIndex(p,i);
					return TCMD_DAHAI;
				}
			}
			int[] stpi=new int[34];
			if (cc2>1) { //共通捨て牌を探す
				for (i=0;i<4;i++) {
					if ((c&(1<<i))>0) {
						d=sutehai[i][0][0];
						for (j=d;j>0;j--) { 
							pp=sutehai[i][j][0];
							if (pais[pp]>0) { //持ってる牌のみカウント
								stpi[pp]++;
							}
						}
					}
				}
			}
			if (reachCount==0) {
				if (cc2>1) { //共通捨て牌から切る
					for (i=0;i<34;i++) {
						if (stpi[i]==cc2) {
							selectPai=paiIndex(p,i);
							return TCMD_DAHAI;
						}
					}
					for (i=0;i<34;i++) {
						if (stpi[i]>1) {
							selectPai=paiIndex(p,i);
							return TCMD_DAHAI;
						}
					}
				}
			
				if ((c&(1<<oya))>0) { //他家のうち親が該当なら
					d=sutehai[oya][0][0];
					for (j=d;j>0;j--) { //捨て牌（河）にあるのを選らんで捨てる
						pp=sutehai[oya][j][0];
						if (pais[pp]>0) {
							selectPai=paiIndex(p,pp);
							return TCMD_DAHAI;
						}
					}
				}
	
				for (i=0;i<4;i++) {
					if ((c&(1<<i))>0) {
						d=sutehai[i][0][0];
						for (j=d;j>0;j--) { //捨て牌（河）にあるのを選らんで捨てる
							pp=sutehai[i][j][0];
							if (pais[pp]>0) {
								selectPai=paiIndex(p,pp);
								return TCMD_DAHAI;
							}
						}
					}
				}
			}

			fg=0; //他家の打牌参考
			j=0;
			do {
				for (i=1;i<4;i++) {
					k=(p-i+4)%4;
					d=sutehai[k][0][0];
					if (d-j>0) {
						pp=sutehai[k][d-j][0];
						if (pais[pp]>0) {
							selectPai=paiIndex(p,pp);
							return TCMD_DAHAI;
						}
						if ((sutehai[k][d-j][1]&2)>0) {
							fg=14;
							break;
						}
					} else {
						fg|=1<<i;
					} 
				}
				j++;
			} while (fg!=14);

			if (cc2>1) { //共通捨て牌から切る
				for (i=0;i<34;i++) {
					if (stpi[i]==cc2) {
						selectPai=paiIndex(p,i);
						return TCMD_DAHAI;
					}
				}
			}

			if ((c&(1<<oya))>0) { //他家のうち親が該当なら
				d=sutehai[oya][0][0];
				for (j=d;j>0;j--) { //捨て牌（河）にあるのを選らんで捨てる
					pp=sutehai[oya][j][0];
					if (pais[pp]>0) {
						selectPai=paiIndex(p,pp);
						return TCMD_DAHAI;
					}
				}
			}

			if (cc2>1) { //共通捨て牌から切る
				for (i=0;i<34;i++) {
					if (stpi[i]>1) {
						selectPai=paiIndex(p,i);
						return TCMD_DAHAI;
					}
				}
			}

			for (i=0;i<4;i++) {
				if ((c&(1<<i))>0) {
					d=sutehai[i][0][0];
					for (j=d;j>0;j--) { //捨て牌（河）にあるのを選らんで捨てる
						pp=sutehai[i][j][0];
						if (pais[pp]>0) {
							selectPai=paiIndex(p,pp);
							return TCMD_DAHAI;
						}
					}
				}
			}
			//他の降り方も考える必要が・・・
		}

		//国士無双
		int t=computeKokushi(p,pai,pais);
		if (t>=0) return t;
		
		for (i=27;i<34;i++) { //今のところ暗槓はしない
			if (pais[i]==4) {
				selectPai=paiIndex(p,i); return TCMD_DAHAI;
			}
		}
		
		//七対子
		c=0; d=0;
		for (i=0;i<34;i++) {
			if (pais[i]==2) c++;
			if (pais[i]>2) d++;
		}
		if ((c>3)||((c>2)&&(d>0))) { //七対子狙います
			if (think[p][1]!=2) think[p][1]=2;
			for (i=0;i<34;i++)
				if (pais[i]>2) { //刻子を崩す
					selectPai=paiIndex(p,i); return TCMD_DAHAI;
				}
			k=sutehai[p][0][0];
			for (i=1;i<=k;i++) { //フリテンになりかねない牌を捨てる
				pp=sutehai[p][i][0];
				if (pais[pp]==1) {
					selectPai=paiIndex(p,pp); return TCMD_DAHAI;
				}
			}
			j=0;
			for (i=0;i<34;i++) { //全て切れてる牌
				if ((pais[i]==1)&&(seepais[i]>2)) {
					selectPai=paiIndex(p,i); return TCMD_DAHAI;
				}
				if (pais[i]==1) j++;
			}
			k=rand(j); j=0;
			for (i=0;i<34;i++) { //適当な牌を切る
				if ((pais[i]==1)&&(k==j)) {
					selectPai=paiIndex(p,i); return TCMD_DAHAI;
				}
				j++;
			}
			for (i=0;i<34;i++) { //上の漏れを拾う
				if (pais[i]==1) {
					return computeDahai4(p,pai,i,pais);
					//selectPai=paiIndex(p,i); return TCMD_DAHAI;
				}
			}
		}
		if (((c>1)&&(d>1))||((think[p][1]==3)&&(furopai[p][0][0]>0))) { //対々和狙います
			if (think[p][1]!=3) think[p][1]=3;
			k=sutehai[p][0][0];
			for (i=1;i<=k;i++) { //フリテンになりかねない牌を捨てる
				pp=sutehai[p][i][0];
				if (pais[pp]==1) {
					selectPai=paiIndex(p,pp); return TCMD_DAHAI;
				}
			}
			j=0;
			for (i=0;i<34;i++) { //刻子にならない牌を捨てる
				if ((pais[i]==1)&&(seepais[i]>1)) {
					selectPai=paiIndex(p,i); return TCMD_DAHAI;
				}
				if (pais[i]==1) j++;
			}
			k=rand(j); j=0;
			for (i=0;i<34;i++) { //適当な牌を切る
				if ((pais[i]==1)&&(k==j)) {
					selectPai=paiIndex(p,i); return TCMD_DAHAI;
				}
				j++;
			}
			for (i=0;i<34;i++) { //上の漏れを拾う
				if (pais[i]==1) {
					return computeDahai4(p,pai,i,pais);
					//selectPai=paiIndex(p,i); return TCMD_DAHAI;
				}
			}
		}

		int[] pais2=new int[34], pais3=new int[34];
		for (i=0;i<34;i++)
			pais2[i]=pais[i];
		
		if (menzen[p]) {
			for (j=0;j<3;j++) { //一盃口の抜き出し
				for (i=0;i<7;i++) {
					k=i+j*9;
					do {
						fg=0;
						if ((pais2[k]>1)&&(pais2[k+1]>1)&&(pais2[k+2]>1)) {
							pais2[k]-=2; pais2[k+1]-=2; pais2[k+2]-=2;
							fg=1;
						}
					} while (fg!=0);
				}
			}
		}
		for (j=0;j<3;j++) { //一気通貫の抜き出し
			c=0;
			for (i=0;i<9;i++) {
				k=i+j*9;
				if (pais2[k]==0) {
					c=1; break;
				}
			}
			if (c==0) {
				for (i=0;i<9;i++) {
					k=i+j*9;
					pais2[k]--;
				}
			}
		}

		for (i=0;i<7;i++) { //三色同順の抜き出し
			fg=0;
			for (j=0;j<3;j++) {
				k=i+j*9;
				if ((pais2[k]>0)&&(pais2[k+1]>0)&&(pais2[k+2]>0)) {
					fg++;
				}
			}
			if (fg==3) {
				for (j=0;j<3;j++) {
					k=i+j*9;
					for (u=0;u<3;u++) {
						pais2[k+u]--;
					}
				}
			}
		}
		c=0;
		for (i=0;i<34;i++) { //暗刻
			if (pais2[i]>2) {
				c++;
			}
		}
		if (c>2) { //三暗刻の保守
			for (i=0;i<34;i++) {
				if (pais2[i]>2) {
					pais2[i]-=3;
				}
			}
		}
		
		for (j=0;j<3;j++) { //順子の抜き出し
			for (i=0;i<7;i++) {
				k=i+j*9;
				do {
					fg=0;
					if ((pais2[k]>0)&&(pais2[k+1]>0)&&(pais2[k+2]>0)) {
						pais2[k]--; pais2[k+1]--; pais2[k+2]--;
						fg=1;
					}
				} while (fg!=0);
			}
		}
		
		fg=0;
		for (i=31;i<34;i++) { //雀頭の抜き出し(三元牌)
			if ((pais2[i]==2)&&(seepais[i]>1)) {
				pais2[i]-=2; fg=1; break;
			}
		}
		if (fg==0) { 
			for (i=0;i<4;i++) { //雀頭の抜き出し（風牌）
				k=i+27;
				if (pais2[k]==2) {
					if ((i==baKaze)||(i==(p-oya+4)%4)) {
						if (seepais[k]>1) {
							pais2[k]-=2; fg=1; break;
						}
					} else {
						pais2[k]-=2; fg=1; break;
					}
				}
			}
		}
		if (fg==0) {
			for (i=0;i<27;i++) { //雀頭の抜き出し(対子数牌)
				if (pais2[i]==2) {
					pais2[i]-=2; fg=1; break;
				}
			}
		}
		if (fg==0) {
			for (i=0;i<27;i++) { //雀頭の抜き出し(刻子数牌)
				if (pais2[i]>1) {
					pais2[i]-=2; fg=1; break;
				}
			}
		}
		if (fg==0) { 
			for (i=0;i<4;i++) { //雀頭の抜き出し（刻子オタ風牌）
				k=i+27;
				if (pais2[k]>1) {
					if ((i!=baKaze)&&(i!=(p-oya+4)%4)) {
						pais2[k]-=2; fg=1; break;
					}
				}
			}
		}
		
		
		for (j=0;j<3;j++) { //両面塔子の抜き出し
			for (i=1;i<7;i++) { //捨て牌との具合をチェックあり
				k=i+j*9;
				do {
					fg=0;
					if ((pais2[k]>0)&&(pais2[k+1]>0)) {
						if ((inSutehai(p,k-1,k+2)==false)
						        &&(seepais[k-1]+seepais[k+2]+pais[k-1]+pais[k+2]<7)) {
							pais2[k]--; pais2[k+1]--; fg=1;
							pais3[k]=1; pais3[k+1]=1;
						}
					}
				} while (fg!=0);
			}
		}

		for (i=0;i<27;i++) { //数牌刻子の抜き出し
			if (pais2[i]>2) {
				pais2[i]-=3;
			}
		}

		for (j=0;j<3;j++) { //捨て牌との具合チェックあり
			k=j*9;
			do {
				fg=0;
				if ((pais2[k]>0)&&(pais2[k+1]>0)) { //辺張塔子の抜き出し
					if ((inSutehai(p,k+2)==false)&&(seepais[k+2]+pais[k+2]<4)) {
						pais2[k]--; pais2[k+1]--; fg=1;
						pais3[k]=2; pais3[k+1]=2;
					}
				}
			} while (fg!=0);
			k=7+j*9;
			do {
				fg=0;
				if ((pais2[k]>0)&&(pais2[k+1]>0)) { //辺張塔子の抜き出し
					if ((inSutehai(p,k-1)==false)&&(seepais[k-1]+pais[k-1]<4)) {
						pais2[k]--; pais2[k+1]--; fg=1;
						pais3[k]=2; pais3[k+1]=2;
					}
				}
			} while (fg!=0);
			
			for (i=0;i<7;i++) { //嵌張塔子の抜き出し
				k=i+j*9;
				do {
					fg=0;
					if ((pais2[k]>0)&&(pais2[k+2]>0)) {
						if ((inSutehai(p,k+1)==false)&&(seepais[k+1]+pais[k+1]<4)) {
							pais2[k]--; pais2[k+2]--; fg=1;
							pais3[k]=3; pais3[k+2]=3;
						}
					}
				} while (fg!=0);
			}
		}

		for (j=0;j<3;j++) { //両面塔子の抜き出し
			for (i=1;i<7;i++) { //捨て牌との具合をチェックなし
				k=i+j*9;
				do {
					fg=0;
					if ((pais2[k]>0)&&(pais2[k+1]>0)) {
						pais2[k]--; pais2[k+1]--; fg=1;
						if (pais2[k]==0) pais2[k]=-1;
						if (pais2[k+1]==0) pais2[k+1]=-1;
					}
				} while (fg!=0);
			}
		}

		for (j=0;j<3;j++) { //捨て牌との具合チェックなし
			k=j*9;
			do {
				fg=0;
				if ((pais2[k]>0)&&(pais2[k+1]>0)) { //辺張塔子の抜き出し
					pais2[k]--; pais2[k+1]--; fg=1;
					if (pais2[k]==0) pais2[k]=-2;
					if (pais2[k+1]==0) pais2[k+1]=-2;
				}
			} while (fg!=0);
			k=7+j*9;
			do {
				fg=0;
				if ((pais2[k]>0)&&(pais2[k+1]>0)) { //辺張塔子の抜き出し
					pais2[k]--; pais2[k+1]--; fg=1;
					if (pais2[k]==0) pais2[k]=-2;
					if (pais2[k+1]==0) pais2[k+1]=-2;
				}
			} while (fg!=0);
			
			for (i=0;i<7;i++) { //嵌張塔子の抜き出し
				k=i+j*9;
				do {
					fg=0;
					if ((pais2[k]>0)&&(pais2[k+2]>0)) {
						pais2[k]--; pais2[k+2]--; fg=1;
						if (pais2[k]==0) pais2[k]=-3;
						if (pais2[k+2]==0) pais2[k+2]=-3;
					}
				} while (fg!=0);
			}
		}

		for (i=0;i<4;i++) { //いらない単騎オタ風切り
			if ((i!=baKaze)&&(i!=(p-oya+4)%4)&&(pais2[i+27]==1)) {
				return computeDahai3(p,pai,i+27,pais);
				//selectPai=paiIndex(p,i+27); return TCMD_DAHAI;
			}
		}
		for (i=27;i<34;i++) { //対子も作れない単騎字牌切り
			if ((seepais[i]>2)&&(pais2[i]>0)){
				return computeDahai3(p,pai,i,pais);
				//selectPai=paiIndex(p,i); return TCMD_DAHAI;
			}
		}
		for (i=27;i<34;i++) { //一枚以上切れてる単騎字牌切り
			if ((seepais[i]>0)&&(pais2[i]==1)) {
				return computeDahai3(p,pai,i,pais);
				//selectPai=paiIndex(p,i); return TCMD_DAHAI;
			}
		}
		for (i=27;i<34;i++) { //雀頭にも刻子にもならない字牌切り
			if ((seepais[i]>1)&&(pais2[i]==2)) {
				return computeDahai3(p,pai,i,pais);
				//selectPai=paiIndex(p,i); return TCMD_DAHAI;
			}
		}
		k=rand(3);
		for (j=0;j<3;j++) {
			k=(k+1)%3;
			switch (k) {
			case 0:
				for (i=27;i<34;i++) { //単騎字牌切り
					if (pais2[i]==1) {
						return computeDahai3(p,pai,i,pais);
						//selectPai=paiIndex(p,i); return TCMD_DAHAI;
					}
				} break;
			case 1:
				for (i=0;i<3;i++) { //ヤオチュー（１，９）切り
					if (pais2[i*9]>0) {
						return computeDahai3(p,pai,i*9,pais);
						//selectPai=paiIndex(p,i*9); return TCMD_DAHAI;
					}
					if (pais2[8+i*9]>0) {
						return computeDahai3(p,pai,8+i*9,pais);
						//selectPai=paiIndex(p,8+i*9); return TCMD_DAHAI;
					}
				} break;
			case 2:
				for (i=0;i<4;i++) { //いらない単騎オタ風切り
					if ((i!=baKaze)&&(i!=(p-oya+4)%4)&&(pais2[i+27]==2)) {
						return computeDahai3(p,pai,i+27,pais);
						//selectPai=paiIndex(p,i+27); return TCMD_DAHAI;
					}
				} break;
			}
		}
		k=rand(2);
		for (j=0;j<2;j++) {
			k=(k+1)%2;
			switch (k) {
			case 0:
				for (i=0;i<27;i++) { //数牌の余った対子切り
					if ((seepais[i]>0)&&(pais2[i]==2)) {
						return computeDahai3(p,pai,i,pais);
						//selectPai=paiIndex(p,i); return TCMD_DAHAI;
					}
				} break;
			case 1:
				for (i=0;i<27;i++) { //いらない対子数牌を切る
					if (pais2[i]==2) {
						return computeDahai3(p,pai,i,pais);
						//selectPai=paiIndex(p,i); return TCMD_DAHAI;
					}
				} break;
			}
		}
		for (i=0;i<27;i++) { //いらない単騎数牌を切る
			if (pais2[i]==1) {
				return computeDahai3(p,pai,i,pais);
				//selectPai=paiIndex(p,i); return TCMD_DAHAI;
			}
		}
		c=0;
		for (i=0;i<27;i++)
			if (pais2[i]<0) c++;
		if (c>0) {
			k=rand(c);
			c=0;
			for (i=0;i<27;i++) {
				if (pais2[i]<0) {
					if (c==k) {
						return computeDahai3(p,pai,i,pais);
						//selectPai=paiIndex(p,i); return TCMD_DAHAI;
					}
					c++;
				}
			}
		}
		
		for (i=0;i<27;i++) {
			if (pais3[i]==2) {
				return computeDahai3(p,pai,i,pais);
			}
		}
		for (i=0;i<27;i++) {
			if (pais3[i]==3) {
				return computeDahai3(p,pai,i,pais);
			}
		}
		for (i=0;i<27;i++) {
			if (pais3[i]>0) {
				return computeDahai3(p,pai,i,pais);
			}
		}
		
		//System.out.println("きた");
		
		if (pai>=0) {
			return computeDahai3(p,pai,pai,pais);
			//selectPai=-1; return TCMD_DAHAI;
		}
		return -1;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private int computeTCmdType4(int p, int pai, int[] pais) {
		int i,j,k,c,d,pp,fg,u;
		
		if ((reach[p])&&(pai>=0)) { //立直してるならツモ捨て
			selectPai=-1;
			return TCMD_DAHAI;
		}

		//国士無双
		int t=computeKokushi(p,pai,pais);
		if (t>=0) return t;
		
		for (i=27;i<34;i++) { //暗槓
			if (pais[i]==4) {
				if (rinshanIndex<3) {
					selectPai=paiIndex(p,i); return TCMD_ANKAN;
				}
			}
		}
		
		if (think[p][1]==4) { //喰い断
			for (i=0;i<3;i++) {
				pp=i*9;
				if (pais[pp]>0) 
					return computeDahai4(p,pai,pp,pais);
				pp=8+i*9;
				if (pais[pp]>0) 
					return computeDahai4(p,pai,pp,pais);
			}
			for (i=27;i<34;i++) {
				if (pais[i]>0) 
					return computeDahai4(p,pai,i,pais);
			}
		}
		
		//七対子
		c=0; d=0;
		for (i=0;i<34;i++) {
			if (pais[i]==2) c++;
			if (pais[i]>2) d++;
		}
		if ((c>4)||((c>3)&&(d>0))) { //七対子狙います
			if (think[p][1]!=2) think[p][1]=2;
			for (i=0;i<34;i++)
				if (pais[i]>2) { //刻子を崩す
					return computeDahai4(p,pai,i,pais);
					//selectPai=paiIndex(p,i); return TCMD_DAHAI;
				}
			k=sutehai[p][0][0];
			for (i=1;i<=k;i++) { //フリテンになりかねない牌を捨てる
				pp=sutehai[p][i][0];
				if (pais[pp]==1) {
					return computeDahai4(p,pai,pp,pais);
					//selectPai=paiIndex(p,pp); return TCMD_DAHAI;
				}
			}
			for (i=0;i<34;i++) { //全て切れてる牌
				if ((pais[i]==1)&&(seepais[i]>2)) {
					return computeDahai4(p,pai,i,pais);
					//selectPai=paiIndex(p,i); return TCMD_DAHAI;
				}
			}
			j=0;
			for (i=0;i<34;i++) { //のこり一枚の牌
				if ((pais[i]==1)&&(seepais[i]>1)) {
					return computeDahai4(p,pai,i,pais);
					//selectPai=paiIndex(p,i); return TCMD_DAHAI;
				}
				if (pais[i]==1) j++;
			}
			k=rand(j); j=0;
			for (i=0;i<34;i++) { //適当な牌を切る
				if ((pais[i]==1)&&(k==j)) {
					return computeDahai4(p,pai,i,pais);
					//selectPai=paiIndex(p,i); return TCMD_DAHAI;
				}
				j++;
			}
			for (i=0;i<34;i++) { //上の漏れを拾う
				if (pais[i]==1) {
					return computeDahai4(p,pai,i,pais);
					//selectPai=paiIndex(p,i); return TCMD_DAHAI;
				}
			}
		}
		if (((c>1)&&(d>1))||((think[p][1]==3)&&(furopai[p][0][0]>0))) { //対々和狙います
			if (think[p][1]!=3) think[p][1]=3;
			k=sutehai[p][0][0];
			for (i=1;i<=k;i++) { //フリテンになりかねない牌を捨てる
				pp=sutehai[p][i][0];
				if (pais[pp]==1) {
					return computeDahai4(p,pai,pp,pais);
					//selectPai=paiIndex(p,pp); return TCMD_DAHAI;
				}
			}
			j=0;
			for (i=0;i<34;i++) { //刻子にならない牌を捨てる
				if ((pais[i]==1)&&(seepais[i]>1)) {
					return computeDahai4(p,pai,i,pais);
					//selectPai=paiIndex(p,i); return TCMD_DAHAI;
				}
				if (pais[i]==1) j++;
			}
			k=rand(j); j=0;
			for (i=0;i<34;i++) { //適当な牌を切る
				if ((pais[i]==1)&&(k==j)) {
					return computeDahai4(p,pai,i,pais);
					//selectPai=paiIndex(p,i); return TCMD_DAHAI;
				}
				j++;
			}
			for (i=0;i<34;i++) { //上の漏れを拾う
				if (pais[i]==1) {
					return computeDahai4(p,pai,i,pais);
					//selectPai=paiIndex(p,i); return TCMD_DAHAI;
				}
			}
		}

		int[] pais2=new int[34], pais3=new int[34];
		for (i=0;i<34;i++)
			pais2[i]=pais[i];
		
		if (menzen[p]) {
			for (j=0;j<3;j++) { //一盃口の抜き出し
				for (i=0;i<7;i++) {
					k=i+j*9;
					do {
						fg=0;
						if ((pais2[k]>1)&&(pais2[k+1]>1)&&(pais2[k+2]>1)) {
							pais2[k]-=2; pais2[k+1]-=2; pais2[k+2]-=2;
							fg=1;
						}
					} while (fg!=0);
				}
			}
		}
		for (j=0;j<3;j++) { //一気通貫の抜き出し
			c=0;
			for (i=0;i<9;i++) {
				k=i+j*9;
				if (pais2[k]==0) {
					c=1; break;
				}
			}
			if (c==0) {
				for (i=0;i<9;i++) {
					k=i+j*9;
					pais2[k]--;
				}
			}
		}
		for (i=0;i<7;i++) { //三色同順の抜き出し
			fg=0;
			for (j=0;j<3;j++) {
				k=i+j*9;
				if ((pais2[k]>0)&&(pais2[k+1]>0)&&(pais2[k+2]>0)) {
					fg++;
				}
			}
			if (fg==3) {
				for (j=0;j<3;j++) {
					k=i+j*9;
					for (u=0;u<3;u++) {
						pais2[k+u]--;
					}
				}
			}
		}
		c=0;
		for (i=0;i<34;i++) { //暗刻
			if (pais2[i]>2) {
				c++;
			}
		}
		if (c>2) { //三暗刻の保守
			for (i=0;i<34;i++) {
				if (pais2[i]>2) {
					pais2[i]-=3;
				}
			}
		}
		
		for (j=0;j<3;j++) { //順子の抜き出し
			for (i=0;i<7;i++) {
				k=i+j*9;
				do {
					fg=0;
					if ((pais2[k]>0)&&(pais2[k+1]>0)&&(pais2[k+2]>0)) {
						pais2[k]--; pais2[k+1]--; pais2[k+2]--;
						fg=1;
					}
				} while (fg!=0);
			}
		}
		
		
		fg=0;
		for (i=31;i<34;i++) { //雀頭の抜き出し(三元牌)
			if ((pais2[i]==2)&&(seepais[i]>1)) {
				pais2[i]-=2; fg=1; break;
			}
		}
		if (fg==0) { 
			for (i=0;i<4;i++) { //雀頭の抜き出し（風牌）
				k=i+27;
				if (pais2[k]==2) {
					if ((i==baKaze)||(i==(p-oya+4)%4)) {
						if (seepais[k]>1) {
							pais2[k]-=2; fg=1; break;
						}
					} else {
						pais2[k]-=2; fg=1; break;
					}
				}
			}
		}
		if (fg==0) {
			for (i=0;i<27;i++) { //雀頭の抜き出し(対子数牌)
				if (pais2[i]==2) {
					pais2[i]-=2; fg=1; break;
				}
			}
		}
		if (fg==0) {
			for (i=0;i<27;i++) { //雀頭の抜き出し(刻子数牌)
				if (pais2[i]>1) {
					pais2[i]-=2; fg=1; break;
				}
			}
		}
		if (fg==0) { 
			for (i=0;i<4;i++) { //雀頭の抜き出し（刻子オタ風牌）
				k=i+27;
				if (pais2[k]>1) {
					if ((i!=baKaze)&&(i!=(p-oya+4)%4)) {
						pais2[k]-=2; fg=1; break;
					}
				}
			}
		}
		
		
		for (j=0;j<3;j++) { //両面塔子の抜き出し
			for (i=1;i<7;i++) { //捨て牌との具合をチェックあり
				k=i+j*9;
				do {
					fg=0;
					if ((pais2[k]>0)&&(pais2[k+1]>0)) {
						if ((inSutehai(p,k-1,k+2)==false)
						        &&(seepais[k-1]+seepais[k+2]+pais[k-1]+pais[k+2]<7)) {
							pais2[k]--; pais2[k+1]--; fg=1;
							pais3[k]=1; pais3[k+1]=1;
						}
					}
				} while (fg!=0);
			}
		}

		for (i=0;i<27;i++) { //数牌刻子の抜き出し
			if (pais2[i]>2) {
				pais2[i]-=3;
			}
		}

		for (j=0;j<3;j++) { //捨て牌との具合チェックあり
			k=j*9;
			do {
				fg=0;
				if ((pais2[k]>0)&&(pais2[k+1]>0)) { //辺張塔子の抜き出し
					if ((inSutehai(p,k+2)==false)&&(seepais[k+2]+pais[k+2]<4)) {
						pais2[k]--; pais2[k+1]--; fg=1;
						pais3[k]=2; pais3[k+1]=2;
					}
				}
			} while (fg!=0);
			k=7+j*9;
			do {
				fg=0;
				if ((pais2[k]>0)&&(pais2[k+1]>0)) { //辺張塔子の抜き出し
					if ((inSutehai(p,k-1)==false)&&(seepais[k-1]+pais[k-1]<4)) {
						pais2[k]--; pais2[k+1]--; fg=1;
						pais3[k]=2; pais3[k+1]=2;
					}
				}
			} while (fg!=0);
			
			for (i=0;i<7;i++) { //嵌張塔子の抜き出し
				k=i+j*9;
				do {
					fg=0;
					if ((pais2[k]>0)&&(pais2[k+2]>0)) {
						if ((inSutehai(p,k+1)==false)&&(seepais[k+1]+pais[k+1]<4)) {
							pais2[k]--; pais2[k+2]--; fg=1;
							pais3[k]=3; pais3[k+2]=3;
						}
					}
				} while (fg!=0);
			}
		}

		for (j=0;j<3;j++) { //両面塔子の抜き出し
			for (i=1;i<7;i++) { //捨て牌との具合をチェックなし
				k=i+j*9;
				do {
					fg=0;
					if ((pais2[k]>0)&&(pais2[k+1]>0)) {
						pais2[k]--; pais2[k+1]--; fg=1;
						if (pais2[k]==0) pais2[k]=-1;
						if (pais2[k+1]==0) pais2[k+1]=-1;
					}
				} while (fg!=0);
			}
		}

		for (j=0;j<3;j++) { //捨て牌との具合チェックなし
			k=j*9;
			do {
				fg=0;
				if ((pais2[k]>0)&&(pais2[k+1]>0)) { //辺張塔子の抜き出し
					pais2[k]--; pais2[k+1]--; fg=1;
					if (pais2[k]==0) pais2[k]=-2;
					if (pais2[k+1]==0) pais2[k+1]=-2;
				}
			} while (fg!=0);
			k=7+j*9;
			do {
				fg=0;
				if ((pais2[k]>0)&&(pais2[k+1]>0)) { //辺張塔子の抜き出し
					pais2[k]--; pais2[k+1]--; fg=1;
					if (pais2[k]==0) pais2[k]=-2;
					if (pais2[k+1]==0) pais2[k+1]=-2;
				}
			} while (fg!=0);
			
			for (i=0;i<7;i++) { //嵌張塔子の抜き出し
				k=i+j*9;
				do {
					fg=0;
					if ((pais2[k]>0)&&(pais2[k+2]>0)) {
						pais2[k]--; pais2[k+2]--; fg=1;
						if (pais2[k]==0) pais2[k]=-3;
						if (pais2[k+2]==0) pais2[k+2]=-3;
					}
				} while (fg!=0);
			}
		}

		for (i=0;i<4;i++) { //いらない単騎オタ風切り
			if ((i!=baKaze)&&(i!=(p-oya+4)%4)&&(pais2[i+27]==1)) {
				return computeDahai4(p,pai,i+27,pais);
				//selectPai=paiIndex(p,i+27); return TCMD_DAHAI;
			}
		}
		for (i=27;i<34;i++) { //対子も作れない単騎字牌切り
			if ((seepais[i]>2)&&(pais2[i]>0)){
				return computeDahai4(p,pai,i,pais);
				//selectPai=paiIndex(p,i); return TCMD_DAHAI;
			}
		}
		for (i=27;i<34;i++) { //一枚以上切れてる単騎字牌切り
			if ((seepais[i]>0)&&(pais2[i]==1)) {
				return computeDahai4(p,pai,i,pais);
				//selectPai=paiIndex(p,i); return TCMD_DAHAI;
			}
		}
		for (i=27;i<34;i++) { //雀頭にも刻子にもならない字牌切り
			if ((seepais[i]>1)&&(pais2[i]==2)) {
				return computeDahai4(p,pai,i,pais);
				//selectPai=paiIndex(p,i); return TCMD_DAHAI;
			}
		}
		k=rand(3);
		for (j=0;j<3;j++) {
			k=(k+1)%3;
			switch (k) {
			case 0:
				for (i=27;i<34;i++) { //単騎字牌切り
					if (pais2[i]==1) {
						return computeDahai4(p,pai,i,pais);
						//selectPai=paiIndex(p,i); return TCMD_DAHAI;
					}
				} break;
			case 1:
				for (i=0;i<3;i++) { //ヤオチュー（１，９）切り
					if (pais2[i*9]>0) {
						return computeDahai4(p,pai,i*9,pais);
						//selectPai=paiIndex(p,i*9); return TCMD_DAHAI;
					}
					if (pais2[8+i*9]>0) {
						return computeDahai4(p,pai,8+i*9,pais);
						//selectPai=paiIndex(p,8+i*9); return TCMD_DAHAI;
					}
				} break;
			case 2:
				for (i=0;i<4;i++) { //いらない単騎オタ風切り
					if ((i!=baKaze)&&(i!=(p-oya+4)%4)&&(pais2[i+27]==2)) {
						return computeDahai3(p,pai,i+27,pais);
						//selectPai=paiIndex(p,i+27); return TCMD_DAHAI;
					}
				} break;
			}
		}
		k=rand(2);
		for (j=0;j<2;j++) {
			k=(k+1)%2;
			switch (k) {
			case 0:
				for (i=0;i<27;i++) { //数牌の余った対子切り
					if ((seepais[i]>0)&&(pais2[i]==2)) {
						return computeDahai4(p,pai,i,pais);
						//selectPai=paiIndex(p,i); return TCMD_DAHAI;
					}
				} break;
			case 1:
				for (i=0;i<27;i++) { //いらない対子数牌を切る
					if (pais2[i]==2) {
						return computeDahai4(p,pai,i,pais);
						//selectPai=paiIndex(p,i); return TCMD_DAHAI;
					}
				} break;
			}
		}
		for (i=0;i<27;i++) { //いらない単騎数牌を切る
			if (pais2[i]==1) {
				return computeDahai4(p,pai,i,pais);
				//selectPai=paiIndex(p,i); return TCMD_DAHAI;
			}
		}
		c=0;
		for (i=0;i<27;i++)
			if (pais2[i]<0) c++;
		if (c>0) {
			k=rand(c);
			c=0;
			for (i=0;i<27;i++) {
				if (pais2[i]<0) {
					if (c==k) {
						return computeDahai4(p,pai,i,pais);
						//selectPai=paiIndex(p,i); return TCMD_DAHAI;
					}
					c++;
				}
			}
		}
		
		for (i=0;i<27;i++) {
			if (pais3[i]==2) {
				return computeDahai4(p,pai,i,pais);
			}
		}
		for (i=0;i<27;i++) {
			if (pais3[i]==3) {
				return computeDahai4(p,pai,i,pais);
			}
		}
		for (i=0;i<27;i++) {
			if (pais3[i]>0) {
				return computeDahai4(p,pai,i,pais);
			}
		}
		
		//System.out.println("きた");
		
		if (pai>=0) {
			return computeDahai4(p,pai,pai,pais);
			//selectPai=-1; return TCMD_DAHAI;
		}
		return -1;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//コンピュータの副露選択
	private int computeFCommand(int p) {
		int pp=1<<p;
		if ((cpuplayer & pp)>0) {
			if (think[p][0]<0) { //ツモ切りモード
				selectFCmd=FCMD_PASS;
				return FIRE;
			}
			int i,j,t;
			int[] pais=new int[34];
			int n=tehai[p][13];
			for (i=0;i<n;i++)
				pais[tehai[p][i]]++;
				
			if (checkRon(p,dahai)) { //あがれるならロン
				selectFCmd=FCMD_RON;
				return FIRE;
			}
			if (reach[p]) { //リーチなのでパス
				selectFCmd=FCMD_PASS;
				return FIRE;
			}
			
			if (think[p][1]==1) { //国士狙いなので
				selectFCmd=FCMD_PASS;
				return FIRE;
			}

			t=-1;
			switch (think[p][0]) {
			case 1:
				t=computeFCmdType1(p,pais); break;
			case 2:
				t=computeFCmdType2(p,pais); break;
			case 3:
				t=computeFCmdType3(p,pais); break;
			case 4:
				t=computeFCmdType4(p,pais); break;
			}
			if (t>=0) {
				selectFCmd=t;
				return FIRE;
			}
			selectFCmd=FCMD_PASS;
			return FIRE;
		}
		return keyEvent;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	
	private int computeFCmdType1(int p, int[] pais) {
		int i;
		
		if ((tenpai[p])&&(furiten[p]==0)) { //テンパイを崩したくないのでパス
			return FCMD_PASS;
		}			
		if ((checkKan(p,dahai))&&(rinshanIndex<3)) {
			if ((dahai==baKaze+27)||(dahai==(p-oya+4)%4+27)||((dahai>=31)&&(dahai<=33))) {
				if (rand(100)>90) { //気まぐれで役牌をカン
					return FCMD_KAN;
				}
			}
		} //*/
		if (checkPon(p,dahai)==2) { //役牌なら迷わずポン
			if ((dahai==baKaze+27)||(dahai==(p-oya+4)%4+27)||((dahai>=31)&&(dahai<=33))) {
				return FCMD_PON;
			}
		}
		int f=furopai[p][0][0];
		int fg=0,pai;
		for (i=1;i<=f;i++) {
			pai=furopai[p][i][1];
			if ((pai==baKaze+27)||(pai==(p-oya+4)%4+27)||((pai>=31)&&(pai<=33))) {
				fg=1;
				break;
			}
		}
		if ((f>0)&&(f<3)&&(fg>0)) {
			if ((checkKan(p,dahai))&&(rinshanIndex<3)) {
				if (rand(100)>90) { //きまぐれでカンする
					return FCMD_PON;
				}
			} 
			if (checkPon(p,dahai)==2) {
				return FCMD_PON;
			}
			int e=checkChi(p,dahai);
			if (e>0) {
				think[p][2]=rand(e&3);
				return FCMD_CHI;
			}
		}
		return FCMD_PASS;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private int computeFCmdType2(int p, int[] pais) {
		if ((checkKan(p,dahai))&&(rinshanIndex<3)) {
			if ((dahai==baKaze+27)||(dahai==(p-oya+4)%4+27)||((dahai>=31)&&(dahai<=33))) {
				if (rand(100)>90) { //気まぐれで役牌をカン
					return FCMD_KAN;
				}
			}
		} //*/
		if (checkPon(p,dahai)==2) { //役牌なら迷わずポン
			if ((dahai==baKaze+27)||(dahai==(p-oya+4)%4+27)||((dahai>=31)&&(dahai<=33))) {
				return FCMD_PON;
			}
		}
		if ((tenpai[p])&&(furiten[p]==0)) { //テンパイを崩したくないのでパス
			return FCMD_PASS;
		}
		
		int i;
		int f=furopai[p][0][0];
		int fg=0,pai;
		for (i=1;i<=f;i++) {
			pai=furopai[p][i][1];
			if ((pai==baKaze+27)||(pai==(p-oya+4)%4+27)||((pai>=31)&&(pai<=33))) {
				fg=1;
				break;
			}
		}
		if (fg==0) {
			if (pais[baKaze+27]==3) fg++;
			if (pais[(p-oya+4)%4+27]==3) fg++;
			for (i=31;i<34;i++)
				if (pais[i]==3) fg++;
		}
		if (fg==0) return FCMD_PASS;

		int[] pais2=new int[34];
		for (i=0;i<34;i++)
			pais2[i]=pais[i];
		
		int e,j,k,c,r;
		c=0;
		
		for (j=0;j<3;j++) { //順子抜き出し
			do {
				e=0;
				for (i=0;i<7;i++) {
					k=i+j*9;
					if ((pais2[k]>0)&&(pais2[k+1]>0)&&(pais2[k+2]>0)) {
						pais2[k]--;
						pais2[k+1]--;
						pais2[k+2]--;
						e=1;
						c+=3;
					}
				}
			} while (e>0);
		}
		do { //刻子抜き出し
			e=0;
			for (i=0;i<34;i++) {
				if (pais2[i]>2) {
					pais2[i]-=3;
					e=1;
					c+=3;
				}
			}
		} while (e>0);
		
		for (i=33;i>=0;i--) { //雀頭抜き出し
			if (pais2[i]==2) {
				pais2[i]=0;
				c+=2;
				break;
			}
		}
		
		if (pais2[dahai]==2) {
			return FCMD_PON;
		}
		
		if (dahai<27) {
			k=dahai%9;
			e=checkChi(p,dahai);
			if (k<7) {
				if ((pais2[dahai+1]>0)&&(pais2[dahai+2]>0)) {
					if ((e&(1<<3))>0) {
						think[p][2]=0;
						return FCMD_CHI;
					}
				}
			}
			if ((k>0)&&(k<8)) {
				if ((pais2[dahai-1]>0)&&(pais2[dahai+1]>0)) {
					if ((e&(1<<4))>0) {
						r=0;
						if ((e&(1<<3))>0) r++;
						think[p][2]=r;
						return FCMD_CHI;
					}
				}
			}
			if (k>1) {
				if ((pais2[dahai-1]>0)&&(pais2[dahai-2]>0)) {
					if ((e&(1<<5))>0) {
						r=0;
						if ((e&(1<<3))>0) r++;
						if ((e&(1<<4))>0) r++;
						think[p][2]=r;
						return FCMD_CHI;
					}
				}
			}
		}
		
		return FCMD_PASS;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private int computeFCmdType3(int p, int[] pais) {
		int i;

		switch (think[p][1]) {
		case 2: //七対子
			return FCMD_PASS;
		case 3:
			if (checkPon(p,dahai)==2)
				return FCMD_PON;
			return FCMD_PASS;
		case 100:
			return FCMD_PASS;
		}
		if (pais[dahai]==2) {
			if ((dahai==baKaze+27)||(dahai==(p-oya+4)%4+27)||(dahai>30)) {
				if ((checkPon(p,dahai)>1)&&(seepais[dahai]>0)) {
					return FCMD_PON;
				}
			}
		}
		return FCMD_PASS;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private int computeFCmdType4(int p, int[] pais) {
		int i,j,k,c,d,fg,e,r,pp,n;

		switch (think[p][1]) {
		case 2: //七対子
			return FCMD_PASS;
		case 3:
			if (checkPon(p,dahai)==2)
				return FCMD_PON;
			return FCMD_PASS;
		}
		if (pais[dahai]==2) { //役牌
			if ((dahai==baKaze+27)||(dahai==(p-oya+4)%4+27)||(dahai>30)) {
				if ((checkPon(p,dahai)>1)&&(seepais[dahai]>0)) {
					return FCMD_PON;
				}
			}
		}
		
		int f=furopai[p][0][0];
		
		fg=0; d=0;
		for (i=1;i<=f;i++) {
			pp=furopai[p][i][1];
			if ((pp==baKaze+27)||(pp==(p-oya+4)%4+27)||(pp>30)) {
				fg=1; break;
			}
			c=pp%9;
			if ((((c==0)||(c>5))&&(pp<27))||(pp>=27)) {
				d++;
			}
		}
		
		if ((d==0)&&(rule_kuitan)&&(dahai<27)) {
			n=tehai[p][13]; c=0;
			for (i=0;i<n;i++) {
				pp=tehai[p][i];
				k=(pp%9)%8;
				if (((k==0)&&(pp<27))||(pp>26)) {
					c=1; break;
				}
			}
			k=(dahai%9)%8;
			if ((k>0)&&(c==0)) {
				fg=1;
				if (think[p][1]!=4) think[p][1]=4; //喰い断
			}
		}
		
		if (fg==0) return FCMD_PASS;

		int[] pais2=new int[34];
		for (i=0;i<34;i++)
			pais2[i]=pais[i];
		
		if (menzen[p]) {
			for (j=0;j<3;j++) { //一盃口の抜き出し
				for (i=0;i<7;i++) {
					k=i+j*9;
					do {
						fg=0;
						if ((pais2[k]>1)&&(pais2[k+1]>1)&&(pais2[k+2]>1)) {
							pais2[k]-=2; pais2[k+1]-=2; pais2[k+2]-=2;
							fg=1;
						}
					} while (fg!=0);
				}
			}
		}
		for (j=0;j<3;j++) { //一気通貫の抜き出し
			c=0;
			for (i=0;i<9;i++) {
				k=i+j*9;
				if (pais2[k]==0) {
					c=1; break;
				}
			}
			if (c==0) {
				for (i=0;i<9;i++) {
					k=i+j*9;
					pais2[k]--;
				}
			}
		}
		for (i=0;i<7;i++) { //三色同順の抜き出し
			fg=0;
			for (j=0;j<3;j++) {
				k=i+j*9;
				if ((pais2[k]>0)&&(pais2[k+1]>0)&&(pais2[k+2]>0)) {
					fg++;
				}
			}
			if (fg==3) {
				for (j=0;j<3;j++) {
					k=i+j*9;
					for (int u=0;u<3;u++) {
						pais2[k+u]--;
					}
				}
			}
		}
		c=0;
		for (i=0;i<34;i++) { //暗刻
			if (pais2[i]>2) {
				c++;
			}
		}
		if (c>2) { //三暗刻の保守
			for (i=0;i<34;i++) {
				if (pais2[i]>2) {
					pais2[i]-=3;
				}
			}
		}

		for (j=0;j<3;j++) { //順子の抜き出し
			for (i=0;i<7;i++) {
				k=i+j*9;
				do {
					fg=0;
					if ((pais2[k]>0)&&(pais2[k+1]>0)&&(pais2[k+2]>0)) {
						pais2[k]--; pais2[k+1]--; pais2[k+2]--;
						fg=1;
					}
				} while (fg!=0);
			}
		}
		
		fg=0;
		for (i=31;i<34;i++) { //雀頭の抜き出し(三元牌)
			if ((pais2[i]==2)&&(seepais[i]>1)) {
				pais2[i]-=2; fg=1; break;
			}
		}
		if (fg==0) { 
			for (i=0;i<4;i++) { //雀頭の抜き出し（風牌）
				k=i+27;
				if (pais2[k]==2) {
					if ((i==baKaze)||(i==(p-oya+4)%4)) {
						if (seepais[k]>1) {
							pais2[k]-=2; fg=1; break;
						}
					} else {
						pais2[k]-=2; fg=1; break;
					}
				}
			}
		}
		if (fg==0) {
			for (i=0;i<27;i++) { //雀頭の抜き出し(対子数牌)
				if (pais2[i]==2) {
					pais2[i]-=2; fg=1; break;
				}
			}
		}
		if (fg==0) {
			for (i=0;i<27;i++) { //雀頭の抜き出し(刻子数牌)
				if (pais2[i]>1) {
					pais2[i]-=2; fg=1; break;
				}
			}
		}
		if (fg==0) { 
			for (i=0;i<4;i++) { //雀頭の抜き出し（刻子オタ風牌）
				k=i+27;
				if (pais2[k]>1) {
					if ((i!=baKaze)&&(i!=(p-oya+4)%4)) {
						pais2[k]-=2; fg=1; break;
					}
				}
			}
		}
		
		e=checkChi(p,dahai);
		
		for (j=0;j<3;j++) { //両面塔子の抜き出し
			for (i=1;i<7;i++) {
				k=i+j*9;
				do {
					fg=0;
					if ((pais2[k]>0)&&(pais2[k+1]>0)) {
						if (k-1==dahai) {
							think[p][2]=0;
							return FCMD_CHI;
						} 
						if (k+2==dahai) {
							r=0;
							if ((e&(1<<3))>0) r++;
							if ((e&(1<<4))>0) r++;
							think[p][2]=r;
							return FCMD_CHI;
						} 
						pais2[k]--; pais2[k+1]--; fg=1;
					}
				} while (fg!=0);
			}
		}

		for (i=0;i<27;i++) { //数牌刻子の抜き出し
			if (pais2[i]>2) {
				pais2[i]-=3;
			}
		}

		for (j=0;j<3;j++) { //捨て牌との具合チェックなし
			k=j*9;
			do {
				fg=0;
				if ((pais2[k]>0)&&(pais2[k+1]>0)) { //辺張塔子の抜き出し
					if (k+2==dahai) {
						r=0;
						if ((e&(1<<3))>0) r++;
						if ((e&(1<<4))>0) r++;
						think[p][2]=r;
						return FCMD_CHI;
					} 
					pais2[k]--; pais2[k+1]--; fg=1;
				}
			} while (fg!=0);
			k=7+j*9;
			do {
				fg=0;
				if ((pais2[k]>0)&&(pais2[k+1]>0)) { //辺張塔子の抜き出し
					if (k+2==dahai) {
						think[p][2]=0;
						return FCMD_CHI;
					} 
					pais2[k]--; pais2[k+1]--; fg=1;
				}
			} while (fg!=0);
			
			for (i=0;i<7;i++) { //嵌張塔子の抜き出し
				k=i+j*9;
				do {
					fg=0;
					if ((pais2[k]>0)&&(pais2[k+2]>0)) {
						if (k+1==dahai) {
							r=0;
							if ((e&(1<<3))>0) r++;
							think[p][2]=r;
							return FCMD_CHI;
						}
						pais2[k]--; pais2[k+2]--; fg=1;
					}
				} while (fg!=0);
			}
		}
		
		if (pais2[dahai]==2) {
			if (checkPon(p,dahai)>1) {
				return FCMD_PON;
			}
		}
		
		return FCMD_PASS;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//コンピュータのポン・チー時の打牌選択
	private int computeFDahai(int p) {
		int pp=1<<p;
		if ((cpuplayer & pp)>0) {
			int i,j,k;
			int[] pais=new int[34];
			int n=tehai[p][13];

			//牌のカウント
			for (i=0;i<n;i++)
				pais[tehai[p][i]]++;
			
			//喰い替え牌（この処理は各思考ルーチンで処理したほうがよく、副露打牌用のルーチン作ったほうがいいかも
			if (kuikae[0]>=0) { //喰い替え牌(現物)を選択肢から除去
				pais[kuikae[0]]=0;
			}
			if (kuikae[1]>=0) { //喰い替え牌(スジ)を選択肢から除去
				pais[kuikae[1]]=0;
			}
			
			int t=-1;
			switch (think[p][0]) {
			case 1: 
				t=computeTCmdType1(p,-1,pais);
				break;
			case 2: 
				t=computeTCmdType2(p,-1,pais);
				break;
			case 3: 
				t=computeTCmdType3(p,-1,pais);
				break;
			case 4: 
				t=computeTCmdType4(p,-1,pais);
				break;
			}
			if ((t>=0)&&(selectPai>=0)) {
				return FIRE;
			}
			
			do {
				selectPai=rand(tehai[p][13]);
			} while ((tehai[p][selectPai]==kuikae[0])||(tehai[p][selectPai]==kuikae[1]));
			return FIRE;
		}
		return keyEvent;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@  チェック系  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	
	//リーチ可能か
	private boolean checkReach(int p, int spai, int dpai) {
		if ((score[p]>=1000)&&(menzen[p])&&(reach[p]==false)&&(ryukyokuIndex-tumoIndex>4)) {
			if (checkTenpai(p,spai,dpai)) {
				return true;
			}
		}
		return false;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //


	//ポン可能か
	private int checkPon(int p, int pai) {
		int i,n,c=0;
		if (reach[p]) return 0;
		if (ryukyokuIndex-tumoIndex==1) return 0;
		if ((rinshanhai)&&(minkan)&&(rinshanIndex==3)&&(rkk_4kaikan<0)) return 0;
		if ((rinshanhai)&&(rinshanIndex==4)&&(rkk_4kaikan<0)) return 0;
		n=tehai[p][13];
		if (n<2) return 0;
		for (i=0;i<n;i++) {
			if (tehai[p][i]==pai)
				c++;
		}
		return c;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//カン可能か
	private boolean checkKan(int p, int pai) {
		int i,n,c=0;
		if (reach[p]) return false;
		if (ryukyokuIndex-tumoIndex==1) return false;
		if ((rinshanhai)&&(minkan)&&(rinshanIndex==3)&&(rkk_4kaikan<0)) return false;
		if (rinshanIndex==4) return false;
		n=tehai[p][13];
		if (n<3) return false;
		for (i=0;i<n;i++) {
			if (tehai[p][i]==pai)
				c++;
		}
		return (c>2);
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//チー可能か
	private int checkChi(int p, int pai) {
		if (reach[p]) return 0;
		if (ryukyokuIndex-tumoIndex==1) return 0;
		if ((rinshanhai)&&(minkan)&&(rinshanIndex==3)&&(rkk_4kaikan<0)) return 0;
		if ((rinshanhai)&&(rinshanIndex==4)&&(rkk_4kaikan<0)) return 0;
		if ((frCheck>1)||(pai>26)) return 0;
		int i,n,c1,c2,c3,c4,m,k=pai%9;
		int r1=0,r2=0;
		n=tehai[p][13];
		if (n<2) return 0;
		c1=c2=c3=c4=0;
		for (i=0;i<n;i++) {
			if ((tehai[p][i]==pai-2)&&(k>1)) c1++;
			if ((tehai[p][i]==pai-1)&&(k>0)) c2++;
			if ((tehai[p][i]==pai+1)&&(k<8)) c3++;
			if ((tehai[p][i]==pai+2)&&(k<7)) c4++;
		}
		if ((c1>0)&&(c2>0)) {
			m=0; 
			if (n==4) { //喰い替えの制限
				for (i=0;i<n;i++) {
					if (tehai[p][i]==pai) m++; //現物
					if ((tehai[p][i]==pai-3)&&(k>2)) m++; //スジ
				}
			}
			if (m<2) {
				r1=(1<<3);  r2=1;
			}
		}
		if ((c2>0)&&(c3>0)) { 
			m=0; 
			if (n==4) { //喰い替えの制限
				for (i=0;i<n;i++) {
					if (tehai[p][i]==pai) m++; //現物
				}
			}
			if (m<2) {
				r1|=(1<<4); r2++;
			}
		}
		if ((c3>0)&&(c4>0)) {
			m=0; 
			if (n==4) { //喰い替えの制限
				for (i=0;i<n;i++) {
					if (tehai[p][i]==pai) m++; //現物
					if ((tehai[p][i]==pai+3)&&(k<6)) m++; //スジ
				}
			}
			if (m<2) {
				r1|=(1<<5); r2++;
			}
		}
		return (r1|r2);
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private boolean checkTumo(int p, int pai) {
		return (t_agari[p][pai])||((rinshanhai||haitei)&&(k_agari[p][pai]));
	}

	private boolean checkRon(int p, int pai) {
		if (furiten[p]>0) return false;
		if ((haitei)&&((r_agari[p][pai])||(t_agari[p][pai])||(k_agari[p][pai])))
			return true;
		if ((kakan)&&((r_agari[p][pai])||(t_agari[p][pai])||(k_agari[p][pai])))
			return true;
		return r_agari[p][pai];
			
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//副露可能か
	private boolean checkFuro(int p, int pai) {
		return ((checkPon(p,pai)>1)||checkKan(p,pai)||(checkChi(p,pai)>0)
		          ||(checkRon(p,pai)));
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//暗カンできるか
	private boolean checkAnkan(int p, int pai) {
		int i,n,c=0;
		if (ryukyokuIndex-tumoIndex==1) return false;
		if (rinshanIndex==4) return false;
		if (reach[p]) { //リーチ後なら
			int pp=getSelectPai(p,-1);
			checkTenpai(p,-1,-1);
			//for (i=0;i<34;i++)
			//	System.out.println("ankan["+i+"]="+c_ankan[i]);
			return c_ankan[pp];
		}
		n=tehai[p][13];
		for (i=0;i<n;i++) {
			if (tehai[p][i]==pai)
				c++;
		}
		if (rinshanhai) {
			if (yama[135-rinshanIndex]==pai) 
				c++;
		} else {
			if (yama[tumoIndex]==pai) 
				c++;
		}
		return (c==4);
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//加カンできるか
	private boolean checkKakan(int p, int pai) {
		int f=furopai[p][0][0];
		int n=tehai[p][13];
		int i;
		if (ryukyokuIndex-tumoIndex==1) return false;
		if (rinshanIndex==4) return false;
		if (f==0) return false;
		for (i=1;i<=f;i++) {
			if (furopai[p][i][0]==FURO_PON) {
				if (furopai[p][i][1]==pai)
					return true;
			}
		}
		return false;
	}
	
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//テンパイチェック
	//  翻数や符を計算するのではなく、テンパイしてるかを判定するだけ
	//  あがり牌がロンできるかツモできるかや形式テンパイかの区別は判定する
	private boolean checkTenpai(int p, int dpai, int tpai) {
		int[]   pais=new int[34];
		int[]   supais=new int[3];
		int     n=tehai[p][13];
		int     f=furopai[p][0][0];
		int     i,j,k,pp;
		int     tanyao=0;
		int[]   u=new int[5];
//		int[][] v=new int[3][7];
		
		//System.out.println("[checkTenpai("+p+","+dpai+","+tpai+")]");
		
		for (i=0;i<34;i++) { 
			c_r_agari[i]=false;
			c_t_agari[i]=false;
			c_k_agari[i]=false;
			c_ankan[i]=false;
		}
		//System.out.println("...reset flags");
		
		for (i=0;i<n;i++) { //手牌の牌の統計を取る
			pp=tehai[p][i];
			pais[pp]++;
			if (pp<27) {
				supais[pp/9]++;
				k=pp%9;
				if ((0<k)&&(k<8))
					tanyao++;
			}
		}
		//System.out.println("...tehai");
		
		if (dpai>=0) { //打牌した牌があるなら打牌した牌を抜く
			pais[dpai]--;
			if (dpai<27) {
				supais[dpai/9]--;
				k=dpai%9;
				if ((0<k)&&(k<8))
					tanyao--;
			}
		}
		//System.out.println("...dapai");
		
		if (tpai>=0) { //ツモ牌があるならツモ牌を加える
			pais[tpai]++;
			if (tpai<27) {
				supais[tpai/9]++;
				k=tpai%9;
				if ((0<k)&&(k<8))
					tanyao++;
			}
		}
		//System.out.println("...tumopai");
		
		//国士無双チェック
		if ((f==0)&&(tanyao==0)) {
			u[0]=-1; u[1]=u[2]=0;
			for (i=0;i<34;i++) {
				if (i<27) {
					k=i%9;
				} else {
					k=0;
				}
				if ((k==0)||(k==8)) {
					if (pais[i]==0)
						u[0]=i;
					if (pais[i]==1)
						u[1]++;
					if (pais[i]==2)
						u[2]++;
				}
			}
			if (u[1]==13) { //国士無双１３面待ち
				for (i=0;i<3;i++) {
					c_r_agari[i*9]=true;
					c_t_agari[i*9]=true;
					c_r_agari[i*9+8]=true;
					c_t_agari[i*9+8]=true;
				}
				for (i=27;i<34;i++) {
					c_r_agari[i]=true;
					c_t_agari[i]=true;
				}
				return true;
			}
			if ((u[1]==11)&&(u[2]==1)) { //国士無双単機待ち
				c_r_agari[u[0]]=true;
				c_t_agari[u[0]]=true;
				return true;
			}
		}
		//System.out.println("...kokushi");
		
		u[0]=u[1]=u[2]=0;
		for (i=27;i<34;i++) {
			u[pais[i]]++;
		}
		if (u[4]>0) return false; //槓子にしてない４個の特定の字牌がある
		if ((u[2]>2)&&(u[3]>0)) return false; //刻子があり七対子出来ないのに対子が複数ある
		if ((u[1]>0)&&(u[2]>0)&&(u[3]>0)) return false; //雀頭になれない対子・単騎がある
		for (i=0;i<3;i++)
			if (supais[i]==1) u[1]++;
		if (u[1]>1) return false; //単騎が２個以上
		//System.out.println("...simple checks");
		
		if (((u[2]<=2)&&(u[1]==0))||(u[2]==0)) { //七対子系でないテンパイを調べる
			//System.out.println("...start divide ments");
		
			Ments ms=new Ments((p-oya+4)%4);
			int henda=0;
			
			for (i=1;i<=f;i++) {
				if (ms.add(furopai[p][i][0],furopai[p][i][1])==false) henda++;
			}
			
			for (i=27;i<34;i++) { //字牌の
				switch (pais[i]) {
				case 1:
					if (ms.add(MT_TANKI,i)==false) henda++;
					break;
				case 2:
					if (ms.add(MT_TOITSU,i)==false) henda++;
					break;
				case 3:
					if (ms.add(MT_ANKO,i)==false) henda++;
					break;
				}
			}
			//System.out.println("...tupai");
			
			if (henda==0) {
				//System.out.println("...start supais");
			
				MentsSet mss1=new MentsSet();
				mss1.check(0,0,supais[0],pais,ms);
				
				MentsSet mss2=new MentsSet();
				for (i=0;i<mss1.count;i++) {
					mss2.check(0,9,supais[1],pais,mss1.ments[i]);
				}
				
				mss1=new MentsSet();
				for (i=0;i<mss2.count;i++) {
					mss1.check(0,18,supais[2],pais,mss2.ments[i]);
				}
				mss2=null;
				
				//System.out.println("...end to divide");
				
				boolean tmp=false;;
				
				for(j=0;j<34;j++)
					c_ankan[j]=true;
				
				Ments tms;
				for (i=0;i<mss1.count;i++) {
					tms=mss1.ments[i];
					
					for (j=0;j<34;j++) {
						if (tms.c_anko[j]==false) {
							c_ankan[j]=false;
						}
					}
					
					k=tms.tenpai();
					switch (k) {
					case 0: //形式テンパイっぽい
						for (j=0;j<2;j++) {
							pp=tms.machi[j];
							if (pp>=0) {
								c_k_agari[pp]=true;
								if (menzen[p])
									c_t_agari[pp]=true; 
							}
						}
						tmp=true;
						break;
					case 1: //確実に役ある
					case 4: //どちらでも役がつく
						for (j=0;j<2;j++) {
							pp=tms.machi[j];
							if (pp>=0) {
								c_r_agari[pp]=true;
								c_t_agari[pp]=true;
							}
						}
						tmp=true;
						break;
					case 2: //machi[0]なら役がつく
						pp=tms.machi[0];
						if (pp>=0) {
							c_r_agari[pp]=true;
							c_t_agari[pp]=true;
						}
						pp=tms.machi[1];
						if (pp>=0) {
							c_k_agari[pp]=true;
							if (menzen[p])
								c_t_agari[pp]=true;
						}
						tmp=true;
						break;
					case 3: //machi[1]なら役がつく
						pp=tms.machi[0];
						if (pp>=0) {
							c_k_agari[pp]=true;
							if (menzen[p])
								c_t_agari[pp]=true;
						}
						pp=tms.machi[1];
						if (pp>=0) {
							c_r_agari[pp]=true;
							c_t_agari[pp]=true;
						}
						tmp=true;
						break;
					}
					//if (p==0) {
					/*
					System.out.println("**** checkTenpai "+p+","+i+","+k+" ****");
					for (j=0;j<5;j++)
						System.out.println(" 面子["+j+"]"+sss[tms.menz[j][0]]+","+painame(tms.menz[j][1]));
					System.out.println(" 和了牌 "+painame(tms.machi[0])+","+painame(tms.machi[1]));
					System.out.println(" 符="+tms.fu);
					System.out.println(" ピンフ "+tms.pinfu+", タンヤオ "+tms.tanyao);
					System.out.println(" 暗刻"+tms.anko);
					for (j=0;j<34;j++) 
						System.out.println("ankan["+j+"]="+tms.c_anko[j]);
					//*/
					//}
				}
				if (tmp) {
					//ここに空聴チェックを入れる
					int[] pais2=new int[34];
					for (i=0;i<34;i++) { //コピー
						pais2[i]=pais[i];
					}
					for (i=1;i<=f;i++) { //副露牌を加える
						pp=furopai[p][i][1];
						switch (furopai[p][i][0]) {
						case FURO_CHI: //チー
							for (j=0;j<3;j++)
								pais2[pp+j]++;
							break;
						case FURO_PON: //ポン
							pais2[pp]+=3;
							break;
						case FURO_MINKAN:
						case FURO_ANKAN:
							pais2[pp]+=4;
							break;
						}
					}
					int fffg1=-1;
					for (i=0;i<34;i++) {
						if ((c_r_agari[i])||(c_t_agari[i])||(c_k_agari[i])) {
							if (pais2[i]<4) {
								fffg1=1;
								break;
							}
						}
					}
					if (fffg1>0) {
						return true;
					} else {
						for (i=0;i<34;i++) {
							c_r_agari[i]=false;
							c_t_agari[i]=false;
							c_k_agari[i]=false;
						}
						return false;
					}
				}
			}
		} //七対子以外のテンパイはここで抜ける必要性がある（二盃口が七対子と同形でピンフ型の待ちと区別するため）
		
		//System.out.println("...start chitoi");
		//七対子
		if (f==0) {
			for (j=0;j<34;j++)
				c_ankan[j]=false;
			pp=0;
			u[1]=u[2]=0;
			for (i=0;i<34;i++) {
				u[pais[i]]++;
				if (pais[i]==1)
					pp=i;
			}
			if ((u[1]==1)&&(u[2]==6)) { //七対子テンパイ
				c_r_agari[pp]=true;
				c_t_agari[pp]=true;
				return true;
			}
		}
		
		return false;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void checkFuriten1(int p) {
		int i,j,n,pp;

		for (i=0;i<34;i++) { //テンパイチェックの値をセット
			r_agari[p][i]=c_r_agari[i];
			t_agari[p][i]=c_t_agari[i];
			k_agari[p][i]=c_k_agari[i];
		}
		n=sutehai[p][0][0];
		furiten[p]=(furiten[p]/2)*2;
		for (i=1;i<=n;i++) { //フリテンチェック
			pp=sutehai[p][i][0];
			if ((c_r_agari[pp])||(c_t_agari[pp])||(c_k_agari[pp])) {
				furiten[p]=1; //フリテンフラグ
				break;
			}
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void checkYaku(int p, int apai, int agari) {
		int[]    pais=new int[34];
		int[]    supais=new int[3];
		int      n=tehai[p][13];
		int      f=furopai[p][0][0];
		int      i,pp,k,j, t;
		int      tanyao=0,yc=0,tupai=0;
		int[]    u=new int[5];
		boolean  chitoi=true; //二盃口の場合falseにして七対子チェックをスルー
		
		yk_score=0;
		yk_fu=0;
		yk_han=0;
		yk_yakuman=0;
		yaku[0][0]=0;
		
		for (i=0;i<n;i++) { //手牌の牌の統計を取る
			pp=tehai[p][i];
			pais[pp]++;
			if (pp<27) {
				supais[pp/9]++;
				k=pp%9;
				if ((0<k)&&(k<8))
					tanyao++;
			} else {
				tupai++;
			}
		}
		
		//天和・地和
		if ((firstRotate>0)&&(agari==AG_TUMO)) {
			yc++;
			if (p==oya) 
				yaku[yc][0]=YK_TENHO;
			else
				yaku[yc][0]=YK_CHIHO;
			yaku[yc][1]=1;
			yk_yakuman++;
		}
		
		
		//国士無双チェック
		if ((f==0)&&(tanyao==0)) {
			u[1]=0; u[2]=0;
			for (i=0;i<34;i++) {
				if (i<27) {
					k=i%9;
				} else {
					k=0;
				}
				if ((k==0)||(k==8)) {
					if (pais[i]==1)
						u[1]++;
					if (pais[i]==2)
						u[2]++;
				}
			}
			if ((u[1]==11)&&(u[2]==1)) { 
				yc++;
				yaku[yc][0]=YK_KOKUSHI;
				yaku[yc][1]=1;
				yk_yakuman++;
				yaku[0][0]=yc;
				return;
			}
			if (u[1]==13) {
				yc++;
				yaku[yc][0]=YK_KOKUSHI13;
				yaku[yc][1]=1;
				yk_yakuman++;
				yaku[0][0]=yc;
				return;
			}
		}		
		
		//七対子以外のテンパイチェック
		//役満を先にチェックすること
		
		
		//手牌等の解析
		
		Ments ms=new Ments((p-oya+4)%4);
		int henda=0,mt=-1;
		
		for (i=1;i<=f;i++) {
			if (ms.add(furopai[p][i][0],furopai[p][i][1])==false) henda++;
		}
		
		for (i=27;i<34;i++) { //字牌の
			switch (pais[i]) {
			case 1:
				if (ms.add(MT_TANKI,i)==false) henda++;
				break;
			case 2:
				if (ms.add(MT_TOITSU,i)==false) henda++;
				break;
			case 3:
				if (ms.add(MT_ANKO,i)==false) henda++;
				break;
			}
		}
		
		MentsSet mss1=new MentsSet();
		if (henda==0) {
		
			mss1.check(0,0,supais[0],pais,ms);
			
			MentsSet mss2=new MentsSet();
			for (i=0;i<mss1.count;i++) {
				mss2.check(0,9,supais[1],pais,mss1.ments[i]);
			}
			
			mss1=new MentsSet();
			for (i=0;i<mss2.count;i++) {
				mss1.check(0,18,supais[2],pais,mss2.ments[i]);
			}
			mss2=null;
		}
		
		ms=null;
		for (i=0;i<mss1.count;i++) {
			ms=mss1.ments[i];
			if (ms.tenpai()>=0) {
				if ((ms.machi[0]==apai)||(ms.machi[1]==apai)) {
					if (mt<0) {
						mt=i;
					} else {
						int han1=ms.hancount(apai,agari);
						int han2=mss1.ments[mt].hancount(apai,agari);
						if (han1>han2) {
							mt=i;
						} else if (han1==han2) {
							if (ms.agariFu(apai,agari)>mss1.ments[mt].agariFu(apai,agari)) {
								mt=i;
							}
						}
						
					}
				}
			}
		}
		//mss1のments[mt]があがりの分け方
		if (chitoi=(mt<0))
			ms=null;
		else
			ms=mss1.ments[mt];
		
		//緑一色
		k=pais[19]+pais[20]+pais[21]+pais[23]+pais[25]+pais[32];
		switch (apai) {
		case 19:
		case 20:
		case 21:
		case 23:
		case 25:
		case 32:
			k++;
			break;
		}
		for (i=1;i<=f;i++) {
			pp=furopai[p][i][1];
			switch (furopai[p][i][0]) {
			case FURO_CHI:
				if (pp==19) k+=3;
				break;
			default:
				switch (pp) {
				case 19:
				case 20:
				case 21:
				case 23:
				case 25:
				case 32:
					k+=3;
					break;
				}
				break;
			}
		}
		if (k==14) {
			yk_yakuman++;
			yc++;
			yaku[yc][0]=YK_RYUISO;
			yaku[yc][1]=1;
		}
		
		//字一色
		k=0;
		for (i=27;i<34;i++) k+=pais[i];
		if ((27<=apai)&&(apai<34)) k++;
		for (i=1;i<=f;i++) {
			pp=furopai[p][i][1];
			if ((27<=pp)&&(pp<34)) k+=3;
		}
		if (k==14) {
			yk_yakuman++;
			yc++;
			yaku[yc][0]=YK_TUISO;
			yaku[yc][1]=1;
		}

		//大四喜・小四喜
		k=0;
		for (i=27;i<31;i++) k+=pais[i];
		if ((27<=apai)&&(apai<31)) k++;
		for (i=1;i<=f;i++) {
			pp=furopai[p][i][1];
			if ((27<=pp)&&(pp<31)) k+=3;
		}
		if (k==12) {
			yk_yakuman++;
			yc++;
			yaku[yc][0]=YK_DAISUSHI;
			yaku[yc][1]=1;
		}
		if (k==11) {
			yk_yakuman++;
			yc++;
			yaku[yc][0]=YK_SHOSUSHI;
			yaku[yc][1]=1;
		}
		
		//大三元
		k=0;
		for (i=31;i<34;i++) k+=pais[i];
		if ((31<=apai)&&(apai<34)) k++;
		for (i=1;i<=f;i++) {
			pp=furopai[p][i][1];
			if ((31<=pp)&&(pp<34)) k+=3;
		}
		if (k==9) {
			yk_yakuman++;
			yc++;
			yaku[yc][0]=YK_DAISANGEN;
			yaku[yc][1]=1;
		}
		
		//九蓮宝燈
		if (f==0)
			for (i=0;i<3;i++) {
				if (supais[i]==n) {
					u[1]=0; u[0]=u[2]=-1;
					for (j=1;j<8;j++) {
						switch (pais[i*9+j]) {
						case 0: u[0]=i*9+j; break;
						case 1: u[1]++; break;
						case 2: u[2]=i*9+j; break;
						}
					}
					k=-1;
					switch (pais[i*9]) {
					case 2:
						if ((u[1]==7)&&(u[0]<0)&&(u[2]<0)&&(pais[i*9+8]==4)) {
							if (apai%9==0) k=1;
						}
						if ((u[1]==6)&&(u[0]<0)&&(u[2]>=0)&&(pais[i*9+8]==3)) {
							if (apai%9==0) k=1;
						}
						break;
					case 3:
						if ((u[1]==7)&&(pais[i*9+8]==3)) {
							if (apai/9==i) k=2;  //９面待ち
						}
						if ((u[1]==6)&&(u[2]<0)&&(u[0]>=0)&&(pais[i*9+8]==4)) {
							if (apai==u[0]) k=1;
						}
						if ((u[1]==6)&&(u[2]>=0)&&(u[0]<0)&&(pais[i*9+8]==2)) {
							if (apai%9==8) k=1;
						} 
						if ((u[1]==5)&&(u[2]>=0)&&(u[0]>=0)&&(pais[i*9+8]==3)) {
							if (apai==u[0]) k=1;
						}
						break;
					case 4:
						if ((u[1]==7)&&(u[0]<0)&&(u[2]<0)&&(pais[i*9+8]==2)) {
							if (apai%9==8) k=1;
						}
						if ((u[1]==6)&&(u[0]>=0)&&(u[2]<0)&&(pais[i*9+8]==3)) {
							if (apai==u[0]) k=1;
						}
						break;
					}
					if (k>0) {
						yk_yakuman++;
						yc++;
						if (k==2)
							yaku[yc][0]=YK_CHUREN9;
						else
							yaku[yc][0]=YK_CHUREN;
						yaku[yc][1]=1;
					}
				}
			}
		
		if (mt>=0) {
			//清老頭
			if (ms.chinro==5) {
				yk_yakuman++;
				yc++;
				yaku[yc][0]=YK_CHINRO;
				yaku[yc][1]=1;
			}

			//四暗刻
			if (ms.anko==4) {
				yk_yakuman++;
				yc++;
				yaku[yc][0]=YK_4ANKOTANKI;
				yaku[yc][1]=1;
			}
			if (ms.anko==3) {
				if ((ms.toitsu>1)&&(agari==AG_TUMO)) {
					yk_yakuman++;
					yc++;
					yaku[yc][0]=YK_4ANKO;
					yaku[yc][1]=1;
				}
			}
	
			//四槓子
			if (ms.kantsu==4) {
				yk_yakuman++;
				yc++;
				yaku[yc][0]=YK_4KANTSU;
				yaku[yc][1]=1;
			}
		}
		
		if (yk_yakuman>0) { //役満確定ならここで抜ける
			yaku[0][0]=yc;
			return;
		} //以下役満以外の役の判定
		
		//門前清自摸和
		if ((menzen[p])&&(agari==AG_TUMO)) {
			yk_han++;
			yc++;
			yaku[yc][0]=YK_MENZENTUMO;
			yaku[yc][1]=1;
		}
		
		//嶺上開花
		if ((rinshanhai)&&(agari==AG_TUMO)) {
			yk_han++;
			yc++;
			yaku[yc][0]=YK_RINSHANTUMO;
			yaku[yc][1]=1;
		}
		
		//海底摸月 @チェック
		if ((haitei)&&(rinshanhai==false)&&(agari==AG_TUMO)) {
			yk_han++;
			yc++;
			yaku[yc][0]=YK_HAITEI;
			yaku[yc][1]=1;
		}
		
		//河底撈魚
		if ((haitei)&&(agari==AG_RON)) {
			yk_han++;
			yc++;
			yaku[yc][0]=YK_HOTEI;
			yaku[yc][1]=1;
		}
		
		//槍槓
		if ((kakan)&&(agari==AG_RON)) {
			yk_han++;
			yc++;
			yaku[yc][0]=YK_CHANKAN;
			yaku[yc][1]=1;
		}
		
		//リーチ
		if (reach[p]) {
			yc++;
			if (dblReach[p]) {
				yk_han+=2;
				yaku[yc][0]=YK_DBLREACH;
				yaku[yc][1]=2;
			} else {
				yk_han++;
				yaku[yc][0]=YK_REACH;
				yaku[yc][1]=1;
			}
			if (ippatsu[p]) {
				yk_han++;
				yc++;
				yaku[yc][0]=YK_IPPATSU;
				yaku[yc][1]=1;
			}
		}
		
		
		//タンヤオチェック @チェック
		if ((rule_kuitan)||(menzen[p])) {
			for (i=1;i<=f;i++) { 
				pp=furopai[p][i][1];
				if (pp<27) {
					pp=(pp%9)%8;
					if (pp>0) {
						switch (furopai[p][i][0]) {
						case FURO_CHI:
							if (pp<6) tanyao+=3;
							break;
						default:
							tanyao+=3;
							break;
						}
					}
				}
			}
			if (tanyao==13) {
				pp=(apai%9)%8;
				if (pp>0) {
					yk_han++;
					yc++;
					yaku[yc][0]=YK_TANYAO;
					yaku[yc][1]=1;
				}
			}
		}
		
		//順子や刻子の役
		if (mt>=0) {
			yk_fu=ms.agariFu(apai,agari);
			
			//ピンフ
			if (ms.pinfu==5) {
				yk_han++;
				yc++;
				yaku[yc][0]=YK_PINFU;
				yaku[yc][1]=1;
			}
			
			//一盃口・二盃口
			k=ms.ipeko(apai);
			switch (k) {
			case 1:
				yk_han++;
				yc++;
				yaku[yc][0]=YK_IPEKO;
				yaku[yc][1]=1;
				break;
			case 2:
				yk_han+=3;
				yc++;
				yaku[yc][0]=YK_RYANPEKO;
				yaku[yc][1]=3;
				break;
			}
			
			//一気通貫
			if (ms.ittsu(apai)) {
				if (menzen[p]) {
					yk_han+=2;
					yc++;
					yaku[yc][0]=YK_ITTSU;
					yaku[yc][1]=2;
				} else {
					yk_han++;
					yc++;
					yaku[yc][0]=YK_ITTSU;
					yaku[yc][1]=1;
				}
			}
			
			//三色同順
			if (ms.dojun(apai)) {
				if (menzen[p]) {
					yk_han+=2;
					yc++;
					yaku[yc][0]=YK_DOJUN;
					yaku[yc][1]=2;
				} else {
					yk_han++;
					yc++;
					yaku[yc][0]=YK_DOJUN;
					yaku[yc][1]=1;
				}
			}
			
			//対々和
			if (ms.isToitoi()) {
				yk_han+=2;
				yc++;
				yaku[yc][0]=YK_TOITOI;
				yaku[yc][1]=2;
			}
			

			//三暗刻
			if (ms.is3Anko(agari)) {
				yk_han+=2;
				yc++;
				yaku[yc][0]=YK_3ANKO;
				yaku[yc][1]=2;
			}
			
			//三槓子
			if (ms.kantsu==3) {
				yk_han+=2;
				yc++;
				yaku[yc][0]=YK_3KANTSU;
				yaku[yc][1]=2;
			}
			
			//三色同刻
			if (ms.isDoko(apai)) {
				yk_han+=2;
				yc++;
				yaku[yc][0]=YK_DOKO;
				yaku[yc][1]=2;
			}
			
			//混老頭
			if ((ms.honro==5)&&(ms.chinro<5)) {
				yk_han+=2;
				yc++;
				yaku[yc][0]=YK_HONRO;
				yaku[yc][1]=2;
			}
			
			//チャンタ
			if (ms.isChanta(apai)) {
				if (menzen[p]) {
					yk_han+=2;
					yc++;
					yaku[yc][0]=YK_CHANTA;
					yaku[yc][1]=2;
				} else {
					yk_han++;
					yc++;
					yaku[yc][0]=YK_CHANTA;
					yaku[yc][1]=1;
				}
			}

			//純チャン
			if (ms.isJunchan(apai)) {
				if (menzen[p]) {
					yk_han+=3;
					yc++;
					yaku[yc][0]=YK_JUNCHAN;
					yaku[yc][1]=3;
				} else {
					yk_han+=2;
					yc++;
					yaku[yc][0]=YK_JUNCHAN;
					yaku[yc][1]=2;
				}
			}
			
		}
		
		//役牌
		k=0;
		if (pais[(p-oya+4)%4+27]==3) k++; //自風
		if (pais[baKaze+27]==3) k++; //場風
		for (i=31;i<34;i++) 
			if (pais[i]==3) k++; //三元牌
		for (i=1;i<=f;i++) {
			pp=furopai[p][i][1];
			if (pp==(p-oya+4)%4+27) k++; //自風
			if (pp==baKaze+27) k++; //場風
			if ((pp>=31)&&(pp<34)) k++; //三元牌
		}
		if ((pais[apai]==2)&&(apai>=27)) {
			if (apai==(p-oya+4)%4+27) k++; //自風（あがり牌)
			if (apai==baKaze+27) k++; //場風（あがり牌）
			if ((apai>=31)&&(apai<34)) k++; //三元牌（あがり牌）
		}
		if (k>0) {
			yk_han+=k;
			yc++;
			yaku[yc][0]=YK_YAKUHAI;
			yaku[yc][1]=k;
		}

		//小三元
		k=0;
		for (i=31;i<34;i++) k+=pais[i];
		if ((31<=apai)&&(apai<34)) k++;
		for (i=1;i<=f;i++) {
			pp=furopai[p][i][1];
			if ((31<=pp)&&(pp<34)) k+=3;
		}
		if (k==8) {
			yk_han+=2;
			yc++;
			yaku[yc][0]=YK_SHOSANGEN;
			yaku[yc][1]=2;
		}
		
		//七対子のテンパイチェック
		if ((f==0)&&(chitoi)) {
			u[1]=u[2]=0;
			for (i=0;i<34;i++)
				u[pais[i]]++;
			if ((u[1]==1)&&(u[2]==6)) {
				yk_fu=25;
				yk_han+=2;
				yc++;
				yaku[yc][0]=YK_CHITOI;
				yaku[yc][1]=2;
			}
		}
		
		//清一色
		for (i=0;i<3;i++) {
			if (supais[i]==n) {
				if (i==(apai/9)) {
					k=0;
					for (j=1;j<=f;j++) {
						if (i==furopai[p][j][1]/9)
							k++;
					}
					if (k==f) {
						yc++;
						if (menzen[p]) {
							yk_han+=6;
							yaku[yc][0]=YK_CHINITSU;
							yaku[yc][1]=6;
						} else {
							yk_han+=5;
							yaku[yc][0]=YK_CHINITSU;
							yaku[yc][1]=5;
						}
						break;
					}
				}
			}
		}
		
		//混一色 @チェック
		if (tupai==0) k=0; else k=1;
		for (i=0;i<3;i++) {
			//if ((supais[i]>0)&&(supais[(i+1)%3]==0)&&(supais[(i+2)%3]==0)) { //ここ修正
			if ((supais[i]>=0)&&(supais[(i+1)%3]==0)&&(supais[(i+2)%3]==0)) {
				t=0;
				for (j=1;j<=f;j++) {
					pp=furopai[p][j][1];
					if (pp<27) {
						if (i!=(pp/9)) {
							t=-1;
							break;
						}
					} else {
						k++;
					}
				}
				if ((t==0)&&(k>0)) {
					yc++;
					if (menzen[p]) {
						yk_han+=3;
						yaku[yc][0]=YK_HONITSU;
						yaku[yc][1]=3;
					} else {
						yk_han+=2;
						yaku[yc][0]=YK_HONITSU;
						yaku[yc][1]=2;
					}
				}
			}
		}
		
		//ドラ
		for (i=0;i<5;i++)
			u[i]=yama[131-i*2];
		k=0;
		t=rinshanIndex+1; if (t>5) t=5;
		for (i=0;i<t;i++) {
			pp=u[i];
			if (pp<27) pp=(pp+1)%9+(pp/9)*9;
			if ((27<=pp)&&(pp<=30)) pp=27+(pp-27+1)%4;
			if (pp>30) pp=31+(pp-31+1)%3;
			k+=pais[pp];
			if (apai==pp)
				k++;
			for (j=1;j<=f;j++) {
				switch (furopai[p][j][0]) {
				case FURO_CHI:
					if (pp<27) {
						if ((pp/9)==(furopai[p][j][1]/9)) {
							if ((furopai[p][j][1]<=pp)
							             &&(pp-furopai[p][j][1]<=2)) {
								k++;
							}
						}
					}
					break;
				case FURO_PON:
					if (pp==furopai[p][j][1])
						k+=3;
					break;
				default:
					if (pp==furopai[p][j][1])
						k+=4;
					break;
				}
			}
		}
		if (reach[p]) { //裏ドラ
			for (i=0;i<5;i++)
				u[i]=yama[130-i*2];
			//k=0;
			t=rinshanIndex+1; if (t>5) t=5;
			for (i=0;i<t;i++) {
				pp=u[i];
				if (pp<27) pp=(pp+1)%9+(pp/9)*9;
				if ((27<=pp)&&(pp<=30)) pp=27+(pp-27+1)%4;
				if (pp>30) pp=31+(pp-31+1)%3;
				k+=pais[pp];
				if (apai==pp)
					k++;
				for (j=1;j<=f;j++) {
					switch (furopai[p][j][0]) {
					case FURO_ANKAN:
						if (pp==furopai[p][j][1])
							k+=4;
						break;
					}
				}
			}
			/*
			if (k>0) {
				yk_han+=k;
				yc++;
				yaku[yc][0]=YK_URADORA;
				yaku[yc][1]=k;
			}
			*/
		}
		if (k>0) {
			yk_han+=k;
			yc++;
			yaku[yc][0]=YK_DORA;
			yaku[yc][1]=k;
		}
		
		yaku[0][0]=yc;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@   内部クラス   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	// -------------------------------------------------------------------- // class MentsSet
	//テンパイしてるセット
	class MentsSet {
		public Ments[]  ments=new Ments[14];
		public int      count=0;
		
		MentsSet() {
		}
		
		private void add(Ments m) {
			ments[count]=m;
			count++;
		}

		// -------------------------------------------------------------------- // class MentsSet		
		
		public void check(int t, int os, int n, int[] pais, Ments bms) {
			if (n==0) {
				add(bms);
				return;
			}
			Ments ms;
			int i,p,md,tp;
			md=n%3;
			for (i=t;i<9;i++) {
				p=i+os;
				if (pais[p]>0) {
					switch (pais[p]) {
					case 1: //牌１個
						if (md==1) { //単騎
							ms=new Ments(bms);
							if (ms.add(MT_TANKI,p))
								check(i+1,os,n-1,pais,ms);
							ms=null;
						}
						if ((md!=0)&&(n>1)&&(i<8)) { //塔子
							if (pais[p+1]>0) {
								pais[p+1]--;
								ms=new Ments(bms);
								if ((i==0)||(i==7))
									tp=MT_PTATSU;
								else 
									tp=MT_RTATSU;
								if (ms.add(tp,p))
									check(i+1,os,n-2,pais,ms);
								ms=null; pais[p+1]++;
							}
							if (i<7) {
								if (pais[p+2]>0) {
									pais[p+2]--;
									ms=new Ments(bms);
									if (ms.add(MT_KTATSU,p))
										check(i+1,os,n-2,pais,ms);
									ms=null; pais[p+2]++;
								}
							}
						}
						if ((n>2)&&(i<7)) { //順子
							if ((pais[p+1]>0)&&(pais[p+2]>0)) {
								pais[p+1]--; pais[p+2]--;
								ms=new Ments(bms);
								if (ms.add(MT_JUNTSU,p))
									check(i+1,os,n-3,pais,ms);
								ms=null; pais[p+1]++; pais[p+2]++;
							}
						}
						break;
					case 2: //牌２個
						if (md!=0) { //対子
							ms=new Ments(bms);
							if (ms.add(MT_TOITSU,p))
								check(i+1,os,n-2,pais,ms);
							ms=null;
						}
						if ((md==1)&&(n>3)&&(i<7)) { //単騎、順子
							if ((pais[p+1]>0)&&(pais[p+2]>0)) {
								pais[p+1]--; pais[p+2]--;
								ms=new Ments(bms);
								if (ms.add(MT_TANKI,p))
									if (ms.add(MT_JUNTSU,p))
										check(i+1,os,n-4,pais,ms);
								ms=null;
								pais[p+1]++; pais[p+2]++;
							}
						}
						if ((md!=0)&&(n>4)&&(i<7)) { //塔子、順子
							if ((pais[p+1]>1)&&(pais[p+2]>0)) { //両面･辺張塔子
								pais[p+1]-=2; pais[p+2]--;
								ms=new Ments(bms);
								if (i==0)
									tp=MT_PTATSU;
								else
									tp=MT_RTATSU;
								if (ms.add(tp,p))
									if (ms.add(MT_JUNTSU,p))
										check(i+1,os,n-5,pais,ms);
								ms=null;
								pais[p+1]+=2; pais[p+2]++;
							}
							if ((pais[p+1]>0)&&(pais[p+2]>1)) { //嵌張塔子
								pais[p+1]--; pais[p+2]-=2;
								ms=new Ments(bms);
								if (ms.add(MT_KTATSU,p))
									if (ms.add(MT_JUNTSU,p))
										check(i+1,os,n-5,pais,ms);
								ms=null;
								pais[p+1]++; pais[p+2]+=2;
							}
						}
						if (i<7) { //順子２つ
							if ((pais[p+1]>1)&&(pais[p+2]>1)) { 
								pais[p+1]-=2; pais[p+2]-=2;
								ms=new Ments(bms);
								if (ms.add(MT_JUNTSU,p))
									if (ms.add(MT_JUNTSU,p))
										check(i+1,os,n-6,pais,ms);
								ms=null;
								pais[p+1]+=2; pais[p+2]+=2;
							}
						}
						break;
					case 3: //牌３個
						//刻子
						ms=new Ments(bms);
						if (ms.add(MT_ANKO,p))
							check(i+1,os,n-3,pais,ms);
						ms=null;
						if ((md!=0)&&(n>4)&&(i<7)) { //対子、順子
							if ((pais[p+1]>0)&&(pais[p+2]>0)) {
								pais[p+1]--; pais[p+2]--;
								ms=new Ments(bms);
								if (ms.add(MT_TOITSU,p))
									if (ms.add(MT_JUNTSU,p))
										check(i+1,os,n-5,pais,ms);
								ms=null;
								pais[p+1]++; pais[p+2]++;
							}
						}
						if ((md==1)&&(n>3)&&(i<8)) { //対子、塔子
							if (pais[p+1]>0) { //両面･辺張塔子
								pais[p+1]--;
								ms=new Ments(bms);
								if ((i==0)||(i==7))
									tp=MT_PTATSU;
								else 
									tp=MT_RTATSU;
								if (ms.add(tp,p))
									if (ms.add(MT_TOITSU,p))
										check(i+1,os,n-4,pais,ms);
								ms=null; pais[p+1]++;
							}
							if (i<7) { //嵌張塔子
								if (pais[p+2]>0) {
									pais[p+2]--;
									ms=new Ments(bms);
									if (ms.add(MT_KTATSU,p))
										if (ms.add(MT_TOITSU,p))
											check(i+1,os,n-4,pais,ms);
									ms=null; pais[p+2]++;
								}
							}
						}
						if ((md==1)&&(n>6)&&(i<7)) { //単騎、順子２つ
							if ((pais[p+1]>1)&&(pais[p+2]>1)) {
								pais[p+1]-=2; pais[p+2]-=2;
								ms=new Ments(bms);
								if (ms.add(MT_TANKI,p))
									if (ms.add(MT_JUNTSU,p))
										if (ms.add(MT_JUNTSU,p))
											check(i+1,os,n-7,pais,ms);
								ms=null;
								pais[p+1]+=2; pais[p+2]+=2;
							}
						}
						//順子３つ <- たぶんいらない
						break;
					case 4: //牌４個
						if ((n>5)&&(i<7)) { //刻子、順子
							if ((pais[p+1]>0)&&(pais[p+2]>0)) {
								pais[p+1]--; pais[p+2]--;
								ms=new Ments(bms);
								if (ms.add(MT_ANKO,p))
									if (ms.add(MT_JUNTSU,p))
										check(i+1,os,n-6,pais,ms);
								ms=null;
								pais[p+1]++; pais[p+2]++;
							}
						}
						if ((md!=0)&&(n>4)&&(i<8)) { //刻子、塔子
							if (pais[p+1]>0) { //両面･辺張塔子
								pais[p+1]--;
								ms=new Ments(bms);
								if ((i==0)||(i==7))
									tp=MT_PTATSU;
								else 
									tp=MT_RTATSU;
								if (ms.add(tp,p))
									if (ms.add(MT_ANKO,p))
										check(i+1,os,n-5,pais,ms);
								ms=null; pais[p+1]++;
							}
							if (i<7) { //嵌張塔子
								if (pais[p+2]>0) {
									pais[p+2]--;
									ms=new Ments(bms);
									if (ms.add(MT_KTATSU,p))
										if (ms.add(MT_ANKO,p))
											check(i+1,os,n-5,pais,ms);
									ms=null; pais[p+2]++;
								}
							}
						}
						if ((md!=0)&&(n>7)&&(i<7)) { //対子、順子２つ
							if ((pais[p+1]>1)&&(pais[p+2]>1)) {
								pais[p+1]-=2; pais[p+2]-=2;
								ms=new Ments(bms);
								if (ms.add(MT_TOITSU,p))
									if (ms.add(MT_JUNTSU,p))
										if (ms.add(MT_JUNTSU,p))
											check(i+1,os,n-8,pais,ms);
								ms=null;
								pais[p+1]+=2; pais[p+2]+=2;
							}
						}
						if ((md==1)&&(n>6)&&(i<7)) { //対子、順子、塔子
							if ((pais[p+1]>1)&&(pais[p+2]>0)) { //両面･辺張塔子
								pais[p+1]-=2; pais[p+2]--;
								ms=new Ments(bms);
								if ((i==0)||(i==7))
									tp=MT_PTATSU;
								else 
									tp=MT_RTATSU;
								if (ms.add(tp,p))
									if (ms.add(MT_TOITSU,p))
										if (ms.add(MT_JUNTSU,p))
											check(i+1,os,n-7,pais,ms);
								ms=null; pais[p+1]+=2; pais[p+2]++;
							}
							if (i<7) { //嵌張塔子
								if ((pais[p+1]>0)&&(pais[p+2]>1)) {
									pais[p+1]--; pais[p+2]-=2;
									ms=new Ments(bms);
									if (ms.add(MT_KTATSU,p))
										if (ms.add(MT_TOITSU,p))
											if (ms.add(MT_JUNTSU,p))
												check(i+1,os,n-7,pais,ms);
									ms=null; pais[p+1]++; pais[p+2]+=2;
								}
							}
						}
						//順子４つ <- たぶんいらない
						break;
					}
					return;
				}
			}
		}
	
	}	// -------------------------------------------------------------------- // class MentsSet

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//テンパイの型判定用
	class Ments {
		public  int       count=0;
		public  int[][]   menz=new int[5][2];
		
		public  int       fu=0; //符 
		                        //ただし、双ポン待ちの刻子の符、雀頭の符
		                        //        あがりの符 ピンフの符は計算しない
		
		public  int       tanki=0; //単騎の数
		public  int       toitsu=0; //対子の数
		public  int       tatsu=0; //塔子の数
		public  int[]     machi=new int[2]; //待ち牌
		
		
		public  int       kantsu=0; //槓子の数（三槓子、四槓子用）
		public  int       anko=0;   //暗刻子＋暗槓子の数（三暗刻、四暗刻用）
		public  int       toitoi=0; //刻子＋槓子の数 4で対々和成立（対々和用）
		public  int[]     doko=new int[9]; //各数字の刻子の数 (三色同刻用）
		
		public  int[][]   juntsu=new int[3][7]; //各数牌ごとの順子の構成
		
		public  int[]     chinitsu=new int[3];  //5になってるのが清一色
		public  int[]     honitsu=new int[3];   //5になってるのが混一色
		public  int       chanta=0;             //5になったらチャンタ
		public  int       junchan=0;            //5になったら純チャン
		public  int       honro=0;              //5になったら混老頭
		public  int       chinro=0;             //5になったら清老頭
		public  int       pinfu=0;              //値が5になったらピンフ
		public  int       tanyao=0;             //5になったらタンヤオ
		public  int       yakuhai=0;            //1以上で役牌（自風、場風、三元牌）
		public  boolean   menzen=true;
		public  boolean   inJuntsu=false;
		public  int       player=0; //プレイヤーの風
		
		public  boolean[] c_anko=new boolean[34];
		
		// -------------------------------------------------------------------- // class Ments
		
		Ments(int p) {
			player=p; //プレイヤーの風
			machi[0]=machi[1]=-1;
			for (int i=0;i<34;i++)
				c_anko[i]=false;
		}
		Ments(Ments m) {
			int i;
			for (i=0;i<34;i++)
				c_anko[i]=false;
			machi[0]=machi[1]=-1;
			player=m.player;
			for (i=0;i<m.count;i++) {
				add(m.menz[i][0], m.menz[i][1]);
			}
		}

		// -------------------------------------------------------------------- // class Ments

		public int agariFu(int pai, int agari) {
			if ((pinfu==5)&&(agari==AG_TUMO)) return 20; //ツモピンフ２０符固定
			int i,pp;
			if ((this.menzen==false)&&(tatsu==1)&&(agari==AG_RON)) { //喰いピンフ型３０符固定
				int c=0;
				for (i=0;i<5;i++) {
					if (menz[i][0]==MT_TOITSU) { //雀頭が役牌でないこと
						pp=menz[i][1];
						if ((pp==player+27)||(pp==baKaze+27)||(pp>30))
							break;
						else
							c++;
					}
					if (menz[i][0]==FURO_CHI)
						c++;
					if (menz[i][0]==MT_JUNTSU)
						c++;
					if (menz[i][0]==MT_RTATSU)
						c++;
				}
				if (c==5)
					return 30;
			}
			int f=fu+20;
			if ((this.menzen)&&(agari==AG_RON)) f+=10; //門前ロン
			if (agari==AG_TUMO) f+=2; //ツモ符
			if (toitsu>1) {
				for (i=0;i<5;i++) 
					if (menz[i][0]==MT_TOITSU) { //雀頭
						pp=menz[i][1];
						if (pp!=pai) {
							if (pp==player+27) f+=2;
							if (pp==baKaze+27) f+=2;
							if ((31<=pp)&&(pp<34)) f+=2;
						}
					}
				if (pai<27) {
					if ((pai%9)%8>0) {
						if (agari==AG_TUMO) f+=4;
						else f+=2;
					} else {
						if (agari==AG_TUMO) f+=8;
						else f+=4;
					}
				} else {
					if (agari==AG_TUMO) f+=8;
					else f+=4;
				}
			} else if (tanki>0) { //単騎待ち
				if (pai==player+27) f+=2;
				if (pai==baKaze+27) f+=2;
				if ((31<=pai)&&(pai<34)) f+=2;
			} else { //塔子待ち
				//雀頭の符計算
				for (i=0;i<5;i++)
					if (menz[i][0]==MT_TOITSU) {
						pp=menz[i][1];
						if (pp==player+27) f+=2;
						if (pp==baKaze+27) f+=2;
						if ((31<=pp)&&(pp<34)) f+=2;
					}
			}
			return ((f+9)/10)*10;
		}
		
		// -------------------------------------------------------------------- // class Ments
		
		//あがり牌により塔子から出来る順子を返す
		private int machiJuntsu(int pai) {
			int pp=-1;
			switch (tatsu) {
			case 1:
				if (machi[1]==pai) pp=pai-2; else pp=pai;
				break;
			case 2:
				pp=pai-1;
				break;
			case 3:
				if (pai%9==2) pp=pai-2; else pp=pai;
				break;
			}
			return pp;
		}
		
		// -------------------------------------------------------------------- // class Ments
		
		//一盃口なら1、二盃口なら2を返す
		public int ipeko() { 
			int i,j,c=0;
			if (this.menzen) {
				for (i=0;i<3;i++) {
					for (j=0;j<7;j++) {
						if (juntsu[i][j]>1)
							c++;
					}
				}
			}
			return c;
		}
		public int ipeko(int pai) {
			if ((pai>26)||(pai<0)) return ipeko();
			if ((toitsu>1)||(tanki>0)) return ipeko();
			int pp=machiJuntsu(pai);
			int pt=pp/9;
			int pn=pp%9;
			juntsu[pt][pn]++;
			int c=ipeko();
			juntsu[pt][pn]--;
			return c;
		}
		
		// -------------------------------------------------------------------- // class Ments

		public boolean ittsu() {
			int i;
			for (i=0;i<3;i++) {
				if ((juntsu[i][0]>0)&&(juntsu[i][3]>0)&&(juntsu[i][6]>0))
					return true;
			}
			return false;
		}
		public boolean ittsu(int pai) {
			if ((pai>26)||(pai<0)) return ittsu();
			if ((toitsu>1)||(tanki>0)) return ittsu();
			int pp=machiJuntsu(pai);
			int pt=pp/9;
			int pn=pp%9;
			juntsu[pt][pn]++;
			boolean b=ittsu();
			juntsu[pt][pn]--;
			return b;
		}

		// -------------------------------------------------------------------- // class Ments
		
		public boolean dojun() {
			int i;
			for (i=0;i<7;i++) {
				if ((juntsu[0][i]>0)&&(juntsu[1][i]>0)&&(juntsu[2][i]>0))
					return true;
			}
			return false;
		}
		public boolean dojun(int pai) {
			if ((pai>26)||(pai<0)) return dojun();
			if ((toitsu>1)||(tanki>0)) return dojun();
			int pp=machiJuntsu(pai);
			int pt=pp/9;
			int pn=pp%9;
			juntsu[pt][pn]++;
			boolean b=dojun();
			juntsu[pt][pn]--;
			return b;
		}

		// -------------------------------------------------------------------- // class Ments
		
		public boolean isChanta(int pai) {
			if (isJunchan(pai)) return false;
			if ((tatsu==1)&&(chanta==4)) {
				int pp=machiJuntsu(pai)%9;
				if (((pai==machi[0])||(pai==machi[1]))&&(pai>=0))
					return ((pp==0)||(pp==6));
			}
			return ((chanta==5)&&(inJuntsu));
		}
		public boolean isJunchan(int pai) {
			if ((tatsu==1)&&(junchan==4)) {
				int pp=machiJuntsu(pai)%9;
				if (((pai==machi[0])||(pai==machi[1]))&&(pai>=0))
					return ((pp==0)||(pp==6));
			}
			return ((junchan==5)&&(inJuntsu));
		}
		public boolean is4Anko(int agari) {
			if ((toitsu>1)&&(anko==3)) {
				return (agari==AG_TUMO);
			}
			return (anko==4);
		}
		public boolean is3Anko(int agari) {
			if ((toitsu>1)&&(anko==2)) {
				return (agari==AG_TUMO);
			}
			return (anko==3);
		}
		public boolean isToitoi() {
			if ((toitsu>1)&&(toitoi==3)) return true;
			return (toitoi==4);
		}
		public boolean isTanyao(int pai) {
			boolean b;
			if (rule_kuitan) b=true; else b=this.menzen;
			if (b) {
				if ((tatsu==1)&&(tanyao==4)) {
					int pp=machiJuntsu(pai)%9;
					if (((pai==machi[0])||(pai==machi[1]))&&(pai>=0))
						return ((pp==1)||(pp==5));
				}
				return (tanyao==5);
			}
			return false;
		}
		public int isYakuhai(int pai) {
			if (toitsu>1) {
				if (((pai==machi[0])||(pai==machi[1]))&&(pai>=0))
					if ((pai==player+27)||(pai==baKaze+27)||((pai>=31)&&(pai<34)))
						return yakuhai+1;
			}
			return yakuhai;
		}
		public boolean isDoko(int pai) {
			int i;
			for (i=0;i<9;i++) {
				if ((toitsu>1)&&(doko[i]==2)&&(pai<27)){
					if (((machi[0]==pai)||(machi[1]==pai))&&(pai>=0))
						if (pai%9==i) return true;
				}
				if (doko[i]==3) return true;
			}
			return false;
		}
		
		// -------------------------------------------------------------------- // class Ments

		public int hancount(int pai, int agari) {
			int i, c=0;
			//boolean it,ip,dj;

			if (count<5) return -1;
			
			c+=isYakuhai(pai);
			for (i=0;i<3;i++) {
				if (chinitsu[i]==5) {
					c+=5; if (this.menzen) c++; break;
				} else if (honitsu[i]==5) {
					c+=2; if (this.menzen) c++; break;
				}
			}
			if (isJunchan(pai)) {
				c+=2; if (this.menzen) c++;
			} else if (isChanta(pai))   {
				c++; if (this.menzen) c++;
			}
			if (chinro==5)   c+=13; //清老頭
			else if (honro==5)    c+=2;  //混老頭
			if (pinfu==5)    c++;   //ピンフ
			if (isTanyao(pai))   c++;   //タンヤオ
			if (is3Anko(agari)) c+=2;  //三暗刻
			if (is4Anko(agari)) c+=13; //四暗刻
			if (kantsu==3)   c+=2;  //三槓子
			if (kantsu==4)   c+=13; //四槓子
			if (isToitoi())   c+=2;  //対々和
			if (isDoko(pai)) c+=2; //三色同刻
			if (ittsu(pai)) {
				c++; if (this.menzen) c++;  //一気通貫
			}
			if (dojun(pai)) {
				c++; if (this.menzen) c++;  //三色同順
			}
			int k=ipeko(pai); //一盃口・二盃口
			switch (k) {
			case 1: c++; break;
			case 2: c+=3; break;
			}
			//ip=(k>0);
			
			//テンパイと形式テンパイの２通りの可能性がある待ち
			//if (tanki>0) {}
			//if (tatsu>0) {}
			//if (toitsu>1) {}
			
			//緑一色、九蓮宝燈、大四喜、小四喜、大三元、字一色はここでカウントしない
			return c;
		}
		
		// -------------------------------------------------------------------- // class Ments
		
		public int tenpai() { //テンパイの種類を返す
			int i,k;
			
			if (count<5) return -1;
			
			if (yakuhai>0)   return 1;
			k=0;
			if (isYakuhai(machi[0])>0) k=1;
			if (isYakuhai(machi[1])>0) k+=2;
			switch (k) {
			case 1: return 2;
			case 2: return 3;
			case 3: return 4;
			}

			for (i=0;i<3;i++) {
				if (chinitsu[i]==5) return 1;
				if (honitsu[i]==5)  return 1;
			}
			if ((junchan==5)&&(inJuntsu))  return 1;
			k=0;
			if (isJunchan(machi[0])) k=1;
			if (isJunchan(machi[1])) k+=2;
			switch (k) {
			case 1: return 2;
			case 2: return 3;
			case 3: return 4;
			}

			if ((chanta==5)&&(inJuntsu))  return 1;
			k=0;
			if (isChanta(machi[0])) k=1;
			if (isChanta(machi[1])) k+=2;
			switch (k) {
			case 1: return 2;
			case 2: return 3;
			case 3: return 4;
			}

			if (chinro==5)   return 1;
			else if (honro==5)    return 1;
			if (pinfu==5)    return 1;
			boolean b;
			if (rule_kuitan) b=true; else b=this.menzen;
			if (b) {
				if (tanyao==5)  return 1;
				k=0;
				if (isTanyao(machi[0]))  k=1;
				if (isTanyao(machi[1]))  k+=2;
				switch (k) {
				case 1: return 2;
				case 2: return 3;
				case 3: return 4;
				}
			}
			if (anko>2)      return 1;
			if (kantsu>2)    return 1;
			if (isToitoi())   return 1;
			if (isDoko(100))  return 1;
			for (i=0;i<2;i++)
				if (isDoko(machi[i])) return 2+i;
			if (ittsu())     return 1;
			if (dojun())     return 1;
			if (ipeko()>0)   return 1;
			for (i=0;i<2;i++) {
				if (ittsu(machi[i]))  return 2+i;
				if (dojun(machi[i]))  return 2+i;
			}
			k=0;
			if (ipeko(machi[0])>0) k=1;
			if (ipeko(machi[1])>0) k+=2;
			switch (k) {
			case 1: return 2;
			case 2: return 3;
			case 3: return 4;
			}
			
			//テンパイと形式テンパイの２通りの可能性がある待ち
			//if (tanki>0) {}
			//if (tatsu>0) {}
			//if (toitsu>1) {}
			
			//ツモ和時のみ三暗刻と四暗刻の成立は対々和でカバー
			//緑一色は清一色or混一色、九蓮宝燈は清一色で変わりにテンパイ判定できる
			//大四喜と小四喜は混一色でカバー
			//大三元と字一色は役牌判定でＯＫ
			return 0;
		}
		
		// -------------------------------------------------------------------- // class Ments
		
		public boolean add(int type, int pai) {
			if (count==5) return false;
			
			int pt=pai/9; //数牌なら種類(0,1,2) 字牌(3)は意味なし
			int pn=pai%9; //数牌なら数字(0-8)   字牌は意味なし
			
			switch (type) {
			case FURO_CHI:
				juntsu[pt][pn]++;
				if ((pn==0)||(pn==6)) {
					chanta++; junchan++;
					inJuntsu=true; tanyao=-100;
				} else {
					chanta=-100; junchan=-100;
					tanyao++;
				}
				pinfu=-100;
				this.menzen=false;
				break;
			case FURO_PON:
				if (pt<3) {
					doko[pn]++;
					if ((0<pn)&&(pn<8)) fu+=2;
					else fu+=4;
					if ((pn==0)||(pn==8)) {
						chinro++; honro++;
						chanta++; junchan++;
						tanyao=-100;
					} else {
						chinro=-100; honro=-100;
						chanta=-100; junchan=-100;
						tanyao++;
					}
				} else {
					fu+=4; tanyao=-100;
					if ((baKaze+27==pai)||(player+27==pai)||(pai>30))
						yakuhai++;
				}
				toitoi++; pinfu=-100;
				this.menzen=false;
				break;
			case FURO_MINKAN:
				if (pt<3) {
					doko[pn]++;
					if ((0<pn)&&(pn<8)) fu+=8;
					else fu+=16;
					if ((pn==0)||(pn==8)) {
						chinro++; honro++;
						chanta++; junchan++;
						tanyao=-100;
					} else {
						chinro=-100; honro=-100;
						chanta=-100; junchan=-100;
						tanyao++;
					}
				} else {
					fu+=16; tanyao=-100;
					if ((baKaze+27==pai)||(player+27==pai)||(pai>30))
						yakuhai++;
				}
				toitoi++; kantsu++; pinfu=-100;
				this.menzen=false;
				break;
			case FURO_ANKAN:
				if (pt<3) {
					doko[pn]++;
					if ((0<pn)&&(pn<8)) fu+=16;
					else fu+=32;
					if ((pn==0)||(pn==8)) {
						chinro++; honro++;
						chanta++; junchan++;
						tanyao=-100;
					} else {
						chinro=-100; honro=-100;
						chanta=-100; junchan=-100;
						tanyao++;
					}
				} else {
					fu+=32; tanyao=-100;
					if ((baKaze+27==pai)||(player+27==pai)||(pai>30))
						yakuhai++;
				}
				toitoi++; kantsu++; anko++; pinfu=-100;
				break;
			case MT_TANKI:
				if ((tanki>0)||(toitsu>0)||(tatsu>0))
					return false;
				if (pt<3) {
					if ((pn==0)||(pn==8)) {
						chinro++; honro++;
						chanta++; junchan++;
						tanyao=-100;
					} else {
						chinro=-100; honro=-100;
						chanta=-100; junchan=-100;
						tanyao++;
					}
				} else {
					tanyao=-100;
				}
				tanki++; fu+=2; machi[0]=pai; pinfu=-100;
				break;
			case MT_TOITSU:
				if ((tanki>0)||(toitsu>1)||((tatsu>0)&&(toitsu>0)))
					return false;
				if (pt<3) {
					if ((pn==0)||(pn==8)) {
						chinro++; honro++;
						chanta++; junchan++;
						tanyao=-100;
					} else {
						chinro=-100; honro=-100;
						chanta=-100; junchan=-100;
						tanyao++;
					}
				} else {
					tanyao=-100;
				}
				toitsu++;
				if (toitsu==1) {
					if ((pai!=(baKaze+27))&&(pai!=(player+27))&&(pai<31))
						pinfu++;
					else
						pinfu=-100;
					if (tatsu==0)
						machi[0]=pai;
				} else {
					machi[1]=pai;
				}
				break;
			case MT_RTATSU:
				if ((tanki>0)||(toitsu>1)||(tatsu>0))
					return false;
				if ((pt<3)&&(pn>1)&&(pn<6)) {
					chinro=-100; honro=-100;
					chanta=-100; junchan=-100;
					tanyao++;
				}
				tatsu=1; pinfu++;
				machi[0]=pai-1; machi[1]=pai+2;
				break;
			case MT_KTATSU:
				if ((tanki>0)||(toitsu>1)||(tatsu>0))
					return false;
				if ((pn==0)||(pn==6)) {
					chanta++; junchan++; inJuntsu=true;
					tanyao=-100;
				} else {
					chinro=-100; honro=-100;
					chanta=-100; junchan=-100;
					tanyao++;
				}
				tatsu=2; fu+=2; machi[0]=pai+1; pinfu=-100;
				break;
			case MT_PTATSU:
				if ((tanki>0)||(toitsu>1)||(tatsu>0))
					return false;
				chanta++; junchan++; inJuntsu=true;
				tanyao=-100; pinfu=-100;
				tatsu=3; fu+=2;
				if (pn==0) machi[0]=pai+2;
				else machi[0]=pai-1;
				break;
			case MT_JUNTSU:
				juntsu[pt][pn]++;
				if ((pn==0)||(pn==6)) {
					chanta++; junchan++; inJuntsu=true;
					tanyao=-100;
				} else {
					chinro=-100; honro=-100;
					chanta=-100; junchan=-100;
					tanyao++;
				}
				pinfu++;
				break;
			case MT_ANKO:
				if (pt<3) {
					doko[pn]++;
					if ((0<pn)&&(pn<8)) fu+=4;
					else fu+=8;
					if ((pn==0)||(pn==8)) {
						chinro++; honro++;
						chanta++; junchan++;
						tanyao=-100;
					} else {
						chinro=-100; honro=-100;
						chanta=-100; junchan=-100;
						tanyao++;
					}
				} else {
					fu+=8; tanyao=-100;
					if ((baKaze+27==pai)||(player+27==pai)||(pai>30))
						yakuhai++;
				}
				this.c_anko[pai]=true;
				toitoi++; anko++; pinfu=-100;
				break;
			}
			
			if (pt<3) {
				chinitsu[pt]++;
				honitsu[pt]++;
			} else {
				int i;
				for (i=0;i<3;i++) {
					honitsu[i]++;
				}
				honro++; chanta++;
			}
			
			menz[count][0]=type;
			menz[count][1]=pai;
			count++;
			
			return true;
		}
		
	}	// -------------------------------------------------------------------- // class Ments

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@  簡単処理  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//牌山のシャッフル
	private void shuffle() {
		int i,n,p;
		for (i=0;i<2000;i++) {
			n=rand(136);
			p=yama[i%136];
			yama[i%136]=yama[n];
			yama[n]=p;
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//配牌
	private void haipai() {
		int i,j,k,c,n;
		c=0;
		for (i=0;i<3;i++) {
			for (j=0;j<4;j++) {
				n=(j+kicha)%4;
				for (k=0;k<4;k++) {
					tehai[n][i*4+k]=yama[c];
					c++;
				}
			}
		}
		for (j=0;j<4;j++) {
			n=(j+kicha)%4;
			tehai[n][12]=yama[c];
			c++;
			tehai[n][13]=13;
		}
		tumoIndex=c;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//理牌（手牌の整頓）
	private void rihai(int p) {
		int i,j,t,n;
		n=tehai[p][13]-1;
		for (i=0;i<13;i++) {
			for (j=0;j<n;j++) {
				if (tehai[p][j]>tehai[p][j+1]) {
					t=tehai[p][j];
					tehai[p][j]=tehai[p][j+1];
					tehai[p][j+1]=t;
				}
			}
		}
	}
	
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@  描写処理  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void drawTitle() {
		int i,n,x,y;
		x=0;
		for (i=0;i<34;i++) {
			n=0;
			if (i<27) n=(i%9)%8;
			if (n==0) {
				y=120;
				if ((titleCount/5)==x) y-=3;
				drawPai(i, 42+x*PAI_WIDTH, y);
				x++;
			}
		}
		
		g.setColor(255,255,255);
		g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_SMALL));
		//drawNukiMoji("シンプル",120,62,255,0,0,GPC_HT);
		g.drawString("シンプル",120,62,GPC_HT);
		g.setColor(255,255,255);
		g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_LARGE));
		drawNukiMoji("麻雀",120,75,255,0,0,GPC_HT);
		//g.drawString("麻雀",120,75,GPC_HT);
		g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_SMALL));
		g.setColor(128,128,128);
		g.drawString(appver,120,100,GPC_HT);
		
		g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_MEDIUM));
		if (titleCount%2==0)
			g.setColor(255,255,0);
		else 
			g.setColor(255,255,255);
		g.drawString("Press Any Key",120,170,GPC_HT);
		
		//Test
		g.setColor(255,255,255);
		g.drawString("size:"+Integer.toString(rs_size)+"bytes",0,0,GPC_LT);
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void drawMenu() {
		int i,n,x;
		x=0;
		for (i=0;i<34;i++) {
			n=0;
			if (i<27) n=(i%9)%8;
			if (n==0) {
				drawPai(i, 42+x*PAI_WIDTH, 120);
				x++;
			}
		}
		
		g.setColor(255,255,255);
		g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_SMALL));
		//drawNukiMoji("シンプル",120,62,255,0,0,GPC_HT);
		g.drawString("シンプル",120,62,GPC_HT);
		g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_LARGE));
		g.setColor(255,255,255);
		drawNukiMoji("麻雀",120,75,255,0,0,GPC_HT);
		g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_SMALL));
		g.setColor(128,128,128);
		g.drawString(appver,120,100,GPC_HT);
		
		g.setColor(255,255,255);
		g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_MEDIUM));
		g.drawString("対局開始",120,160,GPC_HT);
		g.drawString("データ",120,190,GPC_HT);
		g.drawString("設定",120,220,GPC_HT);
		
		g.setColor(255,255,0);
		g.drawRect(85,158+selectMenu*30,70,20);
		
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void drawSeiseki() {
		int i,n,x;
		x=0;
		for (i=0;i<34;i++) {
			n=0;
			if (i<27) n=(i%9)%8;
			if (n==0) {
				drawPai(i, 42+x*PAI_WIDTH, 120);
				x++;
			}
		}
		
		g.setColor(255,255,255);
		g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_SMALL));
		//drawNukiMoji("シンプル",120,62,255,0,0,GPC_HT);
		g.drawString("シンプル",120,62,GPC_HT);
		g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_LARGE));
		g.setColor(255,255,255);
		drawNukiMoji("麻雀",120,75,255,0,0,GPC_HT);
		g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_SMALL));
		g.setColor(128,128,128);
		g.drawString(appver,120,100,GPC_HT);
		
		g.setColor(255,255,255);
		g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_MEDIUM));
		g.drawString("戦績",120,160,GPC_HT);
		g.drawString("統計",120,190,GPC_HT);
		g.drawString("戦歴",120,220,GPC_HT);
		//g.drawString("設定",120,220,GPC_HT);
		
		g.setColor(255,255,0);
		g.drawRect(85,158+selectSeiseki*30,70,20);

	}
	
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

/*
	private String percent(int n) {
		String s;
        if ((n>0)&&(n<10)) {
	        s="00"+Integer.toString(n%100);
	        s=Integer.toString(n/100)+"."+s.substring(s.length()-2,s.length())+"%";
        } else {
        	int m=n/10;
        	s=Integer.toString(m/10)+"."+Integer.toString(m%10)+"%";
        }
	    return s;
	}
*/
	private String percent(int n1, int n2) {
		long n=0L;
		String s;

		if (n2>0) n=(((long)n1)*10000L)/((long)n2);
        if ((n>0L)&&(n<10L)) {
	        s="00"+Long.toString(n%100L);
	        s=Long.toString(n/100L)+"."+s.substring(s.length()-2,s.length())+"%";
        } else {
        	n+=5L; //四捨五入
        	n/=10L;
        	s=Long.toString(n/10L)+"."+Long.toString(n%10L)+"%";
        }
	    return s;
	}

	private void drawRc_yaku() {
		int i,j,k,p,n;
		int x0=35;
		int x1=205;
		
		g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_MEDIUM));
		g.setColor(255,255,255);
		
		p=rc_yk_player;
		
		String s;
		switch (rc_yk_type) {
		case 0:  s="統計データ";         break;
		case 1:  s="役の出現回数";       break;
		case 2:  s="役の平均翻数";     break;
		case 3:  s="役の出現割合";       break;
		case 4:  s="役満の出現回数";     break;
		case 5:  s="役満の出現割合";     break;
		case 6:  s="翻数の出現回数";     break;
		case 7:  s="翻数の出現割合";     break;
		case 8:  s="符数の出現回数";     break;
		case 9:  s="符数の出現割合";     break;
		default: s=""; break;
		}
		
		g.drawString(PlayerName(p)+"の"+s,120,5,GPC_HT);
		
		if (p>-1)
			g.drawString("＜",5,126,GPC_LT);
		if (p<4)
			g.drawString("＞",235,126,GPC_RT);
		
		StcData tmp;
		
		if (p<0) {
			tmp=new StcData();
			switch (rc_yk_type) {
			case 1: //通常役
			case 2:
			case 3:
				//if (rc_yk_type==3) n=0; else n=rc_yk_type-1;
				for (i=0;i<stcdata.length;i++) {
					for (n=0;n<2;n++) {
						for (j=0;j<28;j++) {
							tmp.Yaku[j][n]+=stcdata[i].Yaku[j][n];
						}
					}
					tmp.TumoCount+=stcdata[i].TumoCount;
					tmp.RonCount+=stcdata[i].RonCount;
				}
				break;
			case 4:
			case 5:
				for (i=0;i<stcdata.length;i++) {
					for (j=0;j<15;j++) {
						tmp.Yakuman[j]+=stcdata[i].Yakuman[j];
					}
					for (j=0;j<5;j++) {
						tmp.Yakumans[j]+=stcdata[i].Yakumans[j];
					}
					tmp.TumoCount+=stcdata[i].TumoCount;
					tmp.RonCount+=stcdata[i].RonCount;
				}
				break;
			case 6:
			case 7:
				for (i=0;i<stcdata.length;i++) {
					tmp.TotalHan+=stcdata[i].TotalHan;
					if (stcdata[i].MaxHan>tmp.MaxHan)
						tmp.MaxHan=stcdata[i].MaxHan;
					for (j=0;j<13;j++) {
						tmp.HanCount[j]+=stcdata[i].HanCount[j];
					}
				}
				break;
			case 8:
			case 9:
				for (i=0;i<stcdata.length;i++) {
					tmp.TotalFu+=stcdata[i].TotalFu;
					if (stcdata[i].MaxFu>tmp.MaxFu)
						tmp.MaxFu=stcdata[i].MaxFu;
					for (j=0;j<11;j++) {
						tmp.FuCount[j]+=stcdata[i].FuCount[j];
					}
				}
				break;
			case 0:
				for (i=0;i<stcdata.length;i++) {
					tmp.Yaku[YK_REACH-1][0]+=stcdata[i].Yaku[YK_REACH-1][0];
					tmp.Yaku[YK_DBLREACH-1][0]+=stcdata[i].Yaku[YK_DBLREACH-1][0];
					tmp.TotalHan+=stcdata[i].TotalHan;
					tmp.TotalFu+=stcdata[i].TotalFu;
					tmp.PlayCount+=stcdata[i].PlayCount;
					tmp.TonpuCount+=stcdata[i].TonpuCount;
					tmp.HanchanCount+=stcdata[i].HanchanCount;
					tmp.IchanCount+=stcdata[i].IchanCount;
					tmp.StopCount+=stcdata[i].StopCount;
					tmp.KyokuCount+=stcdata[i].KyokuCount;
					tmp.ReachCount+=stcdata[i].ReachCount;
					tmp.TobiCount+=stcdata[i].TobiCount;
					tmp.RonCount+=stcdata[i].RonCount;
					tmp.TumoCount+=stcdata[i].TumoCount;
					for (j=0;j<2;j++) {
						tmp.HojuReachCount[j]+=stcdata[i].HojuReachCount[j];
						tmp.HojuFuroCount[j]+=stcdata[i].HojuFuroCount[j];
						tmp.HojuYamiCount[j]+=stcdata[i].HojuYamiCount[j];
					}
					tmp.YmTumoCount+=stcdata[i].YmTumoCount;
					tmp.YmRonCount+=stcdata[i].YmRonCount;
					for (j=0;j<2;j++)
						tmp.YmHojuCount[j]+=stcdata[i].YmHojuCount[j];
					tmp.FuroCount+=stcdata[i].FuroCount;
					tmp.AnkanCount+=stcdata[i].AnkanCount;
					tmp.KakanCount+=stcdata[i].KakanCount;
					tmp.ChiCount+=stcdata[i].ChiCount;
					tmp.PonCount+=stcdata[i].PonCount;
					tmp.MinkanCount+=stcdata[i].MinkanCount;
					tmp.TumoTotalScore+=stcdata[i].TumoTotalScore;
					tmp.RonTotalScore+=stcdata[i].RonTotalScore;
					tmp.HojuTotalScore+=stcdata[i].HojuTotalScore;
					for (j=0;j<2;j++)
						tmp.DblRonHojuCount[j]+=stcdata[i].DblRonHojuCount[j];
					tmp.DblRonHojuTotalScore+=stcdata[i].DblRonHojuTotalScore;
					if (stcdata[i].DblRonHojuMaxScore>tmp.DblRonHojuMaxScore)
						tmp.DblRonHojuMaxScore=stcdata[i].DblRonHojuMaxScore;
					if (stcdata[i].DoraMax>tmp.DoraMax)
						tmp.DoraMax=stcdata[i].DoraMax;
					if (stcdata[i].TumoMaxScore>tmp.TumoMaxScore)
						tmp.TumoMaxScore=stcdata[i].TumoMaxScore;
					if (stcdata[i].RonMaxScore>tmp.RonMaxScore)
						tmp.RonMaxScore=stcdata[i].RonMaxScore;
					if (stcdata[i].HojuMaxScore>tmp.HojuMaxScore)
						tmp.HojuMaxScore=stcdata[i].HojuMaxScore;
					for (j=0;j<6;j++) {
						tmp.Ryukyoku[j]+=stcdata[i].Ryukyoku[j];
					}
					for (j=0;j<4;j++) {
						tmp.RankCount[j]+=stcdata[i].RankCount[j];
					}
					for (j=0;j<13;j++) {
						tmp.HanCount[j]+=stcdata[i].HanCount[j];
					}
					for (j=0;j<11;j++) {
						tmp.FuCount[j]+=stcdata[i].FuCount[j];
					}
					for (j=0;j<2;j++) {
						tmp.KohaiRyukyoku[j]+=stcdata[i].KohaiRyukyoku[j];
					}
					tmp.RenchanCount+=stcdata[i].RenchanCount;
					if (stcdata[i].RenchanMax>tmp.RenchanMax)
						tmp.RenchanMax=stcdata[i].RenchanMax;
				}
				/*
				tmp.PlayCount/=4;
				tmp.TonpuCount/=4;
				tmp.HanchanCount/=4;
				tmp.IchanCount/=4;
				tmp.StopCount/=4;
				tmp.KyokuCount/=4;
				*/
				break;
			}
		} else {
			tmp=stcdata[p];
		}

		switch (rc_yk_type) {
		case 1: //通常役
		case 2:
		case 3:
			for (i=0;i<15;i++) {
				k=i+rc_yk_scroll;
				g.drawString(YakuName(k+1),x0,16*i+25,GPC_LT);
				switch (rc_yk_type) {
				case 1: s=Integer.toString(tmp.Yaku[k][0]); break;
				case 2: // s=Integer.toString(tmp.Yaku[k][1]);
				        if (tmp.Yaku[k][0]>0) {
				        	n=(tmp.Yaku[k][1]*100)/tmp.Yaku[k][0];
				        	n+=5; n/=10; //四捨五入
				        	s=Integer.toString(n/10)+"."+Integer.toString(n%10);
				        } else {
				        	s="0.0";
				        }
				        break;
				case 3: s=percent(tmp.Yaku[k][0],tmp.TumoCount+tmp.RonCount);
				        /*if (tmp.TumoCount+tmp.RonCount>0) {
				        	n=(tmp.Yaku[k][0]*10000)/(tmp.TumoCount+tmp.RonCount);
				        } else {
				        	n=0;
				        }
				        /*
				        if ((n>0)&&(n<10)) {
					        s="00"+Integer.toString(n%100);
					        s=Integer.toString(n/100)+"."+s.substring(s.length()-2,s.length())+"%";
				        } else {
				        	n/=10;
				        	s=Integer.toString(n/10)+"."+Integer.toString(n%10)+"%";
				        }//*/
				        //s=percent(n);
				        break;
				default: s=""; break;
				}
				g.drawString(s,x1,16*i+25,GPC_RT);
				//g.drawString(Integer.toString(stc_yaku[p][k]),x1,16*i+25,GPC_RT);
			}
			g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_SMALL));
			if (rc_yk_scroll<12) 
				g.drawString("▼",210,249,GPC_LT);
			if (rc_yk_scroll>0)
				g.drawString("▲",210,25,GPC_LT);
			break;
		case 4: //役満
		case 5:
			if (rc_yk_scroll<8) 
				g.drawString("▼",210,249,GPC_LT);
			if (rc_yk_scroll>0)
				g.drawString("▲",210,25,GPC_LT);
			i=0;
			for (j=rc_yk_scroll;j<15;j++) {
				g.drawString(YakuName(j+101),x0,16*i+25,GPC_LT);
				switch (rc_yk_type) {
				case 4: s=Integer.toString(tmp.Yakuman[j]); break;
				case 5: s=percent(tmp.Yakuman[j],tmp.TumoCount+tmp.RonCount);
				        /*
				        if (tmp.TumoCount+tmp.RonCount>0) {
				        	n=(tmp.Yakuman[j]*10000)/(tmp.TumoCount+tmp.RonCount);
				        } else {
				        	n=0;
				        }
				        /*
				        if ((n>0)&&(n<10)) {
					        s="00"+Integer.toString(n%100);
					        s=Integer.toString(n/100)+"."+s.substring(s.length()-2,s.length())+"%";
				        } else {
				        	n/=10;
				        	s=Integer.toString(n/10)+"."+Integer.toString(n%10)+"%";
				        }//*/
				        //s=percent(n);
				        break;
				default: s=""; break;
				}
				g.drawString(s,x1,16*i+25,GPC_RT);
				//g.drawString(Integer.toString(stc_yakuman[p][j]),x1,16*i+25,GPC_RT);
				i++;
			}
			i++;
			if (i<15) {
				g.drawString("ダブル役満",x0,16*i+25,GPC_LT);
				switch (rc_yk_type) {
				case 4: s=Integer.toString(tmp.Yakumans[0]); break;
				case 5: s=percent(tmp.Yakumans[0],tmp.TumoCount+tmp.RonCount);
						/*if (tmp.TumoCount+tmp.RonCount>0) {
			            	n=(tmp.Yakumans[0]*10000)/(tmp.TumoCount+tmp.RonCount);
			            } else {
				        	n=0;
				        }
				        /*
				        if ((n>0)&&(n<10)) {
					        s="00"+Integer.toString(n%100);
					        s=Integer.toString(n/100)+"."+s.substring(s.length()-2,s.length())+"%";
				        } else {
				        	n/=10;
				        	s=Integer.toString(n/10)+"."+Integer.toString(n%10)+"%";
				        }//*/
				        //s=percent(n);
				        break;
				default: s=""; break;
				}
				g.drawString(s,x1,16*i+25,GPC_RT);
				i++;

				g.drawString("トリプル役満",x0,16*i+25,GPC_LT);
				switch (rc_yk_type) {
				case 4: s=Integer.toString(tmp.Yakumans[1]); break;
				case 5: s=percent(tmp.Yakumans[1],tmp.TumoCount+tmp.RonCount);
				        /*
				        if (tmp.TumoCount+tmp.RonCount>0) {
				        	n=(tmp.Yakumans[1]*10000)/(tmp.TumoCount+tmp.RonCount);
				        } else {
				        	n=0;
				        }
				        /*
				        if ((n>0)&&(n<10)) {
					        s="00"+Integer.toString(n%100);
					        s=Integer.toString(n/100)+"."+s.substring(s.length()-2,s.length())+"%";
				        } else {
				        	n/=10;
				        	s=Integer.toString(n/10)+"."+Integer.toString(n%10)+"%";
				        }//*/
				        //s=percent(n);
				        break;
				default: s=""; break;
				}
				g.drawString(s,x1,16*i+25,GPC_RT);
				i++;

				if (tmp.Yakumans[2]>0) {
					g.drawString("４倍役満",x0,16*i+25,GPC_LT);
					switch (rc_yk_type) {
					case 4: s=Integer.toString(tmp.Yakumans[2]); break;
					case 5: s=percent(tmp.Yakumans[2],tmp.TumoCount+tmp.RonCount);
					        /*
					        if (tmp.TumoCount+tmp.RonCount>0) {
					        	n=(tmp.Yakumans[2]*10000)/(tmp.TumoCount+tmp.RonCount);
					        } else {
					        	n=0;
					        }
					        /*
					        if ((n>0)&&(n<10)) {
						        s="00"+Integer.toString(n%100);
						        s=Integer.toString(n/100)+"."+s.substring(s.length()-2,s.length())+"%";
					        } else {
					        	n/=10;
					        	s=Integer.toString(n/10)+"."+Integer.toString(n%10)+"%";
					        }//*/
					        //s=percent(n);
					        break;
					default: s=""; break;
					}
					g.drawString(s,x1,16*i+25,GPC_RT);
					i++;
				}

				if (tmp.Yakumans[3]>0) {
					g.drawString("５倍役満",x0,16*i+25,GPC_LT);
					switch (rc_yk_type) {
					case 4: s=Integer.toString(tmp.Yakumans[3]); break;
					case 5: s=percent(tmp.Yakumans[3],tmp.TumoCount+tmp.RonCount);
					        /*
					        if (tmp.TumoCount+tmp.RonCount>0) {
					        	n=(tmp.Yakumans[3]*10000)/(tmp.TumoCount+tmp.RonCount);
					        } else {
					        	n=0;
					        }
					        /*
					        if ((n>0)&&(n<10)) {
						        s="00"+Integer.toString(n%100);
						        s=Integer.toString(n/100)+"."+s.substring(s.length()-2,s.length())+"%";
					        } else {
					        	n/=10;
					        	s=Integer.toString(n/10)+"."+Integer.toString(n%10)+"%";
					        }//*/
					        //s=percent(n);
					        break;
					default: s=""; break;
					}
					g.drawString(s,x1,16*i+25,GPC_RT);
					i++;
				}

				if (tmp.Yakumans[4]>0) {
					g.drawString("６倍役満以上",x0,16*i+25,GPC_LT);
					switch (rc_yk_type) {
					case 4: s=Integer.toString(tmp.Yakumans[4]); break;
					case 5: s=percent(tmp.Yakumans[4],tmp.TumoCount+tmp.RonCount);
					        /*
					        if (tmp.TumoCount+tmp.RonCount>0) {
					        	n=(tmp.Yakumans[4]*10000)/(tmp.TumoCount+tmp.RonCount);
					        } else {
					        	n=0;
					        }
					        /*
					        if ((n>0)&&(n<10)) {
						        s="00"+Integer.toString(n%100);
						        s=Integer.toString(n/100)+"."+s.substring(s.length()-2,s.length())+"%";
					        } else {
					        	n/=10;
					        	s=Integer.toString(n/10)+"."+Integer.toString(n%10)+"%";
					        }//*/
					        //s=percent(n);
					        break;
					default: s=""; break;
					}
					g.drawString(s,x1,16*i+25,GPC_RT);
					i++;
				}
			}
			break;
		case 0: //その他
			if (rc_yk_scroll<74) 
				g.drawString("▼",210,249,GPC_LT);
			if (rc_yk_scroll>0)
				g.drawString("▲",210,25,GPC_LT);
			i=0; k=rc_yk_scroll;
			do {
				switch (k) {
				case 0:
					g.drawString("総プレイ回数", x0,25+16*i,GPC_LT);
					if (p<0)
						g.drawString(Integer.toString(tmp.PlayCount/4),x1,25+16*i,GPC_RT);
					else
						g.drawString(Integer.toString(tmp.PlayCount),x1,25+16*i,GPC_RT);
					i++; break;
				case 1:
					g.drawString("　東風戦", x0,25+16*i,GPC_LT);
					if (p<0)
						g.drawString(Integer.toString(tmp.TonpuCount/4),x1,25+16*i,GPC_RT);
					else 
						g.drawString(Integer.toString(tmp.TonpuCount),x1,25+16*i,GPC_RT);
					i++; break;
				case 2:
					g.drawString("　半荘戦", x0,25+16*i,GPC_LT);
					if (p<0)
						g.drawString(Integer.toString(tmp.HanchanCount/4),x1,25+16*i,GPC_RT);
					else
						g.drawString(Integer.toString(tmp.HanchanCount),x1,25+16*i,GPC_RT);
					i++; break;
				case 3:
					g.drawString("　一荘戦", x0,25+16*i,GPC_LT);
					if (p<0)
						g.drawString(Integer.toString(tmp.IchanCount/4),x1,25+16*i,GPC_RT);
					else 
						g.drawString(Integer.toString(tmp.IchanCount),x1,25+16*i,GPC_RT);
					i++; break;
				case 4:
					g.drawString("　中止回数", x0,25+16*i,GPC_LT);
					if (p<0)
						g.drawString(Integer.toString(tmp.StopCount/4),x1,25+16*i,GPC_RT);
					else
						g.drawString(Integer.toString(tmp.StopCount),x1,25+16*i,GPC_RT);
					i++; break;
				case 5:
					g.drawString("総局数", x0,25+16*i,GPC_LT);
					if (p<0)
						g.drawString(Integer.toString(tmp.KyokuCount/4),x1,25+16*i,GPC_RT);
					else
						g.drawString(Integer.toString(tmp.KyokuCount),x1,25+16*i,GPC_RT);
					i++; break;
				case 7:
				case 8:
				case 9:
				case 10:
					j=k-7;
					g.drawString(Integer.toString(j+1)+"位率", x0,25+16*i,GPC_LT);
					n=0;
					for (int u=0;u<4;u++)
						n+=tmp.RankCount[u];
					//if (n>0)
					//	n=(tmp.RankCount[j]*10000)/n;
					//if ((tmp.PlayCount-tmp.StopCount)>0)
					//	n=(tmp.RankCount[j]*1000)/(tmp.PlayCount-tmp.StopCount);
					g.drawString(percent(tmp.RankCount[j],n),x1,25+16*i,GPC_RT);
					//g.drawString(Integer.toString(n/10)+"."+Integer.toString(n%10)+"%"
					//                               ,x1,25+16*i,GPC_RT);
					i++;
					break;
				case 11:
					g.drawString("連対率", x0,25+16*i,GPC_LT);
					n=0;
					j=tmp.RankCount[0]+tmp.RankCount[1];
					for (int u=0;u<4;u++)
						n+=tmp.RankCount[u];
					g.drawString(percent(j,n),x1,25+16*i,GPC_RT);
					i++; break;
				case 12:
					g.drawString("飛び率", x0,25+16*i,GPC_LT);
					n=0;
					for (int u=0;u<4;u++)
						n+=tmp.RankCount[u];
					//if (n>0)
					//	n=(tmp.TobiCount*10000)/n;
					//if (tmp.PlayCount-tmp.StopCount>0)
					//	n=(tmp.TobiCount*1000)/(tmp.PlayCount-tmp.StopCount);
					g.drawString(percent(tmp.TobiCount,n),x1,25+16*i,GPC_RT);
					//g.drawString(Integer.toString(n/10)+"."+Integer.toString(n%10)+"%"
					//                               ,x1,25+16*i,GPC_RT);
					i++; break;

				case 14:
					g.drawString("和了率", x0,25+16*i,GPC_LT);
					//n=0; if (tmp.KyokuCount>0)
					//	n=((tmp.TumoCount+tmp.RonCount)*10000)/tmp.KyokuCount;
					g.drawString(percent(tmp.TumoCount+tmp.RonCount,tmp.KyokuCount)
					                          ,x1,25+16*i,GPC_RT);
					//g.drawString(Integer.toString(n/10)+"."+Integer.toString(n%10)+"%"
					//                               ,x1,25+16*i,GPC_RT);
					i++; break;
				case 15:
					g.drawString("放銃率(全)", x0,25+16*i,GPC_LT);
					//n=0;
					//if (tmp.KyokuCount>0) 
						n=tmp.HojuReachCount[0]+tmp.HojuReachCount[1]
						    +tmp.HojuFuroCount[0]+tmp.HojuFuroCount[1]
						    +tmp.HojuYamiCount[0]+tmp.HojuYamiCount[1]
						    -tmp.DblRonHojuCount[0]-tmp.DblRonHojuCount[1];
					g.drawString(percent(n,tmp.KyokuCount),x1,25+16*i,GPC_RT);
					//g.drawString(Integer.toString(n/10)+"."+Integer.toString(n%10)+"%"
					//                               ,x1,25+16*i,GPC_RT);
					i++; break;
				case 16:
					g.drawString("放銃率", x0,25+16*i,GPC_LT);
					//n=0;
					//if (tmp.KyokuCount>0) 
						n=tmp.HojuReachCount[0]+tmp.HojuFuroCount[0]+tmp.HojuYamiCount[0]
						    -tmp.DblRonHojuCount[0];
					g.drawString(percent(n,tmp.KyokuCount),x1,25+16*i,GPC_RT);
					//g.drawString(Integer.toString(n/10)+"."+Integer.toString(n%10)+"%"
					//                               ,x1,25+16*i,GPC_RT);
					i++; break;
				case 17:
					g.drawString("　立直者へ", x0,25+16*i,GPC_LT);
					//n=0; if (tmp.KyokuCount>0) 
					//	n=(tmp.HojuReachCount[0]*1000)/tmp.KyokuCount;
					g.drawString(percent(tmp.HojuReachCount[0],tmp.KyokuCount)
					                                 ,x1,25+16*i,GPC_RT);
					//g.drawString(Integer.toString(n/10)+"."+Integer.toString(n%10)+"%"
					//                               ,x1,25+16*i,GPC_RT);
					i++; break;
				case 18:
					g.drawString("　副露者へ", x0,25+16*i,GPC_LT);
					//n=0; if (tmp.KyokuCount>0) 
					//	n=(tmp.HojuFuroCount[0]*1000)/tmp.KyokuCount;
					g.drawString(percent(tmp.HojuFuroCount[0],tmp.KyokuCount)
					                                 ,x1,25+16*i,GPC_RT);
					//g.drawString(Integer.toString(n/10)+"."+Integer.toString(n%10)+"%"
					//                               ,x1,25+16*i,GPC_RT);
					i++; break;
				case 19:
					g.drawString("　闇聴者へ", x0,25+16*i,GPC_LT);
					//n=0; if (tmp.KyokuCount>0) 
					//	n=(tmp.HojuYamiCount[0]*1000)/tmp.KyokuCount;
					g.drawString(percent(tmp.HojuYamiCount[0],tmp.KyokuCount)
					                                 ,x1,25+16*i,GPC_RT);
					//g.drawString(Integer.toString(n/10)+"."+Integer.toString(n%10)+"%"
					//                               ,x1,25+16*i,GPC_RT);
					i++; break;
				case 20:
					g.drawString("放銃率(立直後)", x0,25+16*i,GPC_LT);
					//n=0; if (tmp.KyokuCount>0) 
						n=tmp.HojuReachCount[1]+tmp.HojuFuroCount[1]+tmp.HojuYamiCount[1]
						   -tmp.DblRonHojuCount[1];
					g.drawString(percent(n,tmp.KyokuCount),x1,25+16*i,GPC_RT);
					//g.drawString(Integer.toString(n/10)+"."+Integer.toString(n%10)+"%"
					//                               ,x1,25+16*i,GPC_RT);
					i++; break;
				case 21:
					g.drawString("副露率", x0,25+16*i,GPC_LT);
					//n=0; if (tmp.KyokuCount>0)
					//	n=(tmp.FuroCount*1000)/tmp.KyokuCount;
					g.drawString(percent(tmp.FuroCount,tmp.KyokuCount),x1,25+16*i,GPC_RT);
					//g.drawString(Integer.toString(n/10)+"."+Integer.toString(n%10)+"%"
					//                               ,x1,25+16*i,GPC_RT);
					i++; break;
				case 22:
					g.drawString("立直率", x0,25+16*i,GPC_LT);
					//n=0; if (tmp.KyokuCount>0)
					//	n=(tmp.ReachCount*1000)/tmp.KyokuCount;
					g.drawString(percent(tmp.ReachCount,tmp.KyokuCount),x1,25+16*i,GPC_RT);
					//g.drawString(Integer.toString(n/10)+"."+Integer.toString(n%10)+"%"
					//                               ,x1,25+16*i,GPC_RT);
					i++; break;
				case 23:
					g.drawString("立直成功率", x0,25+16*i,GPC_LT);
					//n=0; if (tmp.ReachCount>0)
						n=tmp.Yaku[YK_REACH-1][0]+tmp.Yaku[YK_DBLREACH-1][0];// *1000)/tmp.ReachCount;
					g.drawString(percent(n,tmp.ReachCount),x1,25+16*i,GPC_RT);
					//g.drawString(Integer.toString(n/10)+"."+Integer.toString(n%10)+"%"
					//                               ,x1,25+16*i,GPC_RT);
					i++; break;
				case 24:
					g.drawString("平均和了点", x0,25+16*i,GPC_LT);
					n=0; if (tmp.TumoCount+tmp.RonCount>0)
						n=(tmp.RonTotalScore+tmp.TumoTotalScore)/(tmp.TumoCount+tmp.RonCount);
					g.drawString(Integer.toString(n),x1,25+16*i,GPC_RT);
					i++; break;
				case 25:
					g.drawString("平均放銃点", x0,25+16*i,GPC_LT);
					n=tmp.HojuReachCount[0]+tmp.HojuFuroCount[0]+tmp.HojuYamiCount[0]
					  +tmp.HojuReachCount[1]+tmp.HojuFuroCount[1]+tmp.HojuYamiCount[1]
					  -tmp.DblRonHojuCount[0]-tmp.DblRonHojuCount[1];
					if (n>0)
						n=tmp.HojuTotalScore/n;
					g.drawString(Integer.toString(n),x1,25+16*i,GPC_RT);
					i++; break;
				case 27:
					g.drawString("ツモで和了", x0,25+16*i,GPC_LT);
					g.drawString(Integer.toString(tmp.TumoCount),x1,25+16*i,GPC_RT);
					i++; break;
				case 28:
					g.drawString("　役満でツモ", x0,25+16*i,GPC_LT);
					g.drawString(Integer.toString(tmp.YmTumoCount),x1,25+16*i,GPC_RT);
					i++; break;
				case 29:
					g.drawString("ロンで和了", x0,25+16*i,GPC_LT);
					g.drawString(Integer.toString(tmp.RonCount),x1,25+16*i,GPC_RT);
					i++; break;
				case 30:
					g.drawString("　役満でロン", x0,25+16*i,GPC_LT);
					g.drawString(Integer.toString(tmp.YmRonCount),x1,25+16*i,GPC_RT);
					i++; break;
				case 31:
					g.drawString("放銃回数(全)", x0,25+16*i,GPC_LT);
					n=tmp.HojuReachCount[0]+tmp.HojuFuroCount[0]+tmp.HojuYamiCount[0]
					  +tmp.HojuReachCount[1]+tmp.HojuFuroCount[1]+tmp.HojuYamiCount[1]
					  -tmp.DblRonHojuCount[0]-tmp.DblRonHojuCount[1];
					g.drawString(Integer.toString(n),x1,25+16*i,GPC_RT);
					i++; break;
				case 32:
					g.drawString("放銃回数", x0,25+16*i,GPC_LT);
					n=tmp.HojuReachCount[0]+tmp.HojuFuroCount[0]+tmp.HojuYamiCount[0]
					      -tmp.DblRonHojuCount[0];
					g.drawString(Integer.toString(n),x1,25+16*i,GPC_RT);
					i++; break;
				case 33:
					g.drawString("　立直者に放銃", x0,25+16*i,GPC_LT);
					g.drawString(Integer.toString(tmp.HojuReachCount[0]),x1,25+16*i,GPC_RT);
					i++; break;
				case 34:
					g.drawString("　副露者に放銃", x0,25+16*i,GPC_LT);
					g.drawString(Integer.toString(tmp.HojuFuroCount[0]),x1,25+16*i,GPC_RT);
					i++; break;
				case 35:
					g.drawString("　闇聴者に放銃", x0,25+16*i,GPC_LT);
					g.drawString(Integer.toString(tmp.HojuYamiCount[0]),x1,25+16*i,GPC_RT);
					i++; break;
				case 36:
					g.drawString("　役満に放銃", x0,25+16*i,GPC_LT);
					g.drawString(Integer.toString(tmp.YmHojuCount[0]),x1,25+16*i,GPC_RT);
					i++; break;
				case 37:
					g.drawString("放銃回数(立直後)", x0,25+16*i,GPC_LT);
					n=tmp.HojuReachCount[1]+tmp.HojuFuroCount[1]+tmp.HojuYamiCount[1]
					    -tmp.DblRonHojuCount[1];
					g.drawString(Integer.toString(n),x1,25+16*i,GPC_RT);
					i++; break;
				case 38:
					g.drawString("　立直者に放銃", x0,25+16*i,GPC_LT);
					g.drawString(Integer.toString(tmp.HojuReachCount[1]),x1,25+16*i,GPC_RT);
					i++; break;
				case 39:
					g.drawString("　副露者に放銃", x0,25+16*i,GPC_LT);
					g.drawString(Integer.toString(tmp.HojuFuroCount[1]),x1,25+16*i,GPC_RT);
					i++; break;
				case 40:
					g.drawString("　闇聴者に放銃", x0,25+16*i,GPC_LT);
					g.drawString(Integer.toString(tmp.HojuYamiCount[1]),x1,25+16*i,GPC_RT);
					i++; break;
				case 41:
					g.drawString("　役満に放銃", x0,25+16*i,GPC_LT);
					g.drawString(Integer.toString(tmp.YmHojuCount[1]),x1,25+16*i,GPC_RT);
					i++; break;
				case 42:
					n=tmp.DblRonHojuCount[0]+tmp.DblRonHojuCount[1];
					g.drawString("ダブロン放銃", x0,25+16*i,GPC_LT);
					g.drawString(Integer.toString(n),x1,25+16*i,GPC_RT);
					i++; break;
				case 44:
					g.drawString("ツモ総和了点", x0,25+16*i,GPC_LT);
					g.drawString(Integer.toString(tmp.TumoTotalScore),x1,25+16*i,GPC_RT);
					i++; break;
				case 45:
					g.drawString("ツモ最高点", x0,25+16*i,GPC_LT);
					g.drawString(Integer.toString(tmp.TumoMaxScore),x1,25+16*i,GPC_RT);
					i++; break;
				case 46:
					g.drawString("ロン総和了点", x0,25+16*i,GPC_LT);
					g.drawString(Integer.toString(tmp.RonTotalScore),x1,25+16*i,GPC_RT);
					i++; break;
				case 47:
					g.drawString("ロン最高点", x0,25+16*i,GPC_LT);
					g.drawString(Integer.toString(tmp.RonMaxScore),x1,25+16*i,GPC_RT);
					i++; break;
				case 48:
					g.drawString("総放銃点", x0,25+16*i,GPC_LT);
					g.drawString(Integer.toString(tmp.HojuTotalScore),x1,25+16*i,GPC_RT);
					i++; break;
				case 49:
					g.drawString("放銃最高点", x0,25+16*i,GPC_LT);
					g.drawString(Integer.toString(tmp.HojuMaxScore),x1,25+16*i,GPC_RT);
					i++; break;
				case 50:
					g.drawString("ダブロン放銃", x0,25+16*i,GPC_LT);
					i++; break;
				case 51:
					g.drawString("　総放銃点", x0,25+16*i,GPC_LT);
					g.drawString(Integer.toString(tmp.DblRonHojuTotalScore),x1,25+16*i,GPC_RT);
					i++; break;
				case 52:
					g.drawString("　放銃最高点", x0,25+16*i,GPC_LT);
					g.drawString(Integer.toString(tmp.DblRonHojuMaxScore),x1,25+16*i,GPC_RT);
					i++; break;


				case 54:
					n=0;
					for (j=0;j<4;j++)
						n+=tmp.RankCount[j];
					if (n>0) {
						n=((tmp.RankCount[0]+2*tmp.RankCount[1]+3*tmp.RankCount[2]+4*tmp.RankCount[3])*100)/n;
						n+=5;
						n/=10;
					}
					g.drawString("平均順位", x0,25+16*i,GPC_LT);
					g.drawString(Integer.toString(n/10)+"."+Integer.toString(n%10)
					                           ,x1,25+16*i,GPC_RT);
					i++; break;
				case 55:
				case 56:
				case 57:
				case 58:
					j=k-55;
					g.drawString(Integer.toString(j+1)+"位回数", x0,25+16*i,GPC_LT);
					g.drawString(Integer.toString(tmp.RankCount[j]),x1,25+16*i,GPC_RT);
					i++; break;
				case 59:
					g.drawString("飛び回数", x0,25+16*i,GPC_LT);
					g.drawString(Integer.toString(tmp.TobiCount),x1,25+16*i,GPC_RT);
					i++; break;

				case 61:
					g.drawString("立直回数", x0,25+16*i,GPC_LT);
					g.drawString(Integer.toString(tmp.ReachCount),x1,25+16*i,GPC_RT);
					i++; break;

				case 63:
					g.drawString("副露した局数", x0,25+16*i,GPC_LT);
					g.drawString(Integer.toString(tmp.FuroCount),x1,25+16*i,GPC_RT);
					i++; break;
				case 64:
					g.drawString("チー", x0,25+16*i,GPC_LT);
					g.drawString(Integer.toString(tmp.ChiCount),x1,25+16*i,GPC_RT);
					i++; break;
				case 65:
					g.drawString("ポン", x0,25+16*i,GPC_LT);
					g.drawString(Integer.toString(tmp.PonCount),x1,25+16*i,GPC_RT);
					i++; break;
				case 66:
					g.drawString("大明槓", x0,25+16*i,GPC_LT);
					g.drawString(Integer.toString(tmp.MinkanCount),x1,25+16*i,GPC_RT);
					i++; break;
				case 67:
					g.drawString("暗槓", x0,25+16*i,GPC_LT);
					g.drawString(Integer.toString(tmp.AnkanCount),x1,25+16*i,GPC_RT);
					i++; break;
				case 68:
					g.drawString("加槓", x0,25+16*i,GPC_LT);
					g.drawString(Integer.toString(tmp.KakanCount),x1,25+16*i,GPC_RT);
					i++; break;
					
				case 70:
					g.drawString("最大ドラ乗り", x0,25+16*i,GPC_LT);
					g.drawString(Integer.toString(tmp.DoraMax),x1,25+16*i,GPC_RT);
					i++; break;
					
				case 72:
				case 73:
				case 74:
				case 75:
				case 76:
				case 77: //特殊流局
					j=k-72;
					g.drawString(RyukyokuName(j+1), x0,25+16*i,GPC_LT);
					g.drawString(Integer.toString(tmp.Ryukyoku[j]),x1,25+16*i,GPC_RT);
					i++;
					break;
				case 79:
					n=tmp.KohaiRyukyoku[0]+tmp.KohaiRyukyoku[1];
					g.drawString("荒廃流局", x0,25+16*i,GPC_LT);
					if (p<0)
						g.drawString(Integer.toString(n/4),x1,25+16*i,GPC_RT);
					else
						g.drawString(Integer.toString(n),x1,25+16*i,GPC_RT);
					i++; break;
				case 80:
					n=tmp.KohaiRyukyoku[0]+tmp.KohaiRyukyoku[1];
					g.drawString("　遭遇率", x0,25+16*i,GPC_LT);
					g.drawString(percent(n,tmp.KyokuCount),x1,25+16*i,GPC_RT);
					i++; break;
				case 81:
					n=tmp.KohaiRyukyoku[0]+tmp.KohaiRyukyoku[1];
					g.drawString("　テンパイ率", x0,25+16*i,GPC_LT);
					g.drawString(percent(tmp.KohaiRyukyoku[1],n),x1,25+16*i,GPC_RT);
					i++; break;
				case 82:
					g.drawString("　テンパイ数", x0,25+16*i,GPC_LT);
					g.drawString(Integer.toString(tmp.KohaiRyukyoku[1]),x1,25+16*i,GPC_RT);
					i++; break;
				case 83:
					g.drawString("　ノーテン数", x0,25+16*i,GPC_LT);
					g.drawString(Integer.toString(tmp.KohaiRyukyoku[0]),x1,25+16*i,GPC_RT);
					i++; break;
				
				case 85:
					g.drawString("総連荘数", x0,25+16*i,GPC_LT);
					g.drawString(Integer.toString(tmp.RenchanCount),x1,25+16*i,GPC_RT);
					i++; break;
				case 86:
					g.drawString("連荘率", x0,25+16*i,GPC_LT);
					g.drawString(percent(tmp.RenchanCount,tmp.KyokuCount),x1,25+16*i,GPC_RT);
					i++; break;
				case 87:
					g.drawString("最大連荘数", x0,25+16*i,GPC_LT);
					g.drawString(Integer.toString(tmp.RenchanMax),x1,25+16*i,GPC_RT);
					i++; break;

				default:
					i++; break;
				}
				k++;
			} while (i<15);
			break;
		case 6:
		case 7:
			i=0;
			g.drawString("総計翻数", x0,25+16*i,GPC_LT);
			g.drawString(Integer.toString(tmp.TotalHan),x1,25+16*i,GPC_RT);
			i++;
			switch (rc_yk_type) {
			case 6:
				g.drawString("最大翻数", x0,25+16*i,GPC_LT);
				g.drawString(Integer.toString(tmp.MaxHan),x1,25+16*i,GPC_RT);
				break;
			case 7:
				n=0;
				for (j=0;j<13;j++)
					n+=tmp.HanCount[j];
				g.drawString("平均翻数", x0,25+16*i,GPC_LT);
				if (n>0) n=(tmp.TotalHan*10)/n;
				g.drawString(Integer.toString(n/10)+"."+Integer.toString(n%10)
				                        ,x1,25+16*i,GPC_RT);
				break;
			}
			i++;
			n=0;
			for (j=0;j<13;j++)
				n+=tmp.HanCount[j];
			int m;
			for (j=0;j<12;j++) {
				g.drawString(" "+Integer.toString(j+1)+"翻", x0,25+16*i,GPC_LT);
				switch (rc_yk_type) {
				case 6:
					g.drawString(Integer.toString(tmp.HanCount[j]),x1,25+16*i,GPC_RT);
					break;
				case 7:
					/*
					m=0; if (n>0) m=(tmp.HanCount[j]*10000)/n;
			        if ((m>0)&&(m<10)) {
				        s="00"+Integer.toString(m%100);
				        s=Integer.toString(m/100)+"."+s.substring(s.length()-2,s.length())+"%";
			        } else {
			        	m/=10;
			        	s=Integer.toString(m/10)+"."+Integer.toString(m%10)+"%";
			        } //*/
				    g.drawString(percent(tmp.HanCount[j],n),x1,25+16*i,GPC_RT);
					//g.drawString(Integer.toString(m/10)+"."+Integer.toString(m%10)+"%"
				    //                    ,x1,25+16*i,GPC_RT);
					
					break;
				}
				i++;
			}
			g.drawString(" 13翻以上", x0,25+16*i,GPC_LT);
			switch (rc_yk_type) {
			case 6:
				g.drawString(Integer.toString(tmp.HanCount[12]),x1,25+16*i,GPC_RT);
				break;
			case 7:
				/*
				m=0; if (n>0) m=(tmp.HanCount[12]*10000)/n;
		        if ((m>0)&&(m<10)) {
			        s="00"+Integer.toString(m%100);
			        s=Integer.toString(m/100)+"."+s.substring(s.length()-2,s.length())+"%";
		        } else {
		        	m/=10;
		        	s=Integer.toString(m/10)+"."+Integer.toString(m%10)+"%";
		        } //*/
			    g.drawString(percent(tmp.HanCount[12],n),x1,25+16*i,GPC_RT);
				//g.drawString(Integer.toString(m/10)+"."+Integer.toString(m%10)+"%"
			    //                    ,x1,25+16*i,GPC_RT);
				
				break;
			}
			i++;
			break;
		case 8:
		case 9:
			i=0;
			g.drawString("総計符数", x0,25+16*i,GPC_LT);
			g.drawString(Integer.toString(tmp.TotalFu),x1,25+16*i,GPC_RT);
			i++;
			int m1;
			n=0;
			for (j=0;j<12;j++)
				n+=tmp.FuCount[j];
			switch (rc_yk_type) {
			case 8:
				g.drawString("最大符数", x0,25+16*i,GPC_LT);
				g.drawString(Integer.toString(tmp.MaxFu),x1,25+16*i,GPC_RT);
				break;
			case 9:
				g.drawString("平均符数", x0,25+16*i,GPC_LT);
				m1=0; if (n>0) m1=(tmp.TotalFu*10)/n;
				g.drawString(Integer.toString(m1/10)+"."+Integer.toString(m1%10)
				                            ,x1,25+16*i,GPC_RT);
			}
			i++;
			g.drawString(" 20符", x0,25+16*i,GPC_LT);
			switch (rc_yk_type) {
			case 8: 
				g.drawString(Integer.toString(tmp.FuCount[0]),x1,25+16*i,GPC_RT);
				break;
			case 9:
				/*
				m1=0; if (n>0) m1=(tmp.FuCount[0]*10000)/n;
		        if ((m1>0)&&(m1<10)) {
			        s="00"+Integer.toString(m1%100);
			        s=Integer.toString(m1/100)+"."+s.substring(s.length()-2,s.length())+"%";
		        } else {
		        	m1/=10;
		        	s=Integer.toString(m1/10)+"."+Integer.toString(m1%10)+"%";
		        } //*/
			    g.drawString(percent(tmp.FuCount[0],n),x1,25+16*i,GPC_RT);
				//g.drawString(Integer.toString(m1/10)+"."+Integer.toString(m1%10)+"%"
				//                            ,x1,25+16*i,GPC_RT);
				break;
			}
			i++;
			g.drawString(" 25符", x0,25+16*i,GPC_LT);
			switch (rc_yk_type) {
			case 8: 
				g.drawString(Integer.toString(tmp.FuCount[1]),x1,25+16*i,GPC_RT);
				break;
			case 9:
				/*
				m1=0; if (n>0) m1=(tmp.FuCount[1]*10000)/n;
		        if ((m1>0)&&(m1<10)) {
			        s="00"+Integer.toString(m1%100);
			        s=Integer.toString(m1/100)+"."+s.substring(s.length()-2,s.length())+"%";
		        } else {
		        	m1/=10;
		        	s=Integer.toString(m1/10)+"."+Integer.toString(m1%10)+"%";
		        } //*/
			    g.drawString(percent(tmp.FuCount[1],n),x1,25+16*i,GPC_RT);
				//g.drawString(Integer.toString(m1/10)+"."+Integer.toString(m1%10)+"%"
				//                            ,x1,25+16*i,GPC_RT);
				break;
			}
			i++;
			for (j=2;j<11;j++) {
				g.drawString(" "+Integer.toString((j+1)*10)+"符", x0,25+16*i,GPC_LT);
				switch (rc_yk_type) {
				case 8: 
					g.drawString(Integer.toString(tmp.FuCount[j]),x1,25+16*i,GPC_RT);
					break;
				case 9:
					/*
					m1=0; if (n>0) m1=(tmp.FuCount[j]*10000)/n;
			        if ((m1>0)&&(m1<10)) {
				        s="00"+Integer.toString(m1%100);
				        s=Integer.toString(m1/100)+"."+s.substring(s.length()-2,s.length())+"%";
			        } else {
			        	m1/=10;
			        	s=Integer.toString(m1/10)+"."+Integer.toString(m1%10)+"%";
			        } //*/
				    g.drawString(percent(tmp.FuCount[j],n),x1,25+16*i,GPC_RT);
					//g.drawString(Integer.toString(m1/10)+"."+Integer.toString(m1%10)+"%"
					//                            ,x1,25+16*i,GPC_RT);
					break;
				}
				i++;
			}
			g.drawString(" 120符以上", x0,25+16*i,GPC_LT);
			switch (rc_yk_type) {
			case 8: 
				g.drawString(Integer.toString(tmp.FuCount[11]),x1,25+16*i,GPC_RT);
				break;
			case 9:
				/*
				m1=0; if (n>0) m1=(tmp.FuCount[11]*10000)/n;
		        if ((m1>0)&&(m1<10)) {
			        s="00"+Integer.toString(m1%100);
			        s=Integer.toString(m1/100)+"."+s.substring(s.length()-2,s.length())+"%";
		        } else {
		        	m1/=10;
		        	s=Integer.toString(m1/10)+"."+Integer.toString(m1%10)+"%";
		        }//*/
			    g.drawString(percent(tmp.FuCount[11],n),x1,25+16*i,GPC_RT);
				//g.drawString(Integer.toString(m1/10)+"."+Integer.toString(m1%10)+"%"
				//                            ,x1,25+16*i,GPC_RT);
				break;
			}
			i++;
			break;
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void drawRc_data() {
		int i,j,k,n,m,t;
		int x0=35;
		int x1=205;
		int y0=25;
		int p=rc_dt_player;
		String s1;

		switch (rule_PlayLong) {
		case 0:  s1="東風戦"; break;
		case 1:  s1="半荘戦"; break;
		case 2:  s1="一荘戦"; break;
		default: s1="？？？"; break;
		}
		
		g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_MEDIUM));
		g.setColor(255,255,255);
		g.drawString(s1+"の"+PlayerName(p)+"の戦績",120,5,GPC_HT);
		
		if (p>-1)
			g.drawString("＜",5,126,GPC_LT);
		if (p<4)
			g.drawString("＞",235,126,GPC_RT);

		SensekiData tmp;
		
		if (p<0) {
			tmp=new SensekiData(); //************************************************** ここ！
			//*
			for (i=0;i<senseki.length;i++) {
				tmp.PlayCount+=senseki[i].PlayCount;
				tmp.ContinueCount+=senseki[i].ContinueCount;
				tmp.StopCount+=senseki[i].StopCount;
				tmp.TobiCount+=senseki[i].TobiCount;
				tmp.TotalPoint+=senseki[i].TotalPoint;
				if (senseki[i].MaxPoint>tmp.MaxPoint)
					tmp.MaxPoint=senseki[i].MaxPoint;
				if (senseki[i].MinPoint<tmp.MinPoint)
					tmp.MinPoint=senseki[i].MinPoint;
				for (j=0;j<4;j++) {
					tmp.RankCount[j]     += senseki[i].RankCount[j];
					tmp.KyokuCount[j]    += senseki[i].KyokuCount[j];
					tmp.HoraCount[j]     += senseki[i].HoraCount[j];
					tmp.ReachCount[j]    += senseki[i].ReachCount[j];
					tmp.ReachHoraCount[j]+= senseki[i].ReachHoraCount[j];
					tmp.FuroCount[j]     += senseki[i].FuroCount[j];
					tmp.HoraTotalScore[j]+= senseki[i].HoraTotalScore[j];
					tmp.HojuTotalScore[j]+= senseki[i].HojuTotalScore[j];
					for (k=0;k<2;k++) {
						tmp.HojuReachCount[j][k]+=senseki[i].HojuReachCount[j][k];
						tmp.HojuFuroCount[j][k] +=senseki[i].HojuFuroCount[j][k];
						tmp.HojuYamiCount[j][k] +=senseki[i].HojuYamiCount[j][k];
						tmp.DblRonHojuCount[j][k] +=senseki[i].DblRonHojuCount[j][k];
					}
				}
			} //*/
		} else {
			tmp=senseki[p];
		}
		
		switch (rc_dt_type) {
		case 0:
			if (rc_dt_scroll<41) 
				g.drawString("▼",210,249,GPC_LT);
			if (rc_dt_scroll>0)
				g.drawString("▲",210,25,GPC_LT);
			i=0; k=rc_dt_scroll;

			s1="";
			/*
			if (rule_Continue) {
				switch (rule_PlayLong) {
				case 0: s1=s1+"南入"; if (rule_SuddenDeath) s1=s1+"SD"; s1=s1+"有 "; break;
				case 1: s1=s1+"西入"; if (rule_SuddenDeath) s1=s1+"SD"; s1=s1+"有 "; break;
				}
			} //*/
			s1=s1+"飛び";
			if (rule_tobi) s1=s1+"有"; else s1=s1+"無";
			s1=s1+" 喰い断";
			if (rule_kuitan) s1=s1+"有"; else s1=s1+"無";			
			g.drawString(s1, 120,y0+16*i,GPC_HT);
			i++;

			do {
				switch (k) {
				case 0:
					g.drawString("総プレイ数", x0,y0+16*i,GPC_LT);
					n=tmp.PlayCount; if (p<0) n/=4;
					g.drawString(Integer.toString(n),x1,y0+16*i,GPC_RT);
					i++; break;
				case 1:
					//if ((rule_PlayLong<2)&&(rule_Continue)) {
					if (rule_PlayLong<2) {
						switch (rule_PlayLong) {
						case 0:
							g.drawString("　南入回数", x0,y0+16*i,GPC_LT); break;
						case 1:
							g.drawString("　西入回数", x0,y0+16*i,GPC_LT); break;
						}
						n=tmp.ContinueCount; if (p<0) n/=4;
						g.drawString(Integer.toString(n),x1,y0+16*i,GPC_RT);
						i++;
					}
					break;
				case 2:
					g.drawString("　中止回数", x0,y0+16*i,GPC_LT);
					n=tmp.StopCount; if (p<0) n/=4;
					g.drawString(Integer.toString(n),x1,y0+16*i,GPC_RT);
					i++; break;
				case 3:
					n=0;
					for (j=0;j<4;j++)
						n+=tmp.KyokuCount[j];
					if (p<0) n/=4;
					g.drawString("総局数", x0,y0+16*i,GPC_LT);
					g.drawString(Integer.toString(n),x1,y0+16*i,GPC_RT);
					i++; break;
				case 5:
				case 6:
				case 7:
				case 8:
					j=k-5;
					//n=tmp.PlayCount-tmp.StopCount;
					n=0;
					for (int u=0;u<4;u++)
						n+=tmp.RankCount[u];
					g.drawString(Integer.toString(j+1)+"位率", x0,y0+16*i,GPC_LT);
					//m=0;
					//if (n>0) m=(tmp.RankCount[j]*1000)/n;
					//g.drawString(Integer.toString(m/10)+"."+Integer.toString(m%10)+"%"
					g.drawString(percent(tmp.RankCount[j],n)
				                               ,x1,y0+16*i,GPC_RT);
					i++; break;
				case 9:
					g.drawString("連対率", x0,y0+16*i,GPC_LT);
					n=0;
					for (int u=0;u<4;u++)
						n+=tmp.RankCount[u];
					j=tmp.RankCount[0]+tmp.RankCount[1];
					g.drawString(percent(j,n)
				                               ,x1,y0+16*i,GPC_RT);
					i++; break;
				case 10:
					g.drawString("飛び率", x0,y0+16*i,GPC_LT);
					//n=tmp.PlayCount-tmp.StopCount;
					n=0;
					for (int u=0;u<4;u++)
						n+=tmp.RankCount[u];
					//m=tmp.TobiCount;
					//if (n>0) m=(m*1000)/n;
					//g.drawString(Integer.toString(m/10)+"."+Integer.toString(m%10)+"%"
					g.drawString(percent(tmp.TobiCount,n)
				                               ,x1,y0+16*i,GPC_RT);
					i++; break;
				case 12:
					g.drawString("和了率", x0,y0+16*i,GPC_LT);
					n=0; m=0;
					for (j=0;j<4;j++) {
						n+=tmp.KyokuCount[j]; m+=tmp.HoraCount[j];
					}
					//if (n>0) m=(m*1000)/n;
					//g.drawString(Integer.toString(m/10)+"."+Integer.toString(m%10)+"%"
					g.drawString(percent(m,n)
				                               ,x1,y0+16*i,GPC_RT);
					i++; break;
				case 13:
					g.drawString("放銃率(全)", x0,y0+16*i,GPC_LT);
					n=0; m=0;
					for (j=0;j<4;j++) {
						n+=tmp.KyokuCount[j]; 
						m+=tmp.HojuReachCount[j][0]+tmp.HojuFuroCount[j][0]+tmp.HojuYamiCount[j][0]
						   +tmp.HojuReachCount[j][1]+tmp.HojuFuroCount[j][1]+tmp.HojuYamiCount[j][1]
						   -tmp.DblRonHojuCount[j][0]-tmp.DblRonHojuCount[j][1];
					}
					//if (n>0) m=(m*1000)/n;
					//g.drawString(Integer.toString(m/10)+"."+Integer.toString(m%10)+"%"
					g.drawString(percent(m,n)
				                               ,x1,y0+16*i,GPC_RT);
					i++; break;
				case 14:
					g.drawString("放銃率", x0,y0+16*i,GPC_LT);
					n=0; m=0;
					for (j=0;j<4;j++) {
						n+=tmp.KyokuCount[j]; 
						m+=tmp.HojuReachCount[j][0]+tmp.HojuFuroCount[j][0]+tmp.HojuYamiCount[j][0]
						     -tmp.DblRonHojuCount[j][0];
					} 
					//if (n>0) m=(m*1000)/n;
					//g.drawString(Integer.toString(m/10)+"."+Integer.toString(m%10)+"%"
					g.drawString(percent(m,n)
				                               ,x1,y0+16*i,GPC_RT);
					i++; break;
				case 15:
					g.drawString("　立直者へ", x0,y0+16*i,GPC_LT);
					n=0; m=0;
					for (j=0;j<4;j++) {
						n+=tmp.KyokuCount[j]; 
						m+=tmp.HojuReachCount[j][0];
					}
					//if (n>0) m=(m*1000)/n;
					//g.drawString(Integer.toString(m/10)+"."+Integer.toString(m%10)+"%"
					g.drawString(percent(m,n)
				                               ,x1,y0+16*i,GPC_RT);
					i++; break;
				case 16:
					g.drawString("　副露者へ", x0,y0+16*i,GPC_LT);
					n=0; m=0;
					for (j=0;j<4;j++) {
						n+=tmp.KyokuCount[j]; 
						m+=tmp.HojuFuroCount[j][0];
					}
					//if (n>0) m=(m*1000)/n;
					//g.drawString(Integer.toString(m/10)+"."+Integer.toString(m%10)+"%"
					g.drawString(percent(m,n)
				                               ,x1,y0+16*i,GPC_RT);
					i++; break;
				case 17:
					g.drawString("　闇聴者へ", x0,y0+16*i,GPC_LT);
					n=0; m=0;
					for (j=0;j<4;j++) {
						n+=tmp.KyokuCount[j]; 
						m+=tmp.HojuYamiCount[j][0];
					}
					//if (n>0) m=(m*1000)/n;
					//g.drawString(Integer.toString(m/10)+"."+Integer.toString(m%10)+"%"
					g.drawString(percent(m,n)
				                               ,x1,y0+16*i,GPC_RT);
					i++; break;
				case 18:
					g.drawString("放銃率(立直後)", x0,y0+16*i,GPC_LT);
					n=0; m=0;
					for (j=0;j<4;j++) {
						n+=tmp.KyokuCount[j]; 
						m+=tmp.HojuReachCount[j][1]+tmp.HojuFuroCount[j][1]+tmp.HojuYamiCount[j][1]
						      -tmp.DblRonHojuCount[j][1];
					}
					//if (n>0) m=(m*1000)/n;
					//g.drawString(Integer.toString(m/10)+"."+Integer.toString(m%10)+"%"
					g.drawString(percent(m,n)
				                               ,x1,y0+16*i,GPC_RT);
					i++; break;
				case 19:
					g.drawString("副露率", x0,y0+16*i,GPC_LT);
					n=0; m=0;
					for (j=0;j<4;j++) {
						n+=tmp.KyokuCount[j]; m+=tmp.FuroCount[j];
					}
					//if (n>0) m=(m*1000)/n;
					//g.drawString(Integer.toString(m/10)+"."+Integer.toString(m%10)+"%"
					g.drawString(percent(m,n)
				                               ,x1,y0+16*i,GPC_RT);
					i++; break;
				case 20:
					g.drawString("立直率", x0,y0+16*i,GPC_LT);
					n=0; m=0;
					for (j=0;j<4;j++) {
						n+=tmp.KyokuCount[j]; m+=tmp.ReachCount[j];
					}
					//if (n>0) m=(m*1000)/n;
					//g.drawString(Integer.toString(m/10)+"."+Integer.toString(m%10)+"%"
					g.drawString(percent(m,n)
				                               ,x1,y0+16*i,GPC_RT);
					i++; break;
				case 21:
					g.drawString("立直成功率", x0,y0+16*i,GPC_LT);
					n=0; m=0;
					for (j=0;j<4;j++) {
						n+=tmp.ReachCount[j]; m+=tmp.ReachHoraCount[j];
					}
					//if (n>0) m=(m*1000)/n;
					//g.drawString(Integer.toString(m/10)+"."+Integer.toString(m%10)+"%"
					g.drawString(percent(m,n)
				                               ,x1,y0+16*i,GPC_RT);
					i++; break;
				case 22:
					g.drawString("平均和了点", x0,y0+16*i,GPC_LT);
					n=0; m=0;
					for (j=0;j<4;j++) {
						n+=tmp.HoraCount[j]; m+=tmp.HoraTotalScore[j];
					}
					if (n>0) m/=n;
					g.drawString(Integer.toString(m),x1,y0+16*i,GPC_RT);
					i++; break;
				case 23:
					g.drawString("平均放銃点", x0,y0+16*i,GPC_LT);
					n=0; m=0;
					for (j=0;j<4;j++) {
						n+=tmp.HojuReachCount[j][0]+tmp.HojuFuroCount[j][0]+tmp.HojuYamiCount[j][0]
						   +tmp.HojuReachCount[j][1]+tmp.HojuFuroCount[j][1]+tmp.HojuYamiCount[j][1]
						   -tmp.DblRonHojuCount[j][0]-tmp.DblRonHojuCount[j][1];
						m+=tmp.HojuTotalScore[j];
					}
					if (n>0) m/=n;
					g.drawString(Integer.toString(m),x1,y0+16*i,GPC_RT);
					i++; break;
				case 25:
					g.drawString("平均順位", x0,y0+16*i,GPC_LT);
					n=0; m=0;
					for (j=0;j<4;j++) {
						n+=tmp.RankCount[j]; m+=(j+1)*tmp.RankCount[j];
					}
					if (n>0) {
						m=(m*100)/n;
						m+=5;
						m/=10;
					}
					g.drawString(Integer.toString(m/10)+"."+Integer.toString(m%10)
					                               ,x1,y0+16*i,GPC_RT);
					i++; break;
				case 26:
				case 27:
				case 28:
				case 29:
					j=k-26;
					g.drawString(Integer.toString(j+1)+"位回数", x0,y0+16*i,GPC_LT);
					g.drawString(Integer.toString(tmp.RankCount[j]),x1,y0+16*i,GPC_RT);
					i++; break;
				case 30:
					g.drawString("飛び回数", x0,y0+16*i,GPC_LT);
					g.drawString(Integer.toString(tmp.TobiCount),x1,y0+16*i,GPC_RT);
					i++; break;

				case 32:
					g.drawString("総得点", x0,y0+16*i,GPC_LT);
					g.drawString(Integer.toString(tmp.TotalPoint),x1,y0+16*i,GPC_RT);
					i++; break;
				case 33:
					g.drawString("平均得点", x0,y0+16*i,GPC_LT);
					m=0; n=tmp.PlayCount-tmp.StopCount;
					if (n>0) m=(tmp.TotalPoint*10)/n;
					g.drawString(((m<0)?"-":"")
					                +Integer.toString(Math.abs(m)/10)+"."+Integer.toString(Math.abs(m)%10)
					                             ,x1,y0+16*i,GPC_RT);
					i++; break;
				case 34:
					g.drawString("最高得点", x0,y0+16*i,GPC_LT);
					if (tmp.MaxPoint<-999)
						g.drawString("-",x1,y0+16*i,GPC_RT);
					else
						g.drawString(Integer.toString(tmp.MaxPoint),x1,y0+16*i,GPC_RT);
					i++; break;
				case 35:
					g.drawString("最低得点", x0,y0+16*i,GPC_LT);
					if (tmp.MinPoint>999)
						g.drawString("-",x1,y0+16*i,GPC_RT);
					else
						g.drawString(Integer.toString(tmp.MinPoint),x1,y0+16*i,GPC_RT);
					i++; break;
				case 37:
					n=0;
					for (j=0;j<4;j++)
						n+=tmp.HoraCount[j];
					g.drawString("和了回数", x0,y0+16*i,GPC_LT);
					g.drawString(Integer.toString(n),x1,y0+16*i,GPC_RT);
					i++; break;
				case 38:
					n=0;
					for (j=0;j<4;j++)
						n+=tmp.HojuReachCount[j][0]+tmp.HojuFuroCount[j][0]+tmp.HojuYamiCount[j][0]
						   +tmp.HojuReachCount[j][1]+tmp.HojuFuroCount[j][1]+tmp.HojuYamiCount[j][1]
						   -tmp.DblRonHojuCount[j][0]-tmp.DblRonHojuCount[j][1];
					g.drawString("放銃回数(全)", x0,y0+16*i,GPC_LT);
					g.drawString(Integer.toString(n),x1,y0+16*i,GPC_RT);
					i++; break;
				case 39:
					n=0;
					for (j=0;j<4;j++)
						n+=tmp.HojuReachCount[j][0]+tmp.HojuFuroCount[j][0]+tmp.HojuYamiCount[j][0]
						    -tmp.DblRonHojuCount[j][0];
					g.drawString("放銃回数", x0,y0+16*i,GPC_LT);
					g.drawString(Integer.toString(n),x1,y0+16*i,GPC_RT);
					i++; break;
				case 40:
					n=0;
					for (j=0;j<4;j++)
						n+=tmp.HojuReachCount[j][0];
					g.drawString("　立直者へ", x0,y0+16*i,GPC_LT);
					g.drawString(Integer.toString(n),x1,y0+16*i,GPC_RT);
					i++; break;
				case 41:
					n=0;
					for (j=0;j<4;j++)
						n+=tmp.HojuFuroCount[j][0];
					g.drawString("　副露者へ", x0,y0+16*i,GPC_LT);
					g.drawString(Integer.toString(n),x1,y0+16*i,GPC_RT);
					i++; break;
				case 42:
					n=0;
					for (j=0;j<4;j++)
						n+=tmp.HojuYamiCount[j][0];
					g.drawString("　闇聴者へ", x0,y0+16*i,GPC_LT);
					g.drawString(Integer.toString(n),x1,y0+16*i,GPC_RT);
					i++; break;
				case 43:
					n=0;
					for (j=0;j<4;j++)
						n+=tmp.HojuReachCount[j][1]+tmp.HojuFuroCount[j][1]+tmp.HojuYamiCount[j][1]
						    -tmp.DblRonHojuCount[j][1];
					g.drawString("放銃回数(立直後)", x0,y0+16*i,GPC_LT);
					g.drawString(Integer.toString(n),x1,y0+16*i,GPC_RT);
					i++; break;
				case 44:
					n=0;
					for (j=0;j<4;j++)
						n+=tmp.HojuReachCount[j][1];
					g.drawString("　立直者へ", x0,y0+16*i,GPC_LT);
					g.drawString(Integer.toString(n),x1,y0+16*i,GPC_RT);
					i++; break;
				case 45:
					n=0;
					for (j=0;j<4;j++)
						n+=tmp.HojuFuroCount[j][1];
					g.drawString("　副露者へ", x0,y0+16*i,GPC_LT);
					g.drawString(Integer.toString(n),x1,y0+16*i,GPC_RT);
					i++; break;
				case 46:
					n=0;
					for (j=0;j<4;j++)
						n+=tmp.HojuYamiCount[j][1];
					g.drawString("　闇聴者へ", x0,y0+16*i,GPC_LT);
					g.drawString(Integer.toString(n),x1,y0+16*i,GPC_RT);
					i++; break;
				case 47:
					n=0;
					for (j=0;j<4;j++)
						n+=tmp.DblRonHojuCount[j][0]+tmp.DblRonHojuCount[j][1];
					g.drawString("ダブロン放銃", x0,y0+16*i,GPC_LT);
					g.drawString(Integer.toString(n),x1,y0+16*i,GPC_RT);
					i++; break;
				case 49:
					n=0;
					for (j=0;j<4;j++)
						n+=tmp.FuroCount[j];
					g.drawString("副露回数", x0,y0+16*i,GPC_LT);
					g.drawString(Integer.toString(n),x1,y0+16*i,GPC_RT);
					i++; break;
				case 50:
					n=0;
					for (j=0;j<4;j++)
						n+=tmp.ReachCount[j];
					g.drawString("立直回数", x0,y0+16*i,GPC_LT);
					g.drawString(Integer.toString(n),x1,y0+16*i,GPC_RT);
					i++; break;
				case 51:
					n=0;
					for (j=0;j<4;j++)
						n+=tmp.ReachHoraCount[j];
					g.drawString("立直成功回数", x0,y0+16*i,GPC_LT);
					g.drawString(Integer.toString(n),x1,y0+16*i,GPC_RT);
					i++; break;
				case 52:
					n=0;
					for (j=0;j<4;j++)
						n+=tmp.HoraTotalScore[j];
					g.drawString("総和了点", x0,y0+16*i,GPC_LT);
					g.drawString(Integer.toString(n),x1,y0+16*i,GPC_RT);
					i++; break;
				case 53:
					n=0;
					for (j=0;j<4;j++)
						n+=tmp.HojuTotalScore[j];
					g.drawString("総放銃点", x0,y0+16*i,GPC_LT);
					g.drawString(Integer.toString(n),x1,y0+16*i,GPC_RT);
					i++; break;
				default:
					i++; break;
				}
				k++;
			} while (i<15);
			break;
		default:
			if (rc_dt_scroll<20) 
				g.drawString("▼",210,249,GPC_LT);
			if (rc_dt_scroll>0)
				g.drawString("▲",210,25,GPC_LT);
			t=rc_dt_type-1;
			i=0; k=rc_dt_scroll;
			g.drawString(KazeName(t)+"場", 120,y0+16*i,GPC_HT);
			i++;
			do {
				switch (k) {
				case 0:
					g.drawString("総局数", x0,y0+16*i,GPC_LT);
					n=tmp.KyokuCount[t]; if (p<0) n/=4;
					g.drawString(Integer.toString(n),x1,y0+16*i,GPC_RT);
					i++; break;
				case 2:
					g.drawString("和了率", x0,y0+16*i,GPC_LT);
					//n=tmp.KyokuCount[t]; m=tmp.HoraCount[t];
					//if (n>0) m=(m*1000)/n;
					//g.drawString(Integer.toString(m/10)+"."+Integer.toString(m%10)+"%"
					g.drawString(percent(tmp.HoraCount[t],tmp.KyokuCount[t])
				                               ,x1,y0+16*i,GPC_RT);
					i++; break;
				case 3:
					g.drawString("放銃率(全)", x0,y0+16*i,GPC_LT);
					//n=tmp.KyokuCount[t]; 
					m=tmp.HojuReachCount[t][0]+tmp.HojuFuroCount[t][0]+tmp.HojuYamiCount[t][0]
					   +tmp.HojuReachCount[t][1]+tmp.HojuFuroCount[t][1]+tmp.HojuYamiCount[t][1]
					   -tmp.DblRonHojuCount[t][0]-tmp.DblRonHojuCount[t][1];
					//if (n>0) m=(m*1000)/n;
					//g.drawString(Integer.toString(m/10)+"."+Integer.toString(m%10)+"%"
					g.drawString(percent(m,tmp.KyokuCount[t])
				                               ,x1,y0+16*i,GPC_RT);
					i++; break;
				case 4:
					g.drawString("放銃率", x0,y0+16*i,GPC_LT);
					//n=tmp.KyokuCount[t]; 
					m=tmp.HojuReachCount[t][0]+tmp.HojuFuroCount[t][0]+tmp.HojuYamiCount[t][0]
					   -tmp.DblRonHojuCount[t][0];
					//if (n>0) m=(m*1000)/n;
					//g.drawString(Integer.toString(m/10)+"."+Integer.toString(m%10)+"%"
					g.drawString(percent(m,tmp.KyokuCount[t])
				                               ,x1,y0+16*i,GPC_RT);
					i++; break;
				case 5:
					g.drawString("　立直者へ", x0,y0+16*i,GPC_LT);
					//n=tmp.KyokuCount[t]; m=tmp.HojuReachCount[t][0];
					//if (n>0) m=(m*1000)/n;
					//g.drawString(Integer.toString(m/10)+"."+Integer.toString(m%10)+"%"
					g.drawString(percent(tmp.HojuReachCount[t][0],tmp.KyokuCount[t])
				                               ,x1,y0+16*i,GPC_RT);
					i++; break;
				case 6:
					g.drawString("　副露者へ", x0,y0+16*i,GPC_LT);
					//n=tmp.KyokuCount[t]; m=tmp.HojuFuroCount[t][0];
					//if (n>0) m=(m*1000)/n;
					//g.drawString(Integer.toString(m/10)+"."+Integer.toString(m%10)+"%"
					g.drawString(percent(tmp.HojuFuroCount[t][0],tmp.KyokuCount[t])
				                               ,x1,y0+16*i,GPC_RT);
					i++; break;
				case 7:
					g.drawString("　闇聴者へ", x0,y0+16*i,GPC_LT);
					//n=tmp.KyokuCount[t]; m=tmp.HojuYamiCount[t][0];
					//if (n>0) m=(m*1000)/n;
					//g.drawString(Integer.toString(m/10)+"."+Integer.toString(m%10)+"%"
					g.drawString(percent(tmp.HojuYamiCount[t][0],tmp.KyokuCount[t])
				                               ,x1,y0+16*i,GPC_RT);
					i++; break;
				case 8:
					g.drawString("放銃率(立直後)", x0,y0+16*i,GPC_LT);
					//n=tmp.KyokuCount[t]; 
					m=tmp.HojuReachCount[t][1]+tmp.HojuFuroCount[t][1]+tmp.HojuYamiCount[t][1]
					    -tmp.DblRonHojuCount[t][1];
					//if (n>0) m=(m*1000)/n;
					//g.drawString(Integer.toString(m/10)+"."+Integer.toString(m%10)+"%"
					g.drawString(percent(m,tmp.KyokuCount[t])
				                               ,x1,y0+16*i,GPC_RT);
					i++; break;
				case 9:
					g.drawString("副露率", x0,y0+16*i,GPC_LT);
					//n=tmp.KyokuCount[t]; m=tmp.FuroCount[t];
					//if (n>0) m=(m*1000)/n;
					//g.drawString(Integer.toString(m/10)+"."+Integer.toString(m%10)+"%"
					g.drawString(percent(tmp.FuroCount[t],tmp.KyokuCount[t])
				                               ,x1,y0+16*i,GPC_RT);
					i++; break;
				case 10:
					g.drawString("立直率", x0,y0+16*i,GPC_LT);
					//n=tmp.KyokuCount[t]; m=tmp.ReachCount[t];
					//if (n>0) m=(m*1000)/n;
					//g.drawString(Integer.toString(m/10)+"."+Integer.toString(m%10)+"%"
					g.drawString(percent(tmp.ReachCount[t],tmp.KyokuCount[t])
				                               ,x1,y0+16*i,GPC_RT);
					i++; break;
				case 11:
					g.drawString("立直成功率", x0,y0+16*i,GPC_LT);
					//n=tmp.ReachCount[t]; m=tmp.ReachHoraCount[t];
					//if (n>0) m=(m*1000)/n;
					//g.drawString(Integer.toString(m/10)+"."+Integer.toString(m%10)+"%"
					g.drawString(percent(tmp.ReachHoraCount[t],tmp.ReachCount[t])
				                               ,x1,y0+16*i,GPC_RT);
					i++; break;
				case 12:
					g.drawString("平均和了点", x0,y0+16*i,GPC_LT);
					n=tmp.HoraCount[t]; m=tmp.HoraTotalScore[t];
					if (n>0) m/=n;
					g.drawString(Integer.toString(m),x1,y0+16*i,GPC_RT);
					i++; break;
				case 13:
					g.drawString("平均放銃点", x0,y0+16*i,GPC_LT);
					n=tmp.HojuReachCount[t][0]+tmp.HojuFuroCount[t][0]+tmp.HojuYamiCount[t][0]
					   +tmp.HojuReachCount[t][1]+tmp.HojuFuroCount[t][1]+tmp.HojuYamiCount[t][1]
					   -tmp.DblRonHojuCount[t][0]-tmp.DblRonHojuCount[t][1];
					m=tmp.HojuTotalScore[t];
					if (n>0) m/=n;
					g.drawString(Integer.toString(m),x1,y0+16*i,GPC_RT);
					i++; break;
				case 15:
					g.drawString("和了回数", x0,y0+16*i,GPC_LT);
					g.drawString(Integer.toString(tmp.HoraCount[t]),x1,y0+16*i,GPC_RT);
					i++; break;
				case 16:
					n=tmp.HojuReachCount[t][0]+tmp.HojuFuroCount[t][0]+tmp.HojuYamiCount[t][0]
					   +tmp.HojuReachCount[t][1]+tmp.HojuFuroCount[t][1]+tmp.HojuYamiCount[t][1]
					   -tmp.DblRonHojuCount[t][0]-tmp.DblRonHojuCount[t][1];
					g.drawString("放銃回数(全)", x0,y0+16*i,GPC_LT);
					g.drawString(Integer.toString(n),x1,y0+16*i,GPC_RT);
					i++; break;
				case 17:
					n=tmp.HojuReachCount[t][0]+tmp.HojuFuroCount[t][0]+tmp.HojuYamiCount[t][0]
					   -tmp.DblRonHojuCount[t][0];
					g.drawString("放銃回数", x0,y0+16*i,GPC_LT);
					g.drawString(Integer.toString(n),x1,y0+16*i,GPC_RT);
					i++; break;
				case 18:
					g.drawString("　立直者へ", x0,y0+16*i,GPC_LT);
					g.drawString(Integer.toString(tmp.HojuReachCount[t][0]),x1,y0+16*i,GPC_RT);
					i++; break;
				case 19:
					g.drawString("　副露者へ", x0,y0+16*i,GPC_LT);
					g.drawString(Integer.toString(tmp.HojuFuroCount[t][0]),x1,y0+16*i,GPC_RT);
					i++; break;
				case 20:
					g.drawString("　闇聴者へ", x0,y0+16*i,GPC_LT);
					g.drawString(Integer.toString(tmp.HojuYamiCount[t][0]),x1,y0+16*i,GPC_RT);
					i++; break;
				case 21:
					n=tmp.HojuReachCount[t][1]+tmp.HojuFuroCount[t][1]+tmp.HojuYamiCount[t][1]
					    -tmp.DblRonHojuCount[t][1];
					g.drawString("放銃回数(立直後)", x0,y0+16*i,GPC_LT);
					g.drawString(Integer.toString(n),x1,y0+16*i,GPC_RT);
					i++; break;
				case 22:
					g.drawString("　立直者へ", x0,y0+16*i,GPC_LT);
					g.drawString(Integer.toString(tmp.HojuReachCount[t][1]),x1,y0+16*i,GPC_RT);
					i++; break;
				case 23:
					g.drawString("　副露者へ", x0,y0+16*i,GPC_LT);
					g.drawString(Integer.toString(tmp.HojuFuroCount[t][1]),x1,y0+16*i,GPC_RT);
					i++; break;
				case 24:
					g.drawString("　闇聴者へ", x0,y0+16*i,GPC_LT);
					g.drawString(Integer.toString(tmp.HojuYamiCount[t][1]),x1,y0+16*i,GPC_RT);
					i++; break;
				case 25:
					n=tmp.DblRonHojuCount[t][0]+tmp.DblRonHojuCount[t][1];
					g.drawString("ダブロン放銃", x0,y0+16*i,GPC_LT);
					g.drawString(Integer.toString(n),x1,y0+16*i,GPC_RT);
					i++; break;
				case 27:
					g.drawString("副露回数", x0,y0+16*i,GPC_LT);
					g.drawString(Integer.toString(tmp.FuroCount[t]),x1,y0+16*i,GPC_RT);
					i++; break;
				case 28:
					g.drawString("立直回数", x0,y0+16*i,GPC_LT);
					g.drawString(Integer.toString(tmp.ReachCount[t]),x1,y0+16*i,GPC_RT);
					i++; break;
				case 29:
					g.drawString("立直成功回数", x0,y0+16*i,GPC_LT);
					g.drawString(Integer.toString(tmp.ReachHoraCount[t]),x1,y0+16*i,GPC_RT);
					i++; break;
				case 30:
					g.drawString("総和了点", x0,y0+16*i,GPC_LT);
					g.drawString(Integer.toString(tmp.HoraTotalScore[t]),x1,y0+16*i,GPC_RT);
					i++; break;
				case 31:
					g.drawString("総放銃点", x0,y0+16*i,GPC_LT);
					g.drawString(Integer.toString(tmp.HojuTotalScore[t]),x1,y0+16*i,GPC_RT);
					i++; break;
				default:
					i++; break;
				}
				k++;
			} while (i<15);
			break;
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	
	private String getDate(long ms) {
		String s=(new Date(ms)).toString();
		int n=s.length();
		return s.substring(n-4,n)+" "+s.substring(4,10);
	}

	private void drawRc_reki() {
		int i,p;
		int x1=35;
		int x2=160;
		int x3=205;
		int y0=40;
		String s1;
		
		g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_MEDIUM));
		g.setColor(255,255,255);

		g.drawString("戦歴",120,5,GPC_HT);
		
		p=senrekiCount-rc_rk_scroll-1;
		
		if (p<0) {
		
			g.drawString("データなし",120,120,GPC_HT);
		
		} else {

			if (p>0)
				g.drawString("＞",235,126,GPC_RT);
			if (p<senrekiCount-1)
				g.drawString("＜",5,126,GPC_LT);
	
	
			g.drawString(getDate(senrekiDate[p]),120,y0,GPC_HT);
			
			y0+=20;
			
			g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_LARGE));
	
			int rule=senrekiRule[p];
			int pl=rule&3;
			boolean  tb=((rule&(1<<3))>0),
			         kui=((rule&(1<<4))>0),
			         ct=((rule&(1<<5))>0),
			         sd=((rule&(1<<6))>0);
			switch (pl) {
			case 0: s1="東風戦(南入"; 
			        if (ct) {
			        	s1=s1+"有";
			        	if (sd)
			        		s1=s1+"SD";
			        } else {
			        	s1=s1+"無";
			        }        
		        	s1=s1+")";
			        break;
			case 1: s1="半荘戦(西入"; 
			        if (ct) {
			        	s1=s1+"有";
			        	if (sd)
			        		s1=s1+"SD";
			        } else {
			        	s1=s1+"無";
			        }
			        s1=s1+")";
			        break;
			case 2: s1="一荘戦"; break;
			default: s1="？？？"; break;
			}
			g.drawString(s1,120,y0,GPC_HT);
	
			y0+=22;
			g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_MEDIUM));
			
			s1="飛び";
			if (tb) s1=s1+"有"; else s1=s1+"無";
			s1=s1+" 喰い断";
			if (kui) s1=s1+"有"; else s1=s1+"無";			
			g.drawString(s1, 120,y0,GPC_HT);
	
			y0+=35;
	
			g.drawString("名前",x1,y0,GPC_LT);
			g.drawString("順位",x2,y0,GPC_RT);
			g.drawString("得点",x3,y0,GPC_RT);
	
			y0+=18;
			g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_LARGE));
			
			for (i=0;i<4;i++) {
				g.drawString(PlayerName(senreki[p][i*3]),x1,y0+i*22,GPC_LT);
				g.drawString(Integer.toString(senreki[p][i*3+1]),x2,y0+i*22,GPC_RT);
				g.drawString(Integer.toString(senreki[p][i*3+2]),x3,y0+i*22,GPC_RT);
			}
		
		}
		
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	
	private void drawSettingUL(int n1, int n2) {
		if (n1==n2) {
			g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_UNDERLINED,Font.SIZE_MEDIUM));
			g.setColor(255,255,0);
		} else if (n1-n2==1) {
			g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_MEDIUM));
			g.setColor(255,255,255);
		}
	}
	private void drawSetting() {
		g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_MEDIUM));
		g.setColor(255,255,255);
		int i=0,j,k=0;
		int x0=30, x1=210, x2=22;
		int y0=5;
		String s1,s2;
		
		g.drawString("プレーヤー",x2,y0+i*16,GPC_LT);
		i++;
		for (j=0;j<4;j++) {
			drawSettingUL(k,selSetting); k++;
			g.drawString(Integer.toString(j+1)+"P",x0,y0+i*16,GPC_LT);
			g.drawString(PlayerName(think[j][0]),x1,y0+i*16,GPC_RT);
			i++;
		}
		
		y0+=8;
		
		if (cpuopen) s1="表示"; else s1="非表示";
		drawSettingUL(k,selSetting); k++;
		g.drawString("CPUの手牌",x2,y0+i*16,GPC_LT);
		g.drawString(s1,x1,y0+i*16,GPC_RT);
		i++;
		
		y0+=8;

		g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_MEDIUM));
		g.setColor(255,255,255);
		g.drawString("ルール",x2,y0+i*16,GPC_LT);
		i++;
		switch (rule_PlayLong) {
		case 0:  s1="東風戦"; s2="南入"; break;
		case 1:  s1="半荘戦"; s2="西入"; break;
		case 2:  s1="一荘戦"; s2="−−"; break;
		default: s1="？？戦"; s2="？？"; break;
		}
		drawSettingUL(k,selSetting); k++;
		g.drawString("ゲームの長さ",x0,y0+i*16,GPC_LT);
		g.drawString(s1,x1,y0+i*16,GPC_RT);
		i++;
		drawSettingUL(k,selSetting); k++;
		g.drawString(s2,x0+8,y0+i*16,GPC_LT);
		switch (rule_PlayLong) {
		case 2:  s1="−−"; break;
		default: if (rule_Continue)   {
		         	s1="あり"; 
		         	if (rule_SuddenDeath) s2="あり"; else s2="なし";
		         } else {
		         	s1="なし";
		         	s2="−−";
		         }
		         break;
		}
		g.drawString(s1,x1,y0+i*16,GPC_RT);
		i++;
		drawSettingUL(k,selSetting); k++;
		g.drawString("サドンデス",x0+8,y0+i*16,GPC_LT);
		g.drawString(s2,x1,y0+i*16,GPC_RT);
		i++;
		if (rule_tobi) s1="あり"; else s1="なし";
		drawSettingUL(k,selSetting); k++;
		g.drawString("飛び",x0,y0+i*16,GPC_LT);
		g.drawString(s1,x1,y0+i*16,GPC_RT);
		i++;
		if (rule_kuitan) s1="あり"; else s1="なし";
		drawSettingUL(k,selSetting); k++;
		g.drawString("喰い断",x0,y0+i*16,GPC_LT);
		g.drawString(s1,x1,y0+i*16,GPC_RT);
		i++;
		
		y0+=8;
		
		int c=0;
		for (j=0;j<4;j++) {
			if (think[j][0]<0) { c=-10; break; }
			if (think[j][0]>0) c++;
		}
		
		if ((c<3)||((c!=4)&&(cpuopen))) {
			s1="不可";
		} else {
			if (recording) s1="する"; else s1="しない";
		}
		drawSettingUL(k,selSetting); k++;
		g.drawString("データの記録",x2,y0+i*16,GPC_LT);
		g.drawString(s1,x1,y0+i*16,GPC_RT);
		i++;
		drawSettingUL(k,selSetting); k++;
		g.drawString("データの全消去",x2,y0+i*16,GPC_LT);
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//牌
	private void drawPai(int n, int x, int y) {
		sprite.setPosition(x,y);
		sprite.setFrame(n);
		sprite.paint(g);
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	
	//手牌
	private void drawTehai(int p, int x, int y) {
		int i,n, pp, t;
		pp=1<<p;
		n=tehai[p][13];
		int[] tmp=new int[13];
		int   f=0;
		boolean flag=((paiopen&pp)==0);
		for (i=0;i<n;i++) {
			t=tehai[p][i];
			if (flag) { //手牌非表示
				tmp[i]=34;
				if (t==44) f=i+1;
			} else { //手牌表示
				tmp[i]=t;
			}
		}
		if (f>0) { //手牌非表示、手牌切り
			i=(baKaze*3+p*7+think[p][0]+f*7)%n;
			tmp[i]=44;
		}
		for (i=0;i<n;i++)
			drawPai(tmp[i],x+i*PAI_WIDTH,y);
		/*
		for (i=0;i<n;i++) {
			t=tehai[p][i];
			if (((paiopen & pp)==0)&&(t!=44))
				t=34;
			
			drawPai(t,x+i*PAI_WIDTH,y);
		} //*/
	}
	
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//ツモ牌
	private void drawTumo(int pai) {
		int t,pp;
		pp=1<<nowPlayer;
		if ((paiopen & pp)==0)
			t=34;
		else
			t=pai;
		int x=tehai[nowPlayer][13]*PAI_WIDTH;
		drawPai(t,14+x,13+nowPlayer*60+PAI_HEIGHT*2);
		//drawPai(t,14+x,10+nowPlayer*60); //変更前 @位置
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//捨て牌（河）
	private void drawSutehai(int p, int x, int y) {
		int i,n, x1,y1;
		n=sutehai[p][0][0];
		for (i=0;i<n;i++) {
			x1=x+(i%15)*PAI_WIDTH;
			y1=y+(i/15)*PAI_HEIGHT;
			drawPai(sutehai[p][i+1][0], x1, y1);
			if (sutehai[p][i+1][1]>0) {
				if ((sutehai[p][i+1][1]&1)>0) { //副露された牌
					g.setColor(192,0,128);
					g.drawLine(x1,y1,x1+PAI_WIDTH-2,y1+PAI_HEIGHT-2);
					g.drawLine(x1+PAI_WIDTH-2,y1,x1,y1+PAI_HEIGHT-2);
				}
				if ((sutehai[p][i+1][1]&2)>0) { //リーチした牌
					g.setColor(192,192,0);
				g.drawRect(x1+1, y1+1, PAI_WIDTH-3, PAI_HEIGHT-3);
				}
				g.drawRect(x1, y1, PAI_WIDTH-1, PAI_HEIGHT-1);
			}
		}
	}
	
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//打牌強調
	private void spotDahai(int p,int x, int y) {
		int n=sutehai[p][0][0]-1;
		g.setColor(0,255,255);
		g.drawRect(x+(n%15)*PAI_WIDTH,y+(n/15)*PAI_HEIGHT,PAI_WIDTH-1,PAI_HEIGHT-1);
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//ツモ者コマンド
	private void drawTCommand(int n, int x, int y) {
		String[] s={"打牌","リーチ","暗カン","加カン","流局","ツモ"};
		g.setColor(255,255,255);
		g.fillRect(x,y,40,15);
		g.setColor(0,255,0);
		g.drawRect(x,y,40,15);
		g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_SMALL));
		g.setColor(0,0,0);
		g.drawString(s[n],x+20,y+2,GPC_HT);
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//副露コマンド
	private void drawFCommand(int n, int x, int y) {
		String[] s={"パス","チー","ポン","カン","ロン"};
		g.setColor(255,255,255);
		g.fillRect(x,y,40,15);
		g.setColor(0,255,0);
		g.drawRect(x,y,40,15);
		g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_SMALL));
		g.setColor(0,0,0);
		g.drawString(s[n],x+20,y+2,GPC_HT);
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//副露牌
	private void drawFuro(int p, int dx, int y) {
		int i,j,n,d,x;
		n=furopai[p][0][0];
		//y=10+p*60;
		for (i=1;i<=n;i++) {
			x=dx-(3*PAI_WIDTH+1)*i;
			switch (furopai[p][i][0]) {
			case FURO_CHI: //チー
				for (j=0;j<3;j++) {
					drawPai(furopai[p][i][1]+j,x+j*PAI_WIDTH,y);
				}
				break;
			case FURO_PON: //ポン
				for (j=0;j<3;j++) {
					drawPai(furopai[p][i][1],x+j*PAI_WIDTH,y);
				}
				break;
			case FURO_MINKAN: //明カン
				d=2*PAI_WIDTH/3;
				for (j=3;j>=0;j--) {
					drawPai(furopai[p][i][1],x+j*d,y);
				}
				break;
			case FURO_ANKAN: //暗カン
				d=2*PAI_WIDTH/3;
				for (j=3;j>=0;j--) {
					if ((j<1)||(j>2))
						drawPai(furopai[p][i][1],x+j*d,y);
					else
						drawPai(34,x+j*d,y);
				}
			}
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//コール
	private void drawCall(int n, int x, int y) {
		String[] s={"ツモ","チー","ポン","カン","ロン","リーチ"};
		g.setColor(255,255,255);
		g.fillRect(x,y,40,15);
		g.setColor(255,0,0);
		g.drawRect(x,y,40,15);
		g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_SMALL));
		g.setColor(0,0,0);
		g.drawString(s[n],x+20,y+2,GPC_HT);
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//牌選択カーソル
	private void drawSelectPai(int n, int p) {
		int y=14+p*60+PAI_HEIGHT*3;
		g.setColor(0,255,255);
		if (n<0) {
			int x=tehai[p][13]*PAI_WIDTH;
			g.drawLine(15+x,y,14+x+PAI_WIDTH-2,y);
			//g.drawLine(15+x,PAI_HEIGHT+11+p*60,
			//            14+x+PAI_WIDTH-2,PAI_HEIGHT+11+p*60); //変更前 @位置
		} else {
			g.drawLine(13+n*PAI_WIDTH,y,12+(n+1)*PAI_WIDTH-2,y);
			//g.drawLine(13+n*PAI_WIDTH,PAI_HEIGHT+11+p*60,
			//            12+(n+1)*PAI_WIDTH-2,PAI_HEIGHT+11+p*60); //変更前 @位置
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//情報表示
	private void drawInfo() {
		int y=256,n;
		n=ryukyokuIndex-tumoIndex-1;
		if (n<0) n=0;
		g.setColor(255,255,255);
		g.fillRect(0,y,240,12);
		g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_SMALL));
		g.setColor(0,0,0);
		g.drawString(KazeName(baKaze)+Integer.toString(kyoku+1)+"局 "
		             +Integer.toString(honba)+"本場 供託"+Integer.toString(kyotaku)
		             +" 残り"+Integer.toString(n)
		                      ,0,y,GPC_LT);
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//特殊流局
	private void drawRyokyokuSp(int tp, int x, int y) {
		g.setColor(255,255,255);
		g.fillRect(x,y,80,20);
		g.setColor(0,0,255);
		g.drawRect(x,y,80,20);
		String s;
		s=RyukyokuName(tp);
		/*
		switch (tp) {
		case RKK_4FON:
			s="四風連打";
			break;
		case RKK_9SHU:
			s="九種九牌";
			break;
		case RKK_NAGASHI:
			s="流し満貫";
			break;
		case RKK_KOHAI:
			s="荒牌流局";
			break;
		case RKK_4REACH:
			s="四家立直";
			break;
		case RKK_3RON:
			s="三家和";
			break;
		case RKK_4KAIKAN:
			s="四開槓";
			break;
		case RKK_FINISH:
			s="終局";
			break;
		default:
			s="途中流局";
			break;
		}
		*/
		g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_MEDIUM));
		g.setColor(0,0,0);
		g.drawString(s,x+40,y+2,GPC_HT);
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//流局
	private void drawRyokyoku(int p, int x, int y) {
		g.setColor(255,255,255);
		g.fillRect(x,y,100,15);
		g.setColor(0,0,255);
		g.drawRect(x,y,100,15);
		String s;
		if (tenpai[p])
			s="テンパイ";
		else
			s="ノーテン";
		g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_SMALL));
		g.setColor(0,0,0);
		g.drawString(s,x+5,y+2,GPC_LT);
		s=Integer.toString(addscore[p]);
		if (addscore[p]<0)
			g.setColor(255,0,0);
		if (addscore[p]>0) {
			g.setColor(0,0,255);
			s="+"+s;
		}
		g.drawString(s,x+95,y+2,GPC_RT);
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//流し満貫
	private void drawNagashi(int p, int x, int y) {
		g.setColor(255,255,255);
		g.fillRect(x,y,100,15);
		g.setColor(0,0,255);
		g.drawRect(x,y,100,15);
		String s;
		if ((nagashi&(1<<p))>0)
			s="流し満貫";
		else
			s="";
		g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_SMALL));
		g.setColor(0,0,0);
		g.drawString(s,x+5,y+2,GPC_LT);
		s=Integer.toString(addscore[p]);
		if (addscore[p]<0)
			g.setColor(255,0,0);
		if (addscore[p]>0) {
			g.setColor(0,0,255);
			s="+"+s;
		}
		g.drawString(s,x+95,y+2,GPC_RT);
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//役名表示
	private void drawYaku() {
		String  s1,s2;
		int     p=agariPlayer[agariPlayer[0]];
		int     n=tehai[p][13];
		int     i;

		g.setColor(255,255,255);
		g.fillRect(10,10,220,220);
		g.setColor(0,0,255);
		g.drawRect(10,10,220,220);
		
		drawTehai(p,15,15);
		drawFuro(p,225,15);
		drawPai(agaripai,17+n*PAI_WIDTH,15);
		
		g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_MEDIUM));
		g.setColor(0,0,0);
		
		for (i=0;i<yaku[0][0];i++) {
			s1=YakuName(yaku[i+1][0]);
			g.drawString(s1,30,20+PAI_HEIGHT+i*16,GPC_LT);
			if (yaku[i+1][0]<100) {
				s2=Integer.toString(yaku[i+1][1]);
				g.drawString(s2,210,20+PAI_HEIGHT+i*16,GPC_RT);
			}
		}
		
		if (yk_yakuman>0) {
			s1="役満";
			switch (yk_yakuman) {
			case 2:
				s1="ダブル"+s1;
				break;
			case 3:
				s1="トリプル"+s1;
				break;
			case 4:
				s1="四倍"+s1;
				break;
			case 5:
				s1="五倍"+s1;
				break;
			case 6:
				s1="六倍"+s1;
				break;
			}
		} else {
			s1=Integer.toString(yk_fu)+"符 "+Integer.toString(yk_han)+"翻";
			switch (yk_han) {
			case 1:
			case 2:
			case 3:
			case 4:
				if ((p==oya)&&(yk_score==12000)) {
					s1=s1+" 満貫";
				}
				if ((p!=oya)&&(yk_score==8000)) {
					s1=s1+" 満貫";
				}
				break;
			case 5: //満貫
				s1=s1+" 満貫";
				break;
			case 6: //ハネ満
			case 7:
				s1=s1+" 跳満";
				break;
			case 8: //倍満
			case 9:
			case 10:
				s1=s1+" 倍満";
				break;
			case 11: //３倍満
			case 12:
				s1=s1+" 三倍満";
				break;
			default: //数え役満
				s1=s1+" 数え役満";
				break;
			}
		}
		g.drawString(s1,30,205,GPC_LT);
		g.drawString(Integer.toString(yk_score),210,205,GPC_RT);
		
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//スコア表示
	private void drawAddScore(int p, int x, int y) {
		if (addscore[p]==0) return;
		g.setColor(255,255,255);
		g.fillRect(x,y,80,15);
		g.setColor(0,0,255);
		g.drawRect(x,y,80,15);
		g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_SMALL));
		String s;
		s=Integer.toString(addscore[p]);
		if (addscore[p]<0)
			g.setColor(255,0,0);
		if (addscore[p]>0) {
			g.setColor(0,0,255);
			s="+"+s;
		}
		g.drawString(s,x+75,y+2,GPC_RT);
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//ランキング表示
	private void drawRank(int p, int x, int y) {
		g.setColor(255,255,255);
		g.fillRect(x,y,80,15);
		g.setColor(0,0,255);
		g.drawRect(x,y,80,15);
		g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_SMALL));
		String s;
		g.setColor(0,0,0);
		if (rank[p][0]==1)
			g.setColor(0,0,255);
		s=Integer.toString(rank[p][0])+" 位";
		g.drawString(s,x+10,y+2,GPC_LT);
		s=Integer.toString(rank[p][1]);
		g.setColor(0,0,0);
		if (rank[p][1]<0)
			g.setColor(255,0,0);
		if (rank[p][1]>0) {
			g.setColor(0,0,255);
			s="+"+s;
		}
		g.drawString(s,x+75,y+2,GPC_RT);
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//プレーヤー名表示
	private void drawPlayerName(int p, int x, int y) {
		g.setColor(255,255,255);
		g.fillRect(x,y,100,15);
		g.setColor(0,0,255);
		g.drawRect(x,y,100,15);
		g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_SMALL));
		int cp=think[p][0];
		if (cp==0) {
			g.setColor(0,0,255);
		} else if (cp<0) {
			g.setColor(0,0,0);
		} else {
			g.setColor(255,0,0);
		}
		g.drawString(PlayerName(cp),x+10,y+2,GPC_LT);
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//王牌表示
	private void drawWanpai(int x, int y) {
		int pai;
		if (GameState==0) return;
		
		if (rinshanIndex<2) {
			drawPai(34,x,y);
		}
		if (rinshanIndex<4) {
			drawPai(34,x+PAI_WIDTH,y);
		}
		pai=yama[131]; if (GameState==1) pai=34;
		drawPai(pai,x+PAI_WIDTH*2,y);
		pai=34; if (rinshanIndex>0) pai=yama[129];
		drawPai(pai,x+PAI_WIDTH*3,y);
		pai=34; if (rinshanIndex>1) pai=yama[127];
		drawPai(pai,x+PAI_WIDTH*4,y);
		pai=34; if (rinshanIndex>2) pai=yama[125];
		drawPai(pai,x+PAI_WIDTH*5,y);
		pai=34; if (rinshanIndex>3) pai=yama[123];
		drawPai(pai,x+PAI_WIDTH*6,y);
		
		if (GameState==3) {
			drawPai(yama[130],x+PAI_WIDTH*2,y-PAI_HEIGHT);
			if (rinshanIndex>0)
				drawPai(yama[128],x+PAI_WIDTH*3,y-PAI_HEIGHT);
			if (rinshanIndex>1)
				drawPai(yama[126],x+PAI_WIDTH*4,y-PAI_HEIGHT);
			if (rinshanIndex>2)
				drawPai(yama[124],x+PAI_WIDTH*5,y-PAI_HEIGHT);
			if (rinshanIndex>3)
				drawPai(yama[122],x+PAI_WIDTH*6,y-PAI_HEIGHT);
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//メイン描写処理
	private void drawAll() {
		int i,p,n;
	
		//位置交換に関わる描写 @チェック @位置
		//懸念
		//  位置変更すると点数の表示位置が問題になる可能性がある
		//引数で位置指定（このメソッド内の引数を変えるだけでＯＫ）
		//  自風 点数 起家マーク カレントプレーヤー フリテンマーク(自風の横)
		//  手牌(drawTehai) 捨て牌(drawSutehai) 副露牌(drawFuro)
		//  打牌強調(spotDahai[捨て牌と同じ位置]、これのみ複数箇所で呼び出すので全て修正が必要)
		//メソッド内で位置指定（各メソッドの変更だけでＯＫ）
		//  ツモ牌(drawTumo) 牌選択(drawSelectPai)
		//文字枠(コール・副露選択・ツモ時選択・名前・ランキング得点・ﾃﾝﾊﾟｲﾌﾘﾃﾝ・流し満貫増減点・増減点)
		//   表示をやや下に移動したほうがいい @位置2
		//   drawCall drawTCommand drawFCommand drawPlayerName drawRank
		//   drawRyokyoku drawNagashi drawAddScore
		//位置(nは各所で異なる)
		//            元位置(Y座標)          変更位置(Y座標)
		// ﾘｰﾁ棒[5]   4 +n*60    [基準位置]  4 +n*60
		// 自風[12]   25+n*60                23+n*60
		// 点数[12]   38+n*60                10+n*60
		// 起家[9]    13+n*60                43+n*60
		// ｶﾚﾝﾄP[5]   15+n*60                45+n*60
		// ﾌﾘﾃﾝ[12]   25+n*60                23+n*60
		// 手牌       10+n*60                13+n*60+PAI_H*2
		// 副露       10+n*60                13+n*60+PAI_H*2
		// 捨牌       13+n*60+PAI_H          10+n*60
		// 打強**     13+n*60+PAI_H          10+n*60
		// ツモ*      10+n*60                13+n*60+PAI_H*2
		// 選択*      11+n*60+PAI_H          14+n*60+PAI_H*3 (13+n*60+PAI_H*2 + 1+PAI_H)
		// 文字枠[15] 20+n*60                26+n*60
	
		//境界線
		g.setColor(0,64,0);
		for (i=0;i<4;i++) {
			g.drawLine(0,(i+1)*60,240,(i+1)*60);
		}
	
		//自風
		for (i=0;i<4;i++) {
			g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_SMALL));
			g.setColor(255,255,255);
			g.drawString(KazeName(i),0,23+((oya+i)%4)*60,GPC_LT);
			//g.drawString(KazeName(i),0,25+((oya+i)%4)*60,GPC_LT); //変更前 @位置
		}
		
		//点数
		g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_SMALL));
		g.setColor(255,255,255);
		for (i=0;i<4;i++) {
			g.drawString(Integer.toString(score[i]),0,10+i*60,GPC_LT);
			//g.drawString(Integer.toString(score[i]),0,38+i*60,GPC_LT); //変更前 @位置
		}

		//起家マーク
		g.setColor(255,0,255);
		g.drawArc(0,43+kicha*60,9,9,0,360);
		//g.drawArc(0,13+kicha*60,9,9,0,360); //変更前 @位置
		
		//オーラスマーク
		if (alllast) {
			g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_SMALL));
			g.setColor(255,255,255);
			g.drawString("オーラス",0,244,GPC_LT);
		}
		
		//カレントプレーヤーマーク
		g.setColor(255,255,0);
		g.fillArc(2,45+nowPlayer*60,5,5,0,360);
		//g.fillArc(2,15+nowPlayer*60,5,5,0,360); //変更前 @位置

		//フリテンマーク
		for (i=0;i<4;i++) {
			if ((paiopen&(1<<i))>0)
				if (furiten[i]>0) {
					g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_SMALL));
					g.setColor(255,255,255);
					g.drawString("f",12,23+i*60,GPC_LT);
					//g.drawString("f",12,25+i*60,GPC_LT); //変更前 @位置
				}
		}

		//リーチ棒
		for (i=0;i<4;i++) {
			if (reach[i]) {
				g.drawImage(imgs[0],25,4+60*i,GPC_LT);
			}
		}

		//手牌
		for (i=0;i<4;i++)
			drawTehai(i,12,13+60*i+PAI_HEIGHT*2);
			//drawTehai(i,12,10+60*i); //変更前 @位置
		
		//副露牌
		for (i=0;i<4;i++)
			drawFuro(i,240,13+i*60+PAI_HEIGHT*2);
			//drawFuro(i,240,10+i*60); //変更前 @位置

		//捨て牌（河）
		for (i=0;i<4;i++)
			drawSutehai(i,40,10+60*i);
			//drawSutehai(i,40,13+PAI_HEIGHT+60*i); //変更前 @位置

		switch (GameMode) {
		case GM_NEWGAME:
		case GM_START:
			if ((haipaiCount<0)&&(baKaze==0)&&(kyoku==0)&&(honba==0)) {
				for (i=0;i<4;i++) {
					drawPlayerName(i,25,26+i*60);
					//drawPlayerName(i,25,20+i*60); //変更前 @位置2
				}
				if (recording) {
					g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_SMALL));
					g.setColor(255,0,0);
					g.drawString("REC",0,244,GPC_LT);
				}
			}
			break;
		case GM_TUMO:
			drawTumo(getSelectPai(nowPlayer,-1));
			/*
			if (rinshanhai) 
				drawTumo(yama[135-rinshanIndex]); //嶺上牌
			else
				drawTumo(yama[tumoIndex]);  //ツモ牌
			*/
			//コマンド
			if ((cpuplayer & (1<<nowPlayer))==0)
				drawTCommand(selectTCmd,195,26+nowPlayer*60);
				//drawTCommand(selectTCmd,195,20+nowPlayer*60); //変更前 @位置2
			//牌選択カーソル
			if ((cpuplayer & (1<<nowPlayer))==0)
				drawSelectPai(selectPai,nowPlayer);
			break;
		case GM_MINKAN1:
			if (GameMode==GM_MINKAN1)
				drawCall(CALL_KAN,195,26+callPlayer*60);
				//drawCall(CALL_KAN,195,20+callPlayer*60); //変更前 @位置2
		case GM_CHI1:
			if (GameMode==GM_CHI1)
				drawCall(CALL_CHI,195,26+callPlayer*60);
				//drawCall(CALL_CHI,195,20+callPlayer*60); //変更前 @位置2
		case GM_PON1:
			if (GameMode==GM_PON1)
				drawCall(CALL_PON,195,26+callPlayer*60);
				//drawCall(CALL_PON,195,20+callPlayer*60); //変更前 @位置2
		case GM_FUROCHECK:
		case GM_CALLCHECK:
			//コマンド
			if (furoselect)
				drawFCommand(selectFCmd,195,26+((nowPlayer+frCheck)%4)*60);
				//drawFCommand(selectFCmd,195,20+((nowPlayer+frCheck)%4)*60); //変更前 @位置2
			//打牌強調
			spotDahai(nowPlayer,40,10+60*nowPlayer);
			//spotDahai(nowPlayer,40,13+PAI_HEIGHT+60*nowPlayer); //変更前 @位置
		case GM_NEXT:
			//ツモ牌
			if ((selectPai>=0)&&(furodahai==false)) {
				drawTumo(getSelectPai(nowPlayer,-1));
				/*
				if (rinshanhai)
					drawTumo(yama[135-rinshanIndex]);
				else
					drawTumo(yama[tumoIndex]);
				*/
			}
			break;
		case GM_CHI2:
			if ((cpuplayer & (1<<nowPlayer))==0) {
				//牌選択カーソル
				drawSelectPai(selectChi1,nowPlayer);
				drawSelectPai(selectChi2,nowPlayer);
			}
			break;
		case GM_FURODAHAI:
			//牌選択カーソル
			if ((cpuplayer & (1<<nowPlayer))==0)
				drawSelectPai(selectPai,nowPlayer);
			break;
		case GM_ANKAN1:
		case GM_KAKAN1:
			drawTumo(getSelectPai(nowPlayer,-1));
			/*
			if (rinshanhai) 
				drawTumo(yama[135-rinshanIndex]); //嶺上牌
			else
				drawTumo(yama[tumoIndex]);  //ツモ牌
			*/
			//カンコール
			drawCall(CALL_KAN,195,26+nowPlayer*60);
			//drawCall(CALL_KAN,195,20+nowPlayer*60); //変更前 @位置2
			//牌選択カーソル
			if ((cpuplayer & (1<<nowPlayer))==0)
				drawSelectPai(selectPai,nowPlayer);
			break;
		case GM_RYUKYOKUSP:
			switch (rkk_type) {
			case RKK_4FON:
				//打牌強調
				spotDahai(nowPlayer,40,10+60*nowPlayer);
				//spotDahai(nowPlayer,40,13+PAI_HEIGHT+60*nowPlayer); //変更前 @位置
				break;
			case RKK_9SHU:
				drawTumo(yama[tumoIndex]);  //ツモ牌
				break;
			case RKK_3RON:
				//打牌強調
				spotDahai(nowPlayer,40,10+60*nowPlayer);
				//spotDahai(nowPlayer,40,13+PAI_HEIGHT+60*nowPlayer); //変更前 @位置
				for (i=0;i<4;i++)
					if ((RonCall&(1<<i))>0)
						drawCall(CALL_RON,195,26+i*60);
						//drawCall(CALL_RON,195,20+i*60); //変更前 @位置2
				break;
			}
			drawRyokyokuSp(rkk_type,80,108);
			break;
		case GM_RYUKYOKU1:
			drawRyokyokuSp(rkk_type,80,108);
			break;
		case GM_RYUKYOKU2:
			for (i=0;i<4;i++)
				drawRyokyoku(i,25,26+i*60);
				//drawRyokyoku(i,25,20+i*60); //変更前 @位置2
			break;
		case GM_NAGASHI:
			for (i=0;i<4;i++)
				drawNagashi(i,25,26+i*60);
				//drawNagashi(i,25,20+i*60); //変更前 @位置2
			break;
		case GM_REACH1:
			//ツモ牌
			drawTumo(getSelectPai(nowPlayer,-1));
			/*
			if (rinshanhai)
				drawTumo(yama[135-rinshanIndex]);
			else
				drawTumo(yama[tumoIndex]);
			*/
			drawCall(CALL_REACH,195,26+nowPlayer*60);
			//drawCall(CALL_REACH,195,20+nowPlayer*60); //変更前 @位置2
			break;
		case GM_CALLTUMO1:
			//ツモ牌
			drawTumo(getSelectPai(nowPlayer,-1));
			/*
			if (rinshanhai)
				drawTumo(yama[135-rinshanIndex]);
			else
				drawTumo(yama[tumoIndex]);
			*/
			drawCall(CALL_TUMO,195,26+nowPlayer*60);
			//drawCall(CALL_TUMO,195,20+nowPlayer*60); //変更前 @位置2
			break;
		case GM_CALLRON1:
			//打牌強調
			if ((furodahai==false)&&(selectPai>=0)) {
				//ツモ牌
				drawTumo(getSelectPai(nowPlayer,-1));
				/*
				if (rinshanhai)
					drawTumo(yama[135-rinshanIndex]);
				else
					drawTumo(yama[tumoIndex]);
				*/
			}
			spotDahai(nowPlayer,40,10+60*nowPlayer);
			//spotDahai(nowPlayer,40,13+PAI_HEIGHT+60*nowPlayer); //変更前 @位置
			for (i=0;i<4;i++)
				if ((RonCall&(1<<i))>0)
					drawCall(CALL_RON,195,26+i*60);
					//drawCall(CALL_RON,195,20+i*60); //変更前 @位置2
			break;
		case GM_AGARI1:
			//打牌強調
			if ((furodahai==false)&&(selectPai>=0)&&(agari_type==AG_RON)) {
				//ツモ牌
				drawTumo(getSelectPai(nowPlayer,-1));
				/*
				if (rinshanhai)
					drawTumo(yama[135-rinshanIndex]);
				else
					drawTumo(yama[tumoIndex]);
				*/
			}
			n=agariPlayer[0];
			p=agariPlayer[n];
			if (agari_type==AG_RON) {
				drawCall(CALL_RON,195,26+p*60);
				//drawCall(CALL_RON,195,20+p*60); //変更前 @位置2
				spotDahai(nowPlayer,40,10+60*nowPlayer);
				//spotDahai(nowPlayer,40,13+PAI_HEIGHT+60*nowPlayer); //変更前 @位置
			} else {
				drawTumo(getSelectPai(nowPlayer,-1));
				/*
				if (rinshanhai)
					drawTumo(yama[135-rinshanIndex]);
				else
					drawTumo(yama[tumoIndex]);
				*/
				drawCall(CALL_TUMO,195,26+p*60);
				//drawCall(CALL_TUMO,195,20+p*60); //変更前 @位置2
			}
			break;
		case GM_AGARI2:
			//打牌強調
			if ((furodahai==false)&&(selectPai>=0)&&(agari_type==AG_RON)) {
				//ツモ牌
				drawTumo(getSelectPai(nowPlayer,-1));
				/*
				if (rinshanhai)
					drawTumo(yama[135-rinshanIndex]);
				else
					drawTumo(yama[tumoIndex]);
				*/
			}
			if (agari_type==AG_TUMO) {
				drawTumo(getSelectPai(nowPlayer,-1));
				/*
				if (rinshanhai)
					drawTumo(yama[135-rinshanIndex]);
				else
					drawTumo(yama[tumoIndex]);
				*/
			} else {
				spotDahai(nowPlayer,40,10+60*nowPlayer);
				//spotDahai(nowPlayer,40,13+PAI_HEIGHT+60*nowPlayer); //変更前 @位置
			}
			drawYaku();
			break;
		case GM_AGARI3:
			//打牌強調
			if ((furodahai==false)&&(selectPai>=0)&&(agari_type==AG_RON)) {
				//ツモ牌
				drawTumo(getSelectPai(nowPlayer,-1));
				/*
				if (rinshanhai)
					drawTumo(yama[135-rinshanIndex]);
				else
					drawTumo(yama[tumoIndex]);
				*/
			}
			if (agari_type==AG_TUMO) {
				drawTumo(getSelectPai(nowPlayer,-1));
				/*
				if (rinshanhai)
					drawTumo(yama[135-rinshanIndex]);
				else
					drawTumo(yama[tumoIndex]);
				*/
			} else {
				spotDahai(nowPlayer,40,10+60*nowPlayer);
				//spotDahai(nowPlayer,40,13+PAI_HEIGHT+60*nowPlayer); //変更前 @位置
			}
			for (i=0;i<4;i++)
				drawAddScore(i,25,26+i*60);
				//drawAddScore(i,25,20+i*60); //変更前 @位置2
			break;
		case GM_FINISH2:
			for (i=0;i<4;i++) {
				drawPlayerName(i,25,26+i*60);
				//drawPlayerName(i,25,20+i*60); //変更前 @位置2
				drawRank(i,125,26+i*60);
				//drawRank(i,125,20+i*60); //変更前 @位置2
			}
			break;
		case GM_FINISH1:
			drawRyokyokuSp(RKK_FINISH,80,108);
			break;
		case GM_CHANKAN1:
			//コマンド
			if (furoselect)
				drawFCommand(selectFCmd,195,26+((nowPlayer+frCheck)%4)*60);
				//drawFCommand(selectFCmd,195,20+((nowPlayer+frCheck)%4)*60); //変更前 @位置2
			break;
		}
		
		drawInfo();
		
		drawWanpai(156,253);
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	
	//632byte
	class StcData {
		public int[][]  Yaku           =new int[28][2];
		public int[]    
		                RankCount      =new int[4],
		                Yakuman        =new int[15],
		                Yakumans       =new int[5],
		                Ryukyoku       =new int[6],
		                HanCount       =new int[13],
		                FuCount        =new int[12],
		                HojuReachCount =new int[2],
		                HojuFuroCount  =new int[2],
		                HojuYamiCount  =new int[2],
		                YmHojuCount    =new int[2],
		                DblRonHojuCount=new int[2],
		                KohaiRyukyoku  =new int[2];

		public int      
		                PlayCount      =0,
		                TonpuCount     =0,
		                HanchanCount   =0,
		                IchanCount     =0,
		                StopCount      =0,
		                KyokuCount     =0,

		                FuroCount      =0,
		                AnkanCount     =0,
		                ChiCount       =0,
		                PonCount       =0,
		                KakanCount     =0,
		                MinkanCount    =0,

		                ReachCount     =0,
		                TobiCount      =0,

		                TotalHan       =0,
		                MaxHan         =0,
		                TotalFu        =0,
		                MaxFu          =0,
		                DoraMax        =0,

		                TumoCount      =0,
		                RonCount       =0,
		                YmRonCount     =0,
		                YmTumoCount    =0,
		                
		                TumoTotalScore =0,
		                TumoMaxScore   =0,
		                RonTotalScore  =0,
		                RonMaxScore    =0,
		                HojuTotalScore =0,
		                HojuMaxScore   =0,
		                DblRonHojuTotalScore=0,
		                DblRonHojuMaxScore=0,
		                
		                RenchanCount   =0,
		                RenchanMax     =0;

		StcData() { //コンストラクタ
		}
		
		public boolean read(int n, byte[] data) {
			int i,j,n2=-1;
			int v;
			DataInputStream      dis=null;
			try {
				dis=new DataInputStream(new ByteArrayInputStream(data));
				
				n2=dis.readInt();    //順序用データ
				v=dis.readInt();  //バージョン
				
				PlayCount   =dis.readInt();
				TonpuCount  =dis.readInt();
				HanchanCount=dis.readInt();
				IchanCount  =dis.readInt();
				StopCount   =dis.readInt();
				KyokuCount  =dis.readInt();
				
				FuroCount   =dis.readInt();
				AnkanCount  =dis.readInt();
				ChiCount    =dis.readInt();
				PonCount    =dis.readInt();
				KakanCount  =dis.readInt();
				MinkanCount =dis.readInt();
				
				ReachCount  =dis.readInt();
				TobiCount   =dis.readInt();
				
				if (v>2) {
					if (v<5) {
						DblRonHojuCount[0]=dis.readInt();
					} else {
						for (j=0;j<2;j++)
							DblRonHojuCount[j]=dis.readInt();
					}
				}
				
				TotalHan    =dis.readInt();
				MaxHan      =dis.readInt();
				TotalFu     =dis.readInt();
				MaxFu       =dis.readInt();
				DoraMax     =dis.readInt();
				
				TumoCount      =dis.readInt();
				RonCount       =dis.readInt();
				if (v==1) {
					HojuReachCount[0] =dis.readInt();
					HojuFuroCount[0]  =dis.readInt();
					HojuYamiCount[0]  =dis.readInt();
					YmTumoCount       =dis.readInt();
					YmRonCount        =dis.readInt();
					YmHojuCount[0]    =dis.readInt();
				} else {
					for (j=0;j<2;j++) {
						HojuReachCount[j] =dis.readInt();
						HojuFuroCount[j]  =dis.readInt();
						HojuYamiCount[j]  =dis.readInt();
					}
					YmTumoCount           =dis.readInt();
					YmRonCount            =dis.readInt();
					for (j=0;j<2;j++)
						YmHojuCount[j]    =dis.readInt();
				}
				
				TumoTotalScore =dis.readInt();
				TumoMaxScore   =dis.readInt();
				RonTotalScore  =dis.readInt();
				RonMaxScore    =dis.readInt();
				HojuTotalScore =dis.readInt();
				HojuMaxScore   =dis.readInt();
				if (v>3) {
					DblRonHojuTotalScore =dis.readInt();
					DblRonHojuMaxScore   =dis.readInt();
				}
				
				for (j=0;j<2;j++)
					for (i=0;i<28;i++)
						Yaku[i][j]=dis.readInt();
				
				for (i=0;i<4;i++)
					RankCount[i]=dis.readInt();

				for (i=0;i<15;i++)
					Yakuman[i]=dis.readInt();
				for (i=0;i<5;i++)
					Yakumans[i]=dis.readInt();
				
				for (i=0;i<6;i++)
					Ryukyoku[i]=dis.readInt();
				
				for (i=0;i<13;i++)
					HanCount[i]=dis.readInt();
				
				for (i=0;i<12;i++)
					FuCount[i]=dis.readInt();
				
				if (v>5) {
					for (i=0;i<2;i++)
						KohaiRyukyoku[i]=dis.readInt();
				}
				
				if (v>6) {
					RenchanCount=dis.readInt();
					RenchanMax=dis.readInt();
				}
				
				dis.close();
			} catch (Exception e) {
				//System.out.println("[StcData.read]"+e.toString());
				if (dis!=null) {
					try { dis.close(); } catch (Exception e2) {}
				}
				dis=null;
				return false; //読み込み失敗
			}
			dis=null;
			if (n!=n2) return false;
			return true;
		}
		
		public byte[] toBytes(int n) {
			int i,j;
			int v=7; //バージョン
			ByteArrayOutputStream out=null;
			DataOutputStream      dos=null;
			byte[]                b=null;
			try {
				out=new ByteArrayOutputStream(4);
				dos=new DataOutputStream(out);
				
				dos.writeInt(n); //レコードストアでの順序を示す
				dos.writeInt(v);

				dos.writeInt(PlayCount);
				dos.writeInt(TonpuCount);
				dos.writeInt(HanchanCount);
				dos.writeInt(IchanCount);
				dos.writeInt(StopCount);
				dos.writeInt(KyokuCount);
				
				dos.writeInt(FuroCount);
				dos.writeInt(AnkanCount);
				dos.writeInt(ChiCount);
				dos.writeInt(PonCount);
				dos.writeInt(KakanCount);
				dos.writeInt(MinkanCount);
				
				dos.writeInt(ReachCount);
				dos.writeInt(TobiCount);
				for (j=0;j<2;j++)
					dos.writeInt(DblRonHojuCount[j]);
				
				dos.writeInt(TotalHan);
				dos.writeInt(MaxHan);
				dos.writeInt(TotalFu);
				dos.writeInt(MaxFu);
				dos.writeInt(DoraMax);
				
				dos.writeInt(TumoCount);
				dos.writeInt(RonCount);
				for (j=0;j<2;j++) {
					dos.writeInt(HojuReachCount[j]);
					dos.writeInt(HojuFuroCount[j]);
					dos.writeInt(HojuYamiCount[j]);
				}
				dos.writeInt(YmTumoCount);
				dos.writeInt(YmRonCount);
				for (j=0;j<2;j++)
					dos.writeInt(YmHojuCount[j]);
				
				dos.writeInt(TumoTotalScore);
				dos.writeInt(TumoMaxScore);
				dos.writeInt(RonTotalScore);
				dos.writeInt(RonMaxScore);
				dos.writeInt(HojuTotalScore);
				dos.writeInt(HojuMaxScore);
				dos.writeInt(DblRonHojuTotalScore);
				dos.writeInt(DblRonHojuMaxScore);
				
				for (j=0;j<2;j++)
					for (i=0;i<28;i++)
						dos.writeInt(Yaku[i][j]);
				
				for (i=0;i<4;i++)
					dos.writeInt(RankCount[i]);

				for (i=0;i<15;i++)
					dos.writeInt(Yakuman[i]);
				for (i=0;i<5;i++)
					dos.writeInt(Yakumans[i]);
				
				for (i=0;i<6;i++)
					dos.writeInt(Ryukyoku[i]);
				
				for (i=0;i<13;i++)
					dos.writeInt(HanCount[i]);
				
				for (i=0;i<12;i++)
					dos.writeInt(FuCount[i]);
				
				for (i=0;i<2;i++)
					dos.writeInt(KohaiRyukyoku[i]);
					
				dos.writeInt(RenchanCount);
				dos.writeInt(RenchanMax);
				
				b=out.toByteArray();
				dos.close();
			} catch (Exception e) {
				//System.out.println("[StcData.toBytes]"+e.toString());
				if (dos!=null) {
					try { dos.close(); } catch (Exception e2) {
						if (out!=null) {
							try { out.close(); } catch (Exception e3) {}
						}
					}
					out=null;
				}
				if (out!=null) {
					try { out.close(); } catch (Exception e2) {}
				}
				b=null;
			}
			dos=null; out=null;
			return b;
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//296byte
	class SensekiData {
		public int     PlayCount      = 0,
		               ContinueCount  = 0,
		               StopCount      = 0,
		               TobiCount      = 0,
		               TotalPoint     = 0,
		               MaxPoint       = -1000,
		               MinPoint       = 1000;

		public int[]   RankCount      = new int[4],
		               KyokuCount     = new int[4],
		               HoraCount      = new int[4],
		               ReachCount     = new int[4],
		               ReachHoraCount = new int[4],
		               FuroCount      = new int[4],
		               HoraTotalScore = new int[4],
		               HojuTotalScore = new int[4];
		public int[][] HojuReachCount = new int[4][2],
		               HojuFuroCount  = new int[4][2],
		               HojuYamiCount  = new int[4][2],
		               DblRonHojuCount= new int[4][2];

	
		SensekiData() {
		}

		public boolean read(int t, int n, byte[] data) {
			int i,j,n2=-1, tp=-1;
			int v;
			DataInputStream      dis=null;
			try {
				dis=new DataInputStream(new ByteArrayInputStream(data));
				tp=dis.readInt(); //ルールタイプ
				n2=dis.readInt(); //順序用データ
				v=dis.readInt();  //バージョン

				PlayCount=dis.readInt();
				ContinueCount=dis.readInt();
				StopCount=dis.readInt();
				TobiCount=dis.readInt();
				
				TotalPoint=dis.readInt();
				MaxPoint=dis.readInt();
				MinPoint=dis.readInt();
				
				for (i=0;i<4;i++) 
					RankCount[i]=dis.readInt();

				if (v<3) { //古いバージョンのに順位ウマを乗せる
					TotalPoint+=RankCount[0]*20+RankCount[1]*10
					             -RankCount[2]*10-RankCount[3]*20;
					for (i=0;i<4;i++) {
						if (RankCount[i]>0) {
							switch (i) {
							case 0: MaxPoint+=20; break;
							case 1: MaxPoint+=10; break;
							case 2: MaxPoint-=10; break;
							case 3: MaxPoint-=20; break;
							}
							break;
						}
					}
					for (i=3;i>=0;i--) {
						if (RankCount[i]>0) {
							switch (i) {
							case 0: MinPoint+=20; break;
							case 1: MinPoint+=10; break;
							case 2: MinPoint-=10; break;
							case 3: MinPoint-=20; break;
							}
							break;
						}
					}
				}

				for (i=0;i<4;i++) {
					KyokuCount[i]=dis.readInt();
					HoraCount[i]=dis.readInt();
					ReachCount[i]=dis.readInt();
					ReachHoraCount[i]=dis.readInt();
					FuroCount[i]=dis.readInt();
					HoraTotalScore[i]=dis.readInt();
					HojuTotalScore[i]=dis.readInt();
					for (j=0;j<2;j++) {
						HojuReachCount[i][j]=dis.readInt();
						HojuFuroCount[i][j]=dis.readInt();
						HojuYamiCount[i][j]=dis.readInt();
						if (v>1) {
							DblRonHojuCount[i][j]=dis.readInt();
						}
					}
				}
				
				dis.close();
			} catch (Exception e) {
				//System.out.println("[SensekiData.read]"+e.toString());
				if (dis!=null) {
					try { dis.close(); } catch (Exception e2) {}
				}
				dis=null;
				return false; //読み込み失敗
			}
			dis=null;
			if ((n!=n2)||(t!=tp)) return false;
			return true;
		}

		public byte[] toBytes(int t, int n) {
			int i,j;
			int v=3; //バージョン
			ByteArrayOutputStream out=null;
			DataOutputStream      dos=null;
			byte[]                b=null;
			try {
				out=new ByteArrayOutputStream(4);
				dos=new DataOutputStream(out);
				
				dos.writeInt(t); //ルールのタイプを示す
				dos.writeInt(n); //レコードストアでの順序を示す
				dos.writeInt(v); //バージョン
				
				dos.writeInt(PlayCount);
				dos.writeInt(ContinueCount);
				dos.writeInt(StopCount);
				dos.writeInt(TobiCount);
				
				dos.writeInt(TotalPoint);
				dos.writeInt(MaxPoint);
				dos.writeInt(MinPoint);
				
				for (i=0;i<4;i++) 
					dos.writeInt(RankCount[i]);

				for (i=0;i<4;i++) {
					dos.writeInt(KyokuCount[i]);
					dos.writeInt(HoraCount[i]);
					dos.writeInt(ReachCount[i]);
					dos.writeInt(ReachHoraCount[i]);
					dos.writeInt(FuroCount[i]);
					dos.writeInt(HoraTotalScore[i]);
					dos.writeInt(HojuTotalScore[i]);
					for (j=0;j<2;j++) {
						dos.writeInt(HojuReachCount[i][j]);
						dos.writeInt(HojuFuroCount[i][j]);
						dos.writeInt(HojuYamiCount[i][j]);
						dos.writeInt(DblRonHojuCount[i][j]);
					}
				}
			
				b=out.toByteArray();
				dos.close();
			} catch (Exception e) {
				//System.out.println("[SensekiData.toBytes]"+e.toString());
				if (dos!=null) {
					try { dos.close(); } catch (Exception e2) {
						if (out!=null) {
							try { out.close(); } catch (Exception e3) {}
						}
					}
					out=null;
				}
				if (out!=null) {
					try { out.close(); } catch (Exception e2) {}
				}
				b=null;
			}
			dos=null; out=null;
			return b;
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@ レコードストア @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	private final String  RS_STCDATA="tm.mahjong.stcdata",
	                      RS_SETTING="tm.mahjong.setting",
	                      RS_SENSEKI="tm.mahjong.senseki",
	                      RS_SENREKI="tm.mahjong.senreki";

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private int getRecordSize(String rs_name) {
		RecordStore       rs=null;
		int sz=0;
		try {
			rs=RecordStore.openRecordStore(rs_name,true);
			sz=rs.getSize();
			rs.closeRecordStore();
		} catch (Exception e) {
			//System.out.println("[getRecordSize]"+e.toString());
			if (rs!=null) {
				try { rs.closeRecordStore(); } catch (Exception e2) {}
			}
			sz=0;
		}
		rs=null;
		return sz;
	}

	private int RecordsSize() {
		int sz=0;
		sz+=getRecordSize(RS_STCDATA);
		sz+=getRecordSize(RS_SETTING);
		sz+=getRecordSize(RS_SENSEKI);
		sz+=getRecordSize(RS_SENREKI);
		return sz;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void deleteRecords() {
		try {
			RecordStore.deleteRecordStore(RS_SENSEKI);
		} catch (Exception e) {
			//System.out.println("[deleteRecords]"+e.toString());
		}
		try {
			RecordStore.deleteRecordStore(RS_STCDATA);
		} catch (Exception e) {
			//System.out.println("[deleteRecords]"+e.toString());
		}
		try {
			RecordStore.deleteRecordStore(RS_SENREKI);
		} catch (Exception e) {
			//System.out.println("[deleteRecords]"+e.toString());
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void saveSensekiData() {
		int tp=rule_PlayLong;
		
		if(rule_tobi) tp|=1<<3;
		if(rule_kuitan) tp|=1<<4;

		RecordEnumeration re=null;
		RecordStore       rs=null;
		
		try {
			rs=RecordStore.openRecordStore(RS_SENSEKI,true);
			int n1=rs.getNumRecords();
			int n2=senseki.length;
			int n3=0;
			SensekiDataFilter     f=new SensekiDataFilter(tp);
			SensekiDataComparator c=new SensekiDataComparator();
			if (n1>0) {
				re=rs.enumerateRecords(f,c,false);
				n3=re.numRecords();
			}
			int i,id;
			byte[] data;
			for (i=0;i<n2;i++) {
				data=senseki[i].toBytes(tp,i);
				if (i<n3) {
					id=re.nextRecordId();
					rs.setRecord(id,data,0,data.length);
				} else {
					rs.addRecord(data,0,data.length);
				}
			}
			if (re!=null) re.destroy();
			rs.closeRecordStore();
			c=null; data=null;
		} catch (Exception e) {
			//System.out.println("[saveSensekiData]"+e.toString());
			if (re!=null) {
				try { re.destroy(); } catch (Exception e2) {}
			}
			if (rs!=null) {
				try { rs.closeRecordStore(); } catch (Exception e2) {}
			}
		}
		re=null; rs=null;
	}

	private void loadSensekiData() {
		int i;
		int tp=rule_PlayLong;
		
		if(rule_tobi) tp|=1<<3;
		if(rule_kuitan) tp|=1<<4;

		RecordEnumeration re=null;
		RecordStore       rs=null;
		
		int n2=senseki.length;
		for (i=0;i<n2;i++)
			senseki[i]=new SensekiData();

		try {
			rs=RecordStore.openRecordStore(RS_SENSEKI,true);
			int n1=rs.getNumRecords();
			int n4=0;
			SensekiDataFilter     f=new SensekiDataFilter(tp);
			SensekiDataComparator c=new SensekiDataComparator();
			if (n1>0) {
				re=rs.enumerateRecords(f,c,false);
				n4=re.numRecords();
			}
			int id;
			byte[] data;
			int n3=Math.min(n4,n2);
			for (i=0;i<n3;i++) {
				data=re.nextRecord();
				senseki[i].read(tp,i,data);
			}
			if (re!=null) re.destroy(); re=null;
			rs.closeRecordStore(); rs=null;
		} catch (Exception e) {
			//System.out.println("[loadSensekiData]"+e.toString());
			if (re!=null) {
				try { re.destroy(); } catch (Exception e2) {}
			}
			if (rs!=null) {
				try { rs.closeRecordStore(); } catch (Exception e2) {}
			}
		}

	}
	
	class SensekiDataFilter implements RecordFilter {
		private int tp=0;
		
		SensekiDataFilter(int t) {
			tp=t;
		}

		public boolean matches(byte[] candidate) {
			DataInputStream      dis1=null;
			int t=0;
			try {
				dis1=new DataInputStream(new ByteArrayInputStream(candidate));
				t=dis1.readInt();
				dis1.close(); dis1=null;
			} catch (Exception e) {
				//System.out.println("[SensekiDataFilter.matches]"+e.toString());
				if (dis1!=null) {
					try { dis1.close(); } catch (Exception e2) {} dis1=null;
				}
			}
			return (t==tp);
		}
	}

	class SensekiDataComparator implements RecordComparator {
		public int compare(byte[] rec1, byte[] rec2) {
			DataInputStream      dis1=null, dis2=null;
			int r=PRECEDES;
			try {
				dis1=new DataInputStream(new ByteArrayInputStream(rec1));
				dis2=new DataInputStream(new ByteArrayInputStream(rec2));
				dis1.readInt(); int n1=dis1.readInt();
				dis2.readInt(); int n2=dis2.readInt();
				dis1.close(); dis2.close();
				if (n1>n2)
					r=FOLLOWS;
			} catch (Exception e) {
				//System.out.println("[SensekiDataComparataor.compare]"+e.toString());
				if (dis1!=null) {
					try { dis1.close(); } catch (Exception e2) {} dis1=null;
				}
				if (dis2!=null) {
					try { dis2.close(); } catch (Exception e2) {} dis2=null;
				}
			}
			return r;
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void saveStcData() {
		RecordEnumeration re=null;
		RecordStore       rs=null;
		
		try {
			rs=RecordStore.openRecordStore(RS_STCDATA,true);
			int n1=rs.getNumRecords();
			int n2=stcdata.length;
			StcDataComparator c=new StcDataComparator();
			if (n1>0)
				re=rs.enumerateRecords(null,c,false);
			int i,id;
			byte[] data;
			for (i=0;i<n2;i++) {
				data=stcdata[i].toBytes(i);
				if (i<n1) {
					id=re.nextRecordId();
					rs.setRecord(id,data,0,data.length);
				} else {
					rs.addRecord(data,0,data.length);
				}
			}
			if (re!=null) re.destroy();
			rs.closeRecordStore();
			c=null; data=null;
		} catch (Exception e) {
			//System.out.println("[saveStcData]"+e.toString());
			if (re!=null) {
				try { re.destroy(); } catch (Exception e2) {}
			}
			if (rs!=null) {
				try { rs.closeRecordStore(); } catch (Exception e2) {}
			}
		}
		re=null; rs=null;
	}
	private void loadStcData() {
		int i;
		RecordEnumeration re=null;
		RecordStore       rs=null;
		
		int n2=stcdata.length;
		for (i=0;i<n2;i++)
			stcdata[i]=new StcData();

		try {
			rs=RecordStore.openRecordStore(RS_STCDATA,true);
			int n1=rs.getNumRecords();
			int n3=Math.min(n1,n2);
			StcDataComparator c=new StcDataComparator();
			if (n1>0)
				re=rs.enumerateRecords(null,c,false);
			int id;
			byte[] data;
			for (i=0;i<n3;i++) {
				data=re.nextRecord();
				stcdata[i].read(i,data);
			}
			if (re!=null) re.destroy(); re=null;
			rs.closeRecordStore(); rs=null;
		} catch (Exception e) {
			//System.out.println("[loadStcData]"+e.toString());
			if (re!=null) {
				try { re.destroy(); } catch (Exception e2) {}
			}
			if (rs!=null) {
				try { rs.closeRecordStore(); } catch (Exception e2) {}
			}
		}
		
	}
	class StcDataComparator implements RecordComparator {
		public int compare(byte[] rec1, byte[] rec2) {
			DataInputStream      dis1=null, dis2=null;
			int r=PRECEDES;
			try {
				dis1=new DataInputStream(new ByteArrayInputStream(rec1));
				dis2=new DataInputStream(new ByteArrayInputStream(rec2));
				int n1=dis1.readInt();
				int n2=dis2.readInt();
				dis1.close(); dis2.close();
				if (n1>n2)
					r=FOLLOWS;
			} catch (Exception e) {
				//System.out.println("[StcDataComparataor.compare]"+e.toString());
				if (dis1!=null) {
					try { dis1.close(); } catch (Exception e2) {} dis1=null;
				}
				if (dis2!=null) {
					try { dis2.close(); } catch (Exception e2) {} dis2=null;
				}
			}
			return r;
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void saveSetting() {
		ByteArrayOutputStream out=null;
		DataOutputStream      dos=null;
		RecordEnumeration     re=null;
		RecordStore           rs=null;
		
		try {
			int i;
			int v=1; //バージョン
			
			out=new ByteArrayOutputStream(4);
			dos=new DataOutputStream(out);
			
			dos.writeInt(v); //バージョン
			
			for (i=0;i<4;i++)
				dos.writeInt(think[i][0]);
			dos.writeBoolean(cpuopen);
			
			dos.writeInt(rule_PlayLong);
			dos.writeBoolean(rule_Continue);
			dos.writeBoolean(rule_SuddenDeath);
			dos.writeBoolean(rule_tobi);
			dos.writeBoolean(rule_kuitan);
			
			dos.writeBoolean(recording);
			
			byte[] data=out.toByteArray();
			dos.close(); dos=null; out=null;
			
			rs=RecordStore.openRecordStore(RS_SETTING,true);
			int n1=rs.getNumRecords();
			
			if (n1>0) {
				re=rs.enumerateRecords(null,null,false);
				int id=re.nextRecordId();
				rs.setRecord(id,data,0,data.length);
				re.destroy(); re=null;
			} else {
				rs.addRecord(data,0,data.length);
			}

			rs.closeRecordStore(); rs=null;
		} catch (Exception e) {
			//System.out.println("[saveSetting]"+e.toString());
			if (dos!=null) {
				try { dos.close(); } catch (Exception e2) {} dos=null;
			}
			if (out!=null) {
				try { out.close(); } catch (Exception e2) {} out=null;
			}
			if (re!=null) {
				try { re.destroy(); } catch (Exception e2) {} re=null;
			}
			if (rs!=null) {
				try { rs.closeRecordStore(); } catch (Exception e2) {} rs=null;
			}
		}
	}
	
	private void loadSetting() {
		DataInputStream       dis=null;
		RecordEnumeration     re=null;
		RecordStore           rs=null;
		
		try {
			rs=RecordStore.openRecordStore(RS_SETTING,true);
			int n1=rs.getNumRecords();
			if (n1==0) {
				rs.closeRecordStore();
				return;
			}
			re=rs.enumerateRecords(null,null,false);
			byte[] data=re.nextRecord();
			re.destroy(); re=null;
			rs.closeRecordStore(); rs=null;
			
			dis=new DataInputStream(new ByteArrayInputStream(data));
			int i;
			
			int v=dis.readInt(); //バージョン
			
			for (i=0;i<4;i++)
				think[i][0]=dis.readInt();
			cpuopen=dis.readBoolean();
			
			rule_PlayLong=dis.readInt();
			rule_Continue=dis.readBoolean();
			rule_SuddenDeath=dis.readBoolean();
			rule_tobi=dis.readBoolean();
			rule_kuitan=dis.readBoolean();
			
			recording=dis.readBoolean();
			
			dis.close(); dis=null; data=null;
		} catch (Exception e) {
			//System.out.println("[loadSetting]"+e.toString());
			if (dis!=null) {
				try { dis.close(); } catch (Exception e2) {} dis=null;
			}
			if (re!=null) {
				try { re.destroy(); } catch (Exception e2) {} re=null;
			}
			if (rs!=null) {
				try { rs.closeRecordStore(); } catch (Exception e2) {} rs=null;
			}
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void saveSenreki() {
		ByteArrayOutputStream out=null;
		DataOutputStream      dos=null;
		RecordEnumeration     re=null;
		RecordStore           rs=null;
		
		try {
			int i,j;
			int v=1; //バージョン
			
			out=new ByteArrayOutputStream(4);
			dos=new DataOutputStream(out);
			
			dos.writeInt(v); //バージョン
			
			dos.writeInt(senrekiCount); //戦歴カウント
			
			for (i=0;i<10;i++) {
				for (j=0;j<12;j++) {
					dos.writeInt(senreki[i][j]);
				}
				dos.writeInt(senrekiRule[i]);
				dos.writeLong(senrekiDate[i]);
			}
			
			byte[] data=out.toByteArray();
			dos.close(); dos=null; out=null;
			
			rs=RecordStore.openRecordStore(RS_SENREKI,true);
			int n1=rs.getNumRecords();
			
			if (n1>0) {
				re=rs.enumerateRecords(null,null,false);
				int id=re.nextRecordId();
				rs.setRecord(id,data,0,data.length);
				re.destroy(); re=null;
			} else {
				rs.addRecord(data,0,data.length);
			}

			rs.closeRecordStore(); rs=null;
		} catch (Exception e) {
			//System.out.println("[saveSetting]"+e.toString());
			if (dos!=null) {
				try { dos.close(); } catch (Exception e2) {} dos=null;
			}
			if (out!=null) {
				try { out.close(); } catch (Exception e2) {} out=null;
			}
			if (re!=null) {
				try { re.destroy(); } catch (Exception e2) {} re=null;
			}
			if (rs!=null) {
				try { rs.closeRecordStore(); } catch (Exception e2) {} rs=null;
			}
		}
	}
	private void loadSenreki() {
		DataInputStream       dis=null;
		RecordEnumeration     re=null;
		RecordStore           rs=null;
		
		try {
			rs=RecordStore.openRecordStore(RS_SENREKI,true);
			int n1=rs.getNumRecords();
			if (n1==0) {
				rs.closeRecordStore();
				return;
			}
			re=rs.enumerateRecords(null,null,false);
			byte[] data=re.nextRecord();
			re.destroy(); re=null;
			rs.closeRecordStore(); rs=null;
			
			dis=new DataInputStream(new ByteArrayInputStream(data));
			int i,j;
			
			int v=dis.readInt(); //バージョン
			
			senrekiCount=dis.readInt();
			
			for (i=0;i<10;i++) {
				for (j=0;j<12;j++) {
					senreki[i][j]=dis.readInt();
				}
				senrekiRule[i]=dis.readInt();
				senrekiDate[i]=dis.readLong();
			}
			
			dis.close(); dis=null; data=null;
		} catch (Exception e) {
			//System.out.println("[loadSetting]"+e.toString());
			if (dis!=null) {
				try { dis.close(); } catch (Exception e2) {} dis=null;
			}
			if (re!=null) {
				try { re.destroy(); } catch (Exception e2) {} re=null;
			}
			if (rs!=null) {
				try { rs.closeRecordStore(); } catch (Exception e2) {} rs=null;
			}
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

} //Canvas1の終わり

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
