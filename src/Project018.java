// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// Project018.java  アプリ本体
//
// (C)Tomarky   2009.01.01-
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

//インポート
//import javax.microedition.lcdui.game.*;
//import java.util.*;
//import javax.microedition.rms.*;
//import java.io.*;
//import javax.microedition.media.*;
import javax.microedition.lcdui.*;
import javax.microedition.midlet.*;

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

//MIDlet(アプリ本体)
public class Project018 extends MIDlet {
	private Display         d;
	private Canvas1         c1;
	private Thread          t;

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//コンストラクタ
	public Project018() {

		d=Display.getDisplay(this);

		c1=new Canvas1(this);
	
		d.setCurrent(c1);
	
		t=new Thread(c1);

		//スレッドの実行
		try { t.start();} catch (Exception e) {}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//アプリの開始 (起動時、一時停止からの復旧時に実行される)
	public void startApp() {
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//アプリの一時停止
	public void pauseApp() {
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//アプリの終了
	public void destroyApp(boolean flag) {
		c1.callFinish();
		try {t.join();} catch (Exception e) {}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

} //Project018の終わり

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
