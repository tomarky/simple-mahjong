//---------------------------------------------------------//
//                     canvas1.java                        //
//---------------------------------------------------------//

//*********************************************************//
//                        定数                             //
//*********************************************************//


private final int       DISPLAY_WIDTH  = 240, //画面サイズ
                        DISPLAY_HEIGHT = 268;

private final int       CLEAR =  -8,          //キーコード
                        SEND  = -10,
                        SOFT1 =  -6,
                        SOFT2 =  -7,
                        SOFT3 = -20,
                        SOFT4 = -21;

private final int       PAI_WIDTH  = 12,      //牌のサイズ
                        PAI_HEIGHT = 15;

private final int       CALL_TUMO = 0,        //鳴きの文字表示区別用
                        CALL_CHI  = 1,
                        CALL_PON  = 2,
                        CALL_KAN  = 3,
                        CALL_RON  = 4;

private final int       FURO_CHI    = 0,      //副露の種類を示す
                        FURO_PON    = 1,      
                        FURO_MINKAN = 2,
                        FURO_ANKAN  = 3;

private final int       GM_START     =  0,    //各ゲームモード
                        GM_HAIPAI    =  1, 
                        GM_RIHAI     =  2, 
                        GM_TUMO      =  3,
                        GM_NEXT      =  4, 
                        GM_RYUKYOKU  =  5, 
                        GM_FUROCHECK =  6, 
                        GM_CALLCHECK =  7,
                        GM_PON1      =  8, 
                        GM_PON2      =  9, 
                        GM_FURODAHAI = 10, 
                        GM_CHI1      = 11,
                        GM_CHI2      = 12, 
                        GM_MINKAN1   = 13, 
                        GM_MINKAN2   = 14, 
                        GM_RINSHAN   = 15;

//*********************************************************//
//                        変数                             //
//*********************************************************//


private Graphics        g;                     //グラフィック
private int             keyEvent=-999;         //キーイベント
private int             keyState;              //キー状態
private boolean         bl=true;               //メインループフラグ
private boolean         cf=false;              //callfinish呼び出しフラグ
private Command[]       cmds=new Command[6];   //コマンド
private int             cmd=-1;                //コマンド番号
private Random          rndm=new Random();     //乱数

private MIDlet          midlet;                //MIDlet処理用

private Sprite          sprite;                //牌表示用スプライト

private int             GameMode=GM_START;     //ゲームモード

private int[]           yama=new int[136];           //牌山
private int[][]         tehai=new int[4][14];        //各家の手牌 [][13]は手牌の牌の数
private int[][][]       sutehai=new int[4][30][2];   //各家の捨て牌 [][0][0]は捨て牌の牌の数、
                                                     //  [][][0]は牌番号、[][][1]はリーチ(2)や副露(1)されたかを示す
private int[][][]       furopai=new int[4][5][2];    //各家の副露牌 [][0][0]は副露の数
                                                     //  [][][0]は副露の種類(定数)、[][][1]は牌番号（チーの場合は最小値の牌番号）
private int             kicha,         //起家  0〜3 で 0〜2がコンピュータ、3がプレイヤー
                        frCheck,       //副露確認順 (1〜3 ,打牌した側から 1下家 2対家 3上家)
                        oya,           //親
                        callPlayer,    //副露成功の家
                        nowPlayer;     //現在の家
private int             tumoIndex,     //現在の牌山のツモ位置 配牌時に計算される 
                        ryukyokuIndex, //荒牌流局になるツモ位置 初期値は 136-14
                        rinshanIndex,  //現在の派山の嶺上牌の位置
                        dahai;         //打牌した牌
private int             selectPai,     //選択してる牌（打牌選択時など）
                        selectTCmd,    //選択してる打牌時行動（リーチなど）
                        selectFCmd;    //選択してる副露行動（ポン・チー・パス・ロンなど）
private int             cpuplayer,     //コンピュータの家 4bitフラグ (下位ビットから各家(0~3)に割り当てる
                                       //  プレーヤーは3なので8の桁、1ならコンピュータ、0ならプレーヤー）
                        paiopen,       //手牌をオープンにしてる家 4bitフラグ (割り当ては上に同じ、1なら表示、0なら非表示）
                        PonCall,       //ポンをコールした家(+1して1〜4)
                        KanCall,       //カンをコールした家(+1して1〜4)
                        ChiCall,       //チーをコールした家(+1して1〜4)
                        RonCall;       //ロンをコールしてる家 4bitフラグ 
private int             baKaze,        //現在の場風  0東 1南 2西 3北
                        kyoku,         //現在の局数  数字には0〜3を割り当てる（表示の際は+1すること）
                        honba,         //現在の百点棒の本数（流局数）
                        kyotaku;       //現在の供託棒の本数
private boolean         furoselect,    //副露コマンドの表示を決める（副露出来ない家やコンピュータなど、trueなら表示）
                        furodahai;     //trueならポン・チー後の打牌であることをあらわす

private int             callCount;     //鳴きの文字表示にかける時間をカウントする

private boolean[][]     r_agari=new boolean[4][34],  //各家のロン和出来る牌をあらわす
                        t_agari=new boolean[4][34],  //各家のツモ和出来る牌をあらわす
                        k_agari=new boolean[4][34];  //各家の役なし（形式テンパイ）あがり牌をあらわす

//*********************************************************//
//                   メソッド Public                       //
//*********************************************************//

public void run()                   //Runnableのrunのオーバーライド
public void keyPressed(int keyCode) //CanvasのkeyPressedのオーバーライド
public void commandAction(Command c,Displayable disp)
                                    //CommandListenerのコマンド受け取り用
public void callFinish()            //Project018から呼び出すrunの処理を終了させる

//*********************************************************//
//                   メソッド Private                      //
//*********************************************************//

private String  KazeName(int n) //東南西北の文字を返す
private int     getSelectPai(int p, int pai)
                                //

private int     rand(int num)  //乱数生成メソッド num以下の正の整数の乱数を生成

private void    gm_start()     //ゲームモード(GM_START)の処理 
                               //  牌山をシャッフル後配牌して、各変数を初期化する
private void    gm_haipai()    //ゲームモード(GM_HAIPAI)の処理
                               //  配牌された状態。FIRE入力で理牌する
private void    gm_rihai()     //ゲームモード(GM_RIHAI)の処理
                               //  理牌された状態。FIREでツモ順の人がツモる
private void    gm_tumo()      //ゲームモード(GM_TUMO)の処理
                               //  ツモ後の行動を決める状態。打牌やリーチ等を決める
private void    gm_next()      //
private void    gm_callcheck() 
private void    gm_furocheck() 
private void    gm_chi1()    
private void    gm_chi2() 
private void    gm_pon1() 
private void    gm_pon2()
private void    gm_furodahai()
private void    gm_minkan1()
private void    gm_minkan2()

private boolean isCompute(int p) 
private int     computeTCommand(int p) 
private int     computeFCommand(int p) 
private int     computeFDahai(int p)
private int     computeKCommand(int p) 

private int     checkPon(int p, int pai) 
private boolean checkKan(int p, int pai) 
private int     checkChi(int p, int pai) 
private boolean checkFuro(int p, int pai)
private boolean checkAnkan(int p, int pai)
private boolean checkKakan(int p, int pai)

private void    shuffle()
private void    haipai() 
private void    rihai(int p) 

private void    drawPai(int n, int x, int y) 
private void    drawTehai(int p, int x, int y) 
private void    drawTumo() 
private void    drawSutehai(int p, int x, int y) 
private void    spotDahai(int p,int x, int y) 
private void    drawTCommand(int n, int x, int y) 
private void    drawFCommand(int n, int x, int y) 
private void    drawFuro(int p) 
private void    drawCall(int n, int x, int y) 
private void    drawSelectPai(int n, int p) 
private void    drawInfo()

















